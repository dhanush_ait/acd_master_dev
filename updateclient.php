<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="google-signin-client_id" content="709502795016-qbft7m0mc6m6ekkdu5esdefsvlsbj0s5.apps.googleusercontent.com">

    <title>ACDWeb | Update Client</title>

    <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css">
    <link rel="stylesheet" href="css/defaults.css" type="text/css">
    <link rel="stylesheet" href="css/bootstrap-datetimepicker.css" type="text/css">
    <link rel="stylesheet" href="css/autocomplete.css" type="text/css">

    <script type="application/javascript" src="js/jquery.min.js"></script>
    <script type="application/javascript" src="js/jquery-ui.min.js"></script>
    <script type="application/javascript" src="js/bootstrap.min.js"></script>
    <script type="application/javascript" src="js/validator.min.js"></script>
    <script type="application/javascript" src="js/jquery.lazy.min.js"></script>
    <script type="application/javascript" src="js/bootbox.min.js"></script>
    <script type="application/javascript" src="js/docked-link.min.js"></script>
    <script type="application/javascript" src="js/datatables/datatables.js"></script>
    <script type="text/javascript" src="js/touchspin.min.js"></script>
    <script type="application/javascript" src="js/caseTypeLoader.js"></script>

    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script type="application/javascript">
        $(document).ready(function() {
            $('#updateClientDetailsTable').DataTable({
                "ajax": {
                    "url": "authuser/AJAX/fetchallclientdetails.php",
                    "type": "POST"
                },
                columns: [{
                        "data": "FULL_ID"
                    },
                    {
                        "data": "CASE_ID"
                    },
                    {
                        "data": "CASE_DEFENDANT"
                    },
                    {
                        "data": "CASE_OPPONENT"
                    },
                    {
                        "data": "CLIENT_EMAIL"
                    },
                    {
                        "data": "CLIENT_CONTACT"
                    },
                    {
                        "data": "CLIENT_ADDRESS"
                    },
                    {
                        "data": "CLIENT_CITY"
                    },
                    {
                        "data": "CLIENT_STATE"
                    },
                    {
                        "data": "BUTTON"
                    }
                ],
                responsive: true,
                select: true,
                columnDefs: [{
                        responsivePriority: 1,
                        targets: 0
                    },
                    {
                        responsivePriority: 2,
                        targets: 2
                    },
                    {
                        "targets": [1],
                        "visible": false
                    }
                ]
            });
            loadData();
        });

        function loadData(){
            $.ajax({
                url: "authuser/AJAX/fetchallclientdetails.php",
                type: "POST",
                success: function(results) {
                    window.data = JSON.parse(results);
                    //console.log(data);
                }
            });
        }
        function populateClientDetails(caseID) {
            $('#caseIDSelector').val(data['data'][caseID]['CASE_ID']);
            $('#case_no').val(data['data'][caseID]['FULL_ID']);
            $('#case_appelant').val(data['data'][caseID]['CASE_DEFENDANT']);
            $('#clientEmail').val(data['data'][caseID]['CLIENT_EMAIL']);
            $('#clientContact').val(data['data'][caseID]['CLIENT_CONTACT']);
            $('#clientAddress').val(data['data'][caseID]['CLIENT_ADDRESS']);
            $('#clientCity').val(data['data'][caseID]['CLIENT_CITY']);
            $('#clientState').val(data['data'][caseID]['CLIENT_STATE']);
        }
    </script>

</head>

<body>

    <?php
    session_start();
    if (!isset($_SESSION['emailID']) || $_SESSION['userActive'] != 1) {
        echo "<script type=\"text/javascript\">
                bootbox.alert({
                size: \"large\",
                title: \"Sorry\",
                message: \"There was an error while processing your request. You may try logging-in again.\",
                callback: function() {
                    window.location = \"index.php\";
                }
                })
                </script>";
        echo "1";
        exit();
    }
    ?>

    <!-- Navbar -->

    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="../index.php" class="navbar-brand">
                    <img src="images/advlogo.jpg" width="28" height="24" />
                </a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav">
                    <li><a href="welcome.php">HOME</a></li>
                    <li><a href="updatecaselist.php">SEARCH</a></li>
                    <li><a href="updateclient.php">CLIENTS</a></li>
                    <li><a href="cupdate.php">NOTIFICATIONS</a></li>
                </ul>
                <form class="navbar-form navbar-right" id="recruiterLogout" name="recruiterLogout" method="" action="">
                    <div class="form-group">
                        <input type="button" class="btn btn-sm btn-danger pull-righ;t" onclick="logoutAccount()" name="logoutButtonLargeScreen" id="logoutButtonLargeScreen" value="Logout" />
                        <div class="g-signin2 btn" data-onsuccess="onSignIn" style="display:none"></div>
                        <input type="button" class="btn btn-sm btn-danger center-block" onclick="logoutAccount()" name="logoutButtonSmallScreen" id="logoutButtonSmallScreen" value="Logout" />
                    </div>
                </form>
            </div>
        </div>
    </nav>


    <div class="container" style="">
        <div class="row" id="updateClientDetailsRow" style="">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="text-align: center">
                            <strong>
                                <p class="panel-title" id="datePanel"></p>Client List
                            </strong>
                        </div>
                        <div id="" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row" id="updateClientDetailsPanel">
                                    <div class='col-md-12 col-xs-12 col-lg-12'>
                                        <div class='form-group'>
                                            <table style="width:100%" class='table table-bordered table-hover table-responsive table-striped' id='updateClientDetailsTable'>
                                                <thead>
                                                    <tr style='height: 20px; background-color: lightseagreen; vertical-align: middle'>
                                                        <th style='vertical-align: middle; width: 1%;'>Case No.</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Case ID.</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Defendant</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Opponent</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Email</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Contact</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Address</th>
                                                        <th style='vertical-align: middle; width: 1%;'>City</th>
                                                        <th style='vertical-align: middle; width: 1%;'>State</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="updateClientDetailsTableBody">
                                                </tbody>
                                                <tfoot>
                                                    <tr style='height: 20px; background-color: lightseagreen; vertical-align: middle'>
                                                        <th style='vertical-align: middle; width: 1%;'>Case No.</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Case Id.</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Defendant</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Opponent</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Email</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Contact</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Address</th>
                                                        <th style='vertical-align: middle; width: 1%;'>City</th>
                                                        <th style='vertical-align: middle; width: 1%;'>State</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Action</th>
                                                    </tr>
                                                </tfoot>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="editClientDetailsModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Case</h4>
                </div>
                <div class="modal-body" style="max-height: 60vh; overflow: auto;">
                    <form name="updateCaseDetailsForm" id="updateCaseDetailsForm">
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="caseNumber">Case Number</label>
                            <input type="text" maxlength="7" size="28" name="case_no" class="form-control" id="case_no" title="Enter Maximum 7 digit of Case Number Compulsory Field" autocomplete="off" disabled="disabled">
                            <input type="hidden" name="caseIDSelector" id="caseIDSelector" value="">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="Appelant">Appelant</label>
                            <input type="text" class="form-control" name="case_appelant" id="case_appelant" value="" title="Please enter valid Name" autocomplete="off" disabled="disabled">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="clientEmail">Email</label>
                            <input type="text" class="form-control" name="clientEmail" id="clientEmail" value="" title="Please enter valid Name" autocomplete="off">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="clientContact">Contact</label>
                            <input type="text" class="form-control" maxlength="10" name="clientContact" id="clientContact" value="" title="Please enter valid Name" autocomplete="off">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="clientAddress">Address</label>
                            <input type="text" class="form-control" name="clientAddress" id="clientAddress" value="" title="Please enter valid Name" autocomplete="off">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="clientCity">City</label>
                            <input type="text" class="form-control" name="clientCity" id="clientCity" value="" title="Please enter valid Name" autocomplete="off">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="clientState">State</label>
                            <input type="text" class="form-control" name="clientState" id="clientState" value="" title="Please enter valid Name" autocomplete="off">
                            <div class="help-block with-errors"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="updateClientDetails" class="btn btn-success pull-left">Update</button>
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script type="application/javascript">
        $('#updateClientDetails').click(function() {
            var emailObj = document.getElementsByName('clientEmail');
            if (emailObj[0].value == "") {
                alert("Please enter Email");
                emailObj[0].focus();
                return false;
            }
            var emailVal = emailObj[0].value;
            var reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var emailVal = emailObj[0].value;
            if (!reg.test(emailVal)) {
                alert("Enter only valid email");
                emailObj[0].value = '';
                emailObj[0].focus();
                return false;
            }

            var captchaObj = document.getElementsByName('clientContact');
            if (captchaObj[0].value == "") {
                alert("Please enter contact number");
                captchaObj[0].focus();
                return false;
            }
            var captchaVal = captchaObj[0].value;
            var reg = new RegExp("^[0-9]{10}$");
            if (!captchaVal.match(reg)) {
                alert("Enter valid contact number");
                captchaObj[0].value = '';
                captchaObj[0].focus();
                return false;
            }
            $.ajax({
                url: "authuser/AJAX/updateclientdetails.php",
                type: "POST",
                data: {
                    'caseID': $('#caseIDSelector').val(),
                    'email': $('#clientEmail').val(),
                    'contact': $('#clientContact').val(),
                    'address': $('#clientAddress').val(),
                    'city': $('#clientCity').val(),
                    'state': $('#clientState').val()
                },
                success: function(results) {
                    bootbox.alert({
                        size: "small",
                        title: "Success",
                        message: "Client " + results + " has been successfully updated.",
                    });
                    var table = $('#updateClientDetailsTable').DataTable();
                    table.ajax.reload();
                    loadData();
                    $("#editClientDetailsModal").modal('hide');
                }
            });
        });

        function logoutAccount() {
            window.open("logout.php", '_self');
        }
    </script>

</body>

</html>