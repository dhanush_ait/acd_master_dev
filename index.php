<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>ACDWeb | Welcome</title>

    <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css">
    <link rel="stylesheet" href="css/php_pages/index_php.css" type="text/css">
    <link rel="stylesheet" href="css/defaults.css" type="text/css">
    <link rel="stylesheet" href="css/php_pages/pulse.css" type="text/css">

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/validator.min.js"></script>
    <script type="application/javascript" src="js/bootbox.min.js"></script>
    <script type="application/javascript" src="js/docked-link.min.js"></script>

    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>

<body>

<a class="btn" id="loginDockedButton"  href="applogin.php" style="right: 0px; display: block; position: fixed;">Login</a>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="index.php" class="navbar-brand">
                <img src="images/advlogo.jpg" width="28" height="24" />
            </a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <ul class="nav navbar-nav">
                <li><a href="index.php">Home<span class="sr-only">(current)</span></a></li>
                <li><a href="about.php">About Us</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid containerSpacing"></div>

<div class="container-fluid bannercontainer">
    <img class="splashimage1" src="images/acd_banner.jpg">
</div>

<div class="container-fluid containerSpacing"></div>



<div class="container-fluid containerSpacing"></div>

<div class="container-fluid" style="padding-bottom: 1%;">
    <div class="row">
        <div class="col-xs-12">
            <p id="footertext">© Advoacte's Case Dairy</p>
        </div>
    </div>
</div>

</body>

</html>

<script type="application/javascript">
    
    function validateFun()
    {
        var Name = $("#trialFullName").val();
        var MobileNumber = $('#trialContactNumber').val();
        var ContactMail = $('#trialEmailId').val();

        if(!(/^[a-zA-Z\s]+$/.test(Name)))
        {
            bootbox.alert({
                size: "small",
                message: "Please provide valid name"
            });
            return 0;
        }

        if(!(/^\d{10}$/.test(MobileNumber)))
        {
            bootbox.alert({
                size: "small",
                message: "Please provide valid mobile number"
            });
            return 0;
        }

        if(!(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(ContactMail)))
        {
            bootbox.alert({
                size: "small",
                message: "Please provide valid Email ID"
            });
            return 0;
        }

        return 1;
    }

    $('#trialDockedButton').dockedLink({
        position: 'right',
        pixelsFromTop:250
    });

</script>