<?php
// Initialize the session
session_start();
 
// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  header("location: login.php");
  exit;
}
?>
<?php

// Include config file
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_NAME','case dairy');
 
/* Attempt to connect to MySQL database */
$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
//$db_found = mysql_select_db($database, $db_handle);
// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
} 
// Define variables and initialize with empty values
$A_name = $A_id = $U_age = $email = $gender = $qualification = $salary= "";
//$username_err = $password_err = "";
 
/*function check($link)
{
    $qry="SELECT MAX(id) FROM users";
    $result = mysqli_query($link,$qry);
    $row = mysqli_fetch_row($reslut) ;
    return $row ;
    {      
            add($link,$A_id) ;
            //echo "<h3> Employee id $A_id  Matched.</h3>" ;
            return TRUE ;
    }
            //mysqli_free_result($result);
    else{
        echo "<h3>No match found for Employee id $A_id  Try Again !</h3>";
        return FALSE ;
        //mysqli_free_result($result);
        //exit();
    }*/
//}
/*function add($link,$A_id)
{
        $qry = "INSERT into user_details (A_id) values ($A_id)" ;
        if(mysqli_query($link,$qry))
                    echo "Success" ;
        else 
                    echo "Fail";

}*/
// Processing form data when form is submitted

if($_SERVER["REQUEST_METHOD"] == "POST"){
                
                //$A_id = $_POST['A_id']; 
               // $A_id = check($link);
                //if($A_id)
                $U_age = $_POST['U_age'];
                $A_name =  $_POST['A_name'] ;
                //echo "$caid " ;
                $email = $_POST['email'] ;
                $gender = $_POST['gender'] ;
                $qualification = $_POST['qualification'];
                $salary = $_POST['salary'] ;
                //echo "$c_email" ;
                //$sql1 = "UPDATE user_details SET A_name = '$A_name', U_age=$U_age, email='$email',gender='$gender',qualification='$qualification',salary=$salary WHERE A_id =$A_id";
                $qry = "INSERT INTO user_details(A_name,U_age,email,gender,qualification,salary) VALUES ('$A_name',$U_age,'$email','$gender','$qualification',$salary)";
                if(mysqli_query($link,$qry))
                    echo "<h4> Employee details Added or Updated for $A_name is Successful. </h4>" ;
                else
                    echo "<h4> Please Enter valid Employee ID details</h4>";
                  mysqli_close($link);
    }
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Employe</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
<div class="wrapper" style = "color:green;">
        <h3><a href = "welcome.php">Back to Home </a> </h3>
        <br />
        <br /><p>Please fill for Employee details.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <!--div class="form-group">
                <label>Employee ID *</label>
                <input type="number" min = "1" max="100" name="A_id" placeholder="A_ID" class="form-control" required>
            </div-->
            <div class="form-group">
             <label>Employee Name</label>
                <input type="text" name="A_name" placeholder="Name" class="form-control"  required>
            </div>    
            <div class="form-group">
                <label>User Age</label>
                <input type="number" min="1" max="100" name="U_age" placeholder="Age" class="form-control" >
             </div>  
            <div class="form-group">
                <label>Email Adress</label>
                <input type="email" name="email" placeholder="email address" class="form-control"  required>
            </div>     
            <div class="form-group">
                <label>Gender</label>
                <input type="radio" name="gender" value="male" checked> Male<br>
                <input type="radio" name="gender" value="female"> Female
             </div>
             <div class="form-group">
                <label>Qualification</label>
                <input type="text" name="qualification" placeholder="qualification" class="form-control" >
             </div>
             <div class="form-group">
                <label>Salary</label>
                <input type="number" name="salary" placeholder="Salary" class="form-control" >
             </div>
             <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Add Employee">
                <input type="reset" class="btn btn-primary" value="Reset">          
            </div>
           </form>
    </div>    
</body>
</html>