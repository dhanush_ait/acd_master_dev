<?php

include "registrationutilsacd.php";

function getCaseListFile($details)
{
    $pat = "/[0-9].*@/";
    $court_code = preg_replace($pat, "", $details[3]);
    $advName = preg_replace("/ /", '\ ', $details[2]);
    $bar = preg_split('/\-/',$details[1]);
    chdir('../../python_scraping/ecourtScrapy/spiders');
    $status = shell_exec('scrapy crawl caseListByName -a advName='."{$advName}".' -a courtComplex='."$court_code".' -a ID='."$details[0]");
    if($bar[1]=="" || $bar[2]=="" || $bar[0]==""){
        return "no";
    }
    $status1 = shell_exec('scrapy crawl caseListByBar -a bar1='."$bar[0]".' -a bar2='."$bar[1]".' -a bar3='."$bar[2]".' -a courtComplex='."$court_code".' -a ID='."$details[0]");
    return "success";
}

function writeCaseListToDBFromFile($customerID, $type)
{
    // Get the contents of the JSON file 
    $strJsonFileContents = file_get_contents("/userdata/{$customerID}/caseListBy{$type}.json");
    // Convert to array 
    $array = json_decode($strJsonFileContents, true);

    //var_dump($array);

    $c = [];

    foreach ($array['con'] as $k => $v) {
        //print_r($k);
        $v = json_decode($v, true);
        //Test whether there are any empty sets in the result array
        if($v==""){
            continue;
        }
        foreach ($v as $k1 => $v1) {
            //$v1 = json_decode($v1, true);
            //print_r($v);
            foreach ($v1 as $k2 => $v2) {
                //print_r($k2 . ":".$v2);
                if ($k2 == "cino" && $v2 != "") {
                    $c[$k2] = $v2;
                }
                if ($k2 == "case_type" && $v2 != "") {
                    $c[$k2] = $v2;
                }
                if ($k2 == "case_year" && $v2 != "") {
                    $c[$k2] = $v2;
                }
                if ($k2 == "case_no2" && $v2 != "") {
                    $c[$k2] = $v2;
                }
                if ($k2 == "pet_name" && $v2 != "") {
                    $c[$k2] = $v2;
                }
                if ($k2 == "res_name" && $v2 != "") {
                    $c[$k2] = $v2;
                }
                if ($k2 == "case_type" && $v2 != "") {
                    $c[$k2] = $v2;
                }
                //$c['court_code'] = $k;
            }
            $result[] = $c;

            $details[0] = $c["case_type"];
            $details[1] = $c["case_no2"];
            $details[2] = $c["case_year"];
            $details[3] = $c["pet_name"];
            $details[4] = $c["res_name"];
            $details[5] = $array["court_code"][$k];
            $details[6] = $customerID;
            $details[7] = "no data";
            $details[8] = $c["cino"];

            $caseExists = CheckCaseExistsFromFile($details);

            if ($caseExists) {
                //echo json_encode(1);
                continue;
            }

            $genCaseId = AddNewCaseFromFile($details);

            $data = UpdateCaseProceedingsByScrapy($details[5], $details[8], $details[6]);
            
            $data[0] = date("Y-m-d", strtotime($data[0]));

            AddInitialCaseProceeding($genCaseId, $data[0], $data[1]);

            $genClientId = AddClientEntry($details[7], $genCaseId);

            AddMailDetails($genCaseId, $genClientId, $details[7]);

            AddInitialPaymentDetails($genCaseId, $genClientId, $details[6]);
        }
    }
    return "success";
}

function UpdateCaseProceedingsByScrapy($courtCode, $cnr, $cid){
    
    //chdir('/acdScrapy/pypath/ecourtScrapy/spiders');
    chdir('../../python_scraping/ecourtScrapy/spiders');

    exec('scrapy crawl caseHistory -a court='."$courtCode".' -a cnr='."$cnr".' -a cid='."$cid", $data);
    
    return $data;
}