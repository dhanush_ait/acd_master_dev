<?php
session_start();

require_once '../vendor/autoload.php';

require_once "verifylogin.php";
require "adminutilsacd.php";

$userName = $_POST['username'];
$password = $_POST['password'];
$tokenId = $_POST['tokenId'];
$CLIENT_ID = "709502795016-qbft7m0mc6m6ekkdu5esdefsvlsbj0s5.apps.googleusercontent.com";

// Get $id_token via HTTPS POST.

$client = new Google_Client(['client_id' => $CLIENT_ID]); // Specify the CLIENT_ID of the app that accesses the backend
$payload = $client->verifyIdToken($tokenId);
$email = $payload['email'];
$status = UserLoginValidate($email, $userName, $password);
if ($payload && $status == 1) {
    //$userid = $payload['sub'];
    // If request specified a G Suite domain:
    //$domain = $payload['hd'];
    $customerId = fetchCustomerId($email);

    $_SESSION['userActive'] = 1;
    $_SESSION['emailID'] = $email;
    $_SESSION['customerId'] = $customerId;
    $_SESSION['caseUpdate'] = "false";
    echo $status;
    }
     else {
    // Invalid ID token
    echo $status;
}

exit();