<?php

function UpdateCaseDetails($details)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");
    $prevdate = date("Y-m-d");

    $reminderStatus = 1;

    $update_case_query = $mysql_conn->prepare("
            UPDATE
            case_list,
            case_proceeding
            SET
            CASE_DEFENDANT=?,
            CASE_OPPONENT=?,
            NEXT_DATE=?,
            CASE_SUMMARY=?,
            PREVIOUS_DATE=?,
            REMINDER_STATUS=?,
            LAST_UPDATE_ON=?
            WHERE
            case_list.CASE_ID=? AND
            case_proceeding.CASE_ID=case_list.CASE_ID AND
            CUSTOMER_ID=?
        ");

        $update_case_query->bind_param("sssssssss",
            $details[1],
            $details[2],
            $details[3],
            $details[4],
            $prevdate,
            $reminderStatus,
            $timestamp,
            $details[0],
            $details[5]
        );

        $update_case_query->execute();

    $mysql_conn->close();
}

function UpdateClientDetails($details)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $update_client_query = $mysql_conn->prepare("
            UPDATE
            clients,
            case_list
            SET
            CLIENT_EMAIL=?,
            CLIENT_CONTACT=?,
            CLIENT_ADDRESS=?,
            CLIENT_CITY=?,
            CLIENT_STATE=?,
            LAST_UPDATE_ON=?
            WHERE
            case_list.CASE_ID=? 
            AND
            clients.CASE_ID = case_list.CASE_ID
            AND
            case_list.CUSTOMER_ID=?
        ");

        $update_client_query->bind_param("ssssssss",
            $details[1],
            $details[2],
            $details[3],
            $details[4],
            $details[5],
            $timestamp,
            $details[0],
            $details[6]
        );

        $update_client_query->execute();

    $mysql_conn->close();
}

//Update sent mail details
function UpdateSentMail($details)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $reminderStatus = 0;

    $update_client_query = $mysql_conn->prepare("
            UPDATE
            notification,
            case_list
            SET
            EMAIL_ID=?,
            MESSAGE=?,
            REMINDER_STATUS=?,
            LAST_SENT_ON=?
            WHERE
            notification.CASE_ID=? 
            AND
            CLIENT_ID=?
            AND
            case_list.CASE_ID = notification.CASE_ID

        ");

        $update_client_query->bind_param("ssssss",
            $details[1],
            $details[2],
            $reminderStatus,
            $timestamp,
            $details[0],
            $details[5]
        );

        $update_client_query->execute();

    $mysql_conn->close();
}

//Update user lists
function UpdateUserDetails($details)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");
    $admin = "admin";
    $update_client_query = $mysql_conn->prepare("
            UPDATE
            userslist
            SET
            EMAIL_VERIFY_STATUS=?,
            ACTIVE_STATUS=?,
            LAST_UPDATED_BY=?,
            LAST_UPDATED_ON=?
            WHERE
            CUSTOMER_ID=?
        ");

        $update_client_query->bind_param("iissi",
            $details[1],
            $details[2],
            $admin,
            $timestamp,
            $details[0]
        );
        $update_client_query->execute();

    $mysql_conn->close();
}