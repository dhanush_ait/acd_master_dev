<?php

function LoadDeviceDetailsAdmin($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $load_device_details_query = $mysql_conn->prepare("
        SELECT
        DEVICE_NAME,
        DATE_FORMAT(INSTALLATION_DATE, '%d-%m-%Y') as INSTALLATION_DATE,
        VENUE_NAME
        FROM
        device,venue
        WHERE
        device.VENUE_ID = venue.VENUE_ID
        AND
        DEVICE_NAME=?
    ");

    $load_device_details_query->bind_param("s", $device_name);

    $load_device_details_query->execute();

    $load_device_details_query->bind_result(
        $device_name_col,
        $venue_name_col,
        $installation_date_col
    );

    $device_details = [];

    while ($load_device_details_query->fetch()) {
        array_push(
            $device_details,
            $device_name_col,
            $venue_name_col,
            $installation_date_col
        );
    }

    $load_device_details_query->close();

    $mysql_conn->close();

    return $device_details;
}

function LoadDeviceDetailsCustomer($device_name, $email)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $load_device_details_query = $mysql_conn->prepare("
        SELECT
        DEVICE_NAME,
        DATE_FORMAT(INSTALLATION_DATE, '%d-%m-%Y') as INSTALLATION_DATE,
        venue.VENUE_NAME
        FROM
        device,userslist,venue
        WHERE
        device.VENUE_ID = venue.VENUE_ID
        AND
        device.CUSTOMER_ID = userslist.CUSTOMER_ID
        AND
        userslist.EMAIL=?
        AND
        DEVICE_NAME=?
    ");

    $load_device_details_query->bind_param("ss", $email, $device_name);

    $load_device_details_query->execute();

    $load_device_details_query->bind_result(
        $device_name_col,
        $venue_name_col,
        $installation_date_col
    );

    $device_details = [];

    while ($load_device_details_query->fetch()) {
        array_push(
            $device_details,
            $device_name_col,
            $venue_name_col,
            $installation_date_col
        );
    }

    $load_device_details_query->close();

    $mysql_conn->close();

    return $device_details;
}

function AddScheduleEntry($sched_entry_new)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $check_dup_entry_status = $mysql_conn->query("
        SELECT
        DEVICE_NAME,
        SCHED_DAY,
        SCHED_TIME,
        SCHED_GENRE
        FROM schedule_scratch
        WHERE
        DEVICE_NAME='{$sched_entry_new[0]}'
        AND
        SCHED_DAY='{$sched_entry_new[1]}'
        AND
        SCHED_TIME='{$sched_entry_new[2]}'
        ")->num_rows;

    if($check_dup_entry_status > 0) {
        return 0;
    }

    $savesched_query = $mysql_conn->prepare("
        INSERT INTO
        schedule_scratch
        (DEVICE_NAME,
        SCHED_DAY,
        SCHED_TIME,
        SCHED_GENRE)
        VALUES
        (?, ?, ?, ?)
    ");

    $savesched_query->bind_param("ssss",
        $sched_entry_new[0],
        $sched_entry_new[1],
        $sched_entry_new[2],
        $sched_entry_new[3]);

    $savesched_query->execute();

    $savesched_query->close();

    $mysql_conn->close();

    return 1;
}

function UpdateScheduleEntry($sched_entry_orig, $sched_entry_update)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $check_dup_entry_status = $mysql_conn->query("
        SELECT
        DEVICE_NAME,
        SCHED_DAY,
        SCHED_TIME,
        SCHED_GENRE
        FROM schedule_scratch
        WHERE
        DEVICE_NAME='{$sched_entry_update[0]}'
        AND
        SCHED_DAY='{$sched_entry_update[1]}'
        AND
        SCHED_TIME='{$sched_entry_update[2]}'
        AND
        SCHED_GENRE='{$sched_entry_update[3]}'
        ")->num_rows;

    if($check_dup_entry_status > 0) {
        return 0;
    }

    $savesched_query = $mysql_conn->prepare("
        UPDATE
        schedule_scratch
        SET
        DEVICE_NAME=?,
        SCHED_DAY=?,
        SCHED_TIME=?,
        SCHED_GENRE=?
        WHERE
        DEVICE_NAME=?
        AND
        SCHED_DAY=?
        AND
        SCHED_TIME=?
        AND
        SCHED_GENRE=?
    ");

    $savesched_query->bind_param("ssssssss",
        $sched_entry_update[0],
        $sched_entry_update[1],
        $sched_entry_update[2],
        $sched_entry_update[3],
        $sched_entry_orig[0],
        $sched_entry_orig[1],
        $sched_entry_orig[2],
        $sched_entry_orig[3]);

    $savesched_query->execute();

    $savesched_query->close();

    $mysql_conn->close();

    return 1;
}

function DeleteScheduleEntryForDevice($sched_entry)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $delete_entry_query = $mysql_conn->prepare("
        DELETE
        FROM
        schedule_scratch
        WHERE
        DEVICE_NAME=?
        AND
        SCHED_DAY=?
        AND
        SCHED_TIME=?
        AND
        SCHED_GENRE=?
    ");

    $delete_entry_query->bind_param("ssss",
        $sched_entry[0],
        $sched_entry[1],
        $sched_entry[2],
        $sched_entry[3]
    );

    $delete_entry_query->execute();

    $delete_entry_query->close();

    $mysql_conn->close();

    return 1;
}

function FetchScheduleEntriesForDevice($device, $day)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    return $mysql_conn->query("
        SELECT
        SCHED_TIME,
        SCHED_GENRE
        FROM
        schedule_scratch
        WHERE
        DEVICE_NAME='{$device}'
        AND
        SCHED_DAY='{$day}'
        ORDER BY
        SCHED_TIME ASC
    ")->fetch_all(MYSQLI_ASSOC);
}

function SchedulePrepareForPushToDevice($device, $email)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');

    $days_str = array(
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
    );

    $schedule = "";

    foreach ($days_str as $day) {
        $get_schedule_query = $mysql_conn->prepare("
            SELECT
            SCHED_TIME,
            SCHED_GENRE
            FROM
            schedule_scratch
            WHERE
            DEVICE_NAME=?
            AND
            SCHED_DAY=?
            ORDER BY
            SCHED_TIME ASC
        ");

        $get_schedule_query->bind_param("ss", $device, $day);

        $get_schedule_query->execute();

        $get_schedule_query->bind_result(
            $sched_time_col,
            $sched_genre_col
        );

        while($get_schedule_query->fetch()) {
            $schedule[$day][$sched_time_col] = $sched_genre_col;
        }

        $get_schedule_query->close();
    }

    $schedule_json_data = json_encode($schedule, JSON_PRETTY_PRINT);

    $is_schedule_exist = $mysql_conn->query("
        SELECT
        DEVICE_NAME
        FROM
        schedule_device_push
        WHERE
        DEVICE_NAME='{$device}'
    ")->num_rows;

    if($is_schedule_exist > 0) {
        $timestamp = date("Y-m-d H:i:s");

        $save_schedule_push_query = $mysql_conn->prepare("
            UPDATE
            schedule_device_push
            SET
            SCHEDULE_DATA=?,
            PUSH_TO_DEVICE_STATUS=?,
            LAST_UPDATED_ON=?,
            LAST_UPDATED_BY=?
            WHERE
            DEVICE_NAME=?
        ");

        $push_set_update_pending = 1;

        $save_schedule_push_query->bind_param("sisss",
            $schedule_json_data,
            $push_set_update_pending,
            $timestamp,
            $email,
            $device
        );

        $save_schedule_push_query->execute();

        $save_schedule_push_query->close();
    }

    if($is_schedule_exist == 0) {
        $timestamp = date("Y-m-d H:i:s");

        $save_schedule_push_query = $mysql_conn->prepare("
            INSERT INTO
            schedule_device_push
            (DEVICE_NAME,
            SCHEDULE_DATA,
            PUSH_TO_DEVICE_STATUS,
            LAST_UPDATED_ON,
            LAST_UPDATED_BY)
            VALUES
            (?, ?, ?, ?, ?)
        ");

        $push_set_update_pending = 1;

        $save_schedule_push_query->bind_param("ssiss",
            $device,
            $schedule_json_data,
            $push_set_update_pending,
            $timestamp,
            $email
        );

        $save_schedule_push_query->execute();

        $save_schedule_push_query->close();
    }

    $feature_push_query = $mysql_conn->prepare("
            UPDATE
            features
            SET
            SCHEDULE_UPDATE_AVAILABLE=?
            WHERE
            DEVICE_NAME=?
        ");

    $push_set_update_pending = 1;

    $feature_push_query->bind_param("is",
        $push_set_update_pending,
        $device
    );

    $feature_push_query->execute();

    $feature_push_query->close();

    $mysql_conn->close();

    return 1;
}

function SetVenueEqualiserUpdateAvailable($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');

    $timestamp = date("Y-m-d H:i:s");

    $set_venue_eq_update_available = $mysql_conn->prepare("
            UPDATE
            features
            SET
            EQUALISER_TYPE_UPDATE_AVAILABLE=1,
            LAST_UPDATED_ON=?,
            LAST_UPDATED_BY=?
            WHERE
            DEVICE_NAME=?
        ");

    $set_venue_eq_update_available->bind_param("sss",
        $timestamp,
        $email,
        $device_name
    );

    $set_venue_eq_update_available->execute();

    $set_venue_eq_update_available->close();

    $mysql_conn->close();

    return 1;
}

function JinglesPrepareForPushToDevice($device, $email)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');

    $schedule = null;

    $get_schedule_query = $mysql_conn->prepare("
        SELECT
        AD_MEDIA_FILENAME
        FROM
        jingle_scratch
        WHERE
        DEVICE_NAME=?
        AND
        AD_MEDIA_FILENAME IS NULL
    ");

    $get_schedule_query->bind_param("s", $device);

    $get_schedule_query->execute();

    $get_schedule_query->bind_result(
        $sched_file_col
    );

    $get_schedule_query->store_result();

    $is_valid_schedule = $get_schedule_query->num_rows;

    if($is_valid_schedule > 0) {
        $get_schedule_query->close();

        $mysql_conn->close();

        return 2;
    }

    $get_schedule_query = $mysql_conn->prepare("
        SELECT
        AD_SCHED_TIME,
        AD_MEDIA_FILENAME
        FROM
        jingle_scratch
        WHERE
        DEVICE_NAME=?
        ORDER BY
        AD_SCHED_TIME ASC
    ");

    $get_schedule_query->bind_param("s", $device);

    $get_schedule_query->execute();

    $get_schedule_query->bind_result(
        $sched_time_col,
        $sched_file_col
    );

    while($get_schedule_query->fetch()) {
        $schedule[$sched_time_col] = $sched_file_col;
    }

    $get_schedule_query->close();

    $schedule_json_data = json_encode($schedule, JSON_PRETTY_PRINT);

    $is_schedule_exist = $mysql_conn->query("
        SELECT
        DEVICE_NAME
        FROM
        jingle_device_push
        WHERE
        DEVICE_NAME='{$device}'
    ")->num_rows;

    if($is_schedule_exist > 0) {
        $timestamp = date("Y-m-d H:i:s");

        $save_schedule_push_query = $mysql_conn->prepare("
            UPDATE
            jingle_device_push
            SET
            SCHEDULE_DATA=?,
            LAST_UPDATED_ON=?,
            LAST_UPDATED_BY=?
            WHERE
            DEVICE_NAME=?
        ");

        $save_schedule_push_query->bind_param("ssss",
            $schedule_json_data,
            $timestamp,
            $email,
            $device
        );

        $save_schedule_push_query->execute();

        $save_schedule_push_query->close();

        $save_schedule_push_query = $mysql_conn->prepare("
            UPDATE
            features
            SET
            JINGLE_UPDATE_AVAILABLE=?
            WHERE
            DEVICE_NAME=?
        ");

        $push_set_update_pending = 1;

        $save_schedule_push_query->bind_param("is",
            $push_set_update_pending,
            $device
        );

        $save_schedule_push_query->execute();

        $save_schedule_push_query->close();
    }

    if($is_schedule_exist == 0) {
        $timestamp = date("Y-m-d H:i:s");

        $save_schedule_push_query = $mysql_conn->prepare("
            INSERT INTO
            jingle_device_push
            (DEVICE_NAME,
            SCHEDULE_DATA,
            PUSH_TO_DEVICE_STATUS,
            LAST_UPDATED_ON,
            LAST_UPDATED_BY)
            VALUES
            (?, ?, ?, ?, ?)
        ");

        $push_set_update_pending = 1;

        $save_schedule_push_query->bind_param("ssiss",
            $device,
            $schedule_json_data,
            $push_set_update_pending,
            $timestamp,
            $email
        );

        $save_schedule_push_query->execute();

        $save_schedule_push_query->close();

        $save_schedule_push_query = $mysql_conn->prepare("
            UPDATE
            features
            SET
            JINGLE_UPDATE_AVAILABLE=?
            WHERE
            DEVICE_NAME=?
        ");

        $push_set_update_pending = 1;

        $save_schedule_push_query->bind_param("is",
            $push_set_update_pending,
            $device
        );

        $save_schedule_push_query->execute();

        $save_schedule_push_query->close();
    }

    $mysql_conn->close();

    return 1;
}

function GetDeviceListForTheVenue($email)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    return $mysql_conn->query("
        SELECT
        DEVICE_NAME
        FROM
        userslist,device
        WHERE
        userslist.CUSTOMER_ID = device.CUSTOMER_ID
        AND
        userslist.EMAIL='{$email}'
    ")->fetch_all(MYSQLI_NUM);
}

function GetAdvertisementStatusForDeviceCustomer($device_name, $email)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_ad_status_query = $mysql_conn->prepare("
        SELECT
        JINGLE
        FROM
        device,userslist,features
        WHERE
        userslist.CUSTOMER_ID = device.CUSTOMER_ID
        AND
        features.DEVICE_NAME = device.DEVICE_NAME
        AND
        features.DEVICE_NAME=?
        AND
        userslist.EMAIL=?
    ");

    $get_ad_status_query->bind_param("ss",$device_name, $email);

    $get_ad_status_query->execute();

    $get_ad_status_query->bind_result(
        $device_ad_status_col
    );

    while ($get_ad_status_query->fetch()) {
        return $device_ad_status_col;
    }

    $mysql_conn->close();

    return -1;
}

function GetAdvertisementStatusForDeviceAdmin($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_ad_status_query = $mysql_conn->prepare("
        SELECT
        JINGLE
        FROM
        features
        WHERE
        DEVICE_NAME=?
    ");

    $get_ad_status_query->bind_param("s",$device_name);

    $get_ad_status_query->execute();

    $get_ad_status_query->bind_result(
        $device_ad_status_col
    );

    while ($get_ad_status_query->fetch()) {
        return $device_ad_status_col;
    }

    $mysql_conn->close();

    return -1;
}

function GetAdPlaybackStatusForDeviceAdmin($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_ad_status_query = $mysql_conn->prepare("
        SELECT
        JINGLE_STATUS
        FROM
        features
        WHERE
        DEVICE_NAME=?
    ");

    $get_ad_status_query->bind_param("s",$device_name);

    $get_ad_status_query->execute();

    $get_ad_status_query->bind_result(
        $device_ad_status_col
    );

    while ($get_ad_status_query->fetch()) {
        return $device_ad_status_col;
    }

    $mysql_conn->close();

    return -1;
}

function GetHealthStatusOfDeviceAdmin($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_health_status_query = $mysql_conn->prepare("
        SELECT
        HB_LAST_UPDATE
        FROM
        device_health,device
        WHERE
        device_health.DEVICE_UUID = device.DEVICE_UUID
        AND
        DEVICE_NAME=?
    ");

    $get_health_status_query->bind_param("s",$device_name);

    $get_health_status_query->execute();

    $get_health_status_query->bind_result(
        $device_health_status_col
    );

    while ($get_health_status_query->fetch()) {
        return $device_health_status_col;
    }

    $mysql_conn->close();

    return -1;
}

function GetHealthStatusOfDeviceCustomer($device_name, $email)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_health_status_query = $mysql_conn->prepare("
        SELECT
        HB_LAST_UPDATE
        FROM
        device_health,device,userslist
        WHERE
        device_health.DEVICE_UUID = device.DEVICE_UUID
        AND
        userslist.CUSTOMER_ID = device.CUSTOMER_ID
        AND
        DEVICE_NAME=?
        AND
        userslist.EMAIL=?
    ");

    $get_health_status_query->bind_param("ss",$device_name, $email);

    $get_health_status_query->execute();

    $get_health_status_query->bind_result(
        $device_health_status_col
    );

    while ($get_health_status_query->fetch()) {
        return $device_health_status_col;
    }

    $mysql_conn->close();

    return -1;
}

function GetMusicFilesFromGenre($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_music_files_from_genre_query = $mysql_conn->prepare("
        SELECT
        GENRE
        FROM
        current_playing_track
        WHERE
        DEVICE_NAME=?
    ");

    $get_music_files_from_genre_query->bind_param("s", $device_name);

    $get_music_files_from_genre_query->execute();

    $get_music_files_from_genre_query->bind_result(
        $genre_col
    );

    while ($get_music_files_from_genre_query->fetch()) {
        return $genre_col;
    }

    $mysql_conn->close();

    return 0;
}

function GetTrackMetadataInformation($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    /*$db_conn = parse_ini_file("PHPDBConnectDO.ini");
    $mysql_conn = new mysqli();
    $mysql_conn->ssl_set("/etc/client-key.pem", "/etc/client-cert.pem", "/etc/ca-cert.pem", NULL, NULL);
    $mysql_conn->real_connect($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);*/

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_current_track_query = $mysql_conn->prepare("
        SELECT
        current_playing_track.FILENAME,
        PLAYBACK_START_TIME,
        DURATION,
        ID3V1_TRACKNAME,
        ID3V1_ARTIST,
        ID3V2_TRACKNAME,
        ID3V2_ARTIST
        FROM
        track_metadata,current_playing_track,device
        WHERE
        current_playing_track.DEVICE_NAME = device.DEVICE_NAME
        AND
        current_playing_track.FILENAME = track_metadata.FILENAME
        AND
        current_playing_track.DEVICE_NAME=?
    ");

    $get_current_track_query->bind_param("s", $device_name);

    $get_current_track_query->execute();

    $get_current_track_query->bind_result(
        $filename_col,
        $playback_start_time_col,
        $playtime_seconds_col,
        $id3v1_title_col,
        $id3v1_artist_col,
        $id3v2_title_col,
        $id3v2_artist_col
    );

    while ($get_current_track_query->fetch()) {
        return array(
            $filename_col,
            $playback_start_time_col,
            $playtime_seconds_col,
            $id3v1_title_col,
            $id3v1_artist_col,
            $id3v2_title_col,
            $id3v2_artist_col
        );
    }

    $mysql_conn->close();

    return null;
}

/*function GetThumbnailMetadataInformationForTrack($track)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    $db_conn = parse_ini_file("PHPDBConnectDO.ini");
    $mysql_conn = new mysqli();
    $mysql_conn->ssl_set("/etc/client-key.pem", "/etc/client-cert.pem", "/etc/ca-cert.pem", NULL, NULL);
    $mysql_conn->real_connect($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_current_track_query = $mysql_conn->prepare("
        SELECT
        ID3V1_THUMBNAIL_DATA,
        ID3V2_THUMBNAIL_DATA
        FROM
        media_metadata_thumbnail
        WHERE
        FILENAME=?
    ");

    $get_current_track_query->bind_param("s", $track);

    $get_current_track_query->execute();

    $get_current_track_query->bind_result(
        $id3v1_thumbnail_data_col,
        $id3v2_thumbnail_data_col
    );

    while ($get_current_track_query->fetch()) {
        return array(
            $id3v1_thumbnail_data_col,
            $id3v2_thumbnail_data_col
        );
    }

    $mysql_conn->close();

    return null;
}*/

function SetAdPlaybackStatusForDevice($device_name, $status)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $set_ad_status_query = $mysql_conn->prepare("
        UPDATE
        features
        SET
        JINGLE_STATUS=?
        WHERE
        DEVICE_NAME=?
    ");

    $set_ad_status_query->bind_param("is", $status, $device_name);

    $set_ad_status_query->execute();

    $mysql_conn->close();
}

function GetSubscriptionStatusForDeviceAdmin($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_subs_status_query = $mysql_conn->prepare("
        SELECT
        DEVICE_SUB_STATUS
        FROM
        device
        WHERE
        DEVICE_NAME=?
    ");

    $get_subs_status_query->bind_param("s",$device_name);

    $get_subs_status_query->execute();

    $get_subs_status_query->bind_result(
        $device_ad_status_col
    );

    while ($get_subs_status_query->fetch()) {
        return $device_ad_status_col;
    }

    $mysql_conn->close();

    return -1;
}

function GetSubscriptionStatusForDeviceCustomer($device_name, $email)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_subs_status_query = $mysql_conn->prepare("
        SELECT
        ENABLE_JOOKEBOX
        FROM
        device,venue,userslist,customer,features
        WHERE
        device.VENUE_ID = venue.VENUE_ID
        AND
        userslist.CUSTOMER_ID = customer.CUSTOMER_ID
        AND
        device.DEVICE_NAME = features.DEVICE_NAME
        AND
        userslist.EMAIL=?
        AND
        device.DEVICE_NAME=?
    ");

    $get_subs_status_query->bind_param("ss" ,$email, $device_name);

    $get_subs_status_query->execute();

    $get_subs_status_query->bind_result(
        $device_subs_status_col
    );

    $device_subs_status = null;

    while($get_subs_status_query->fetch()) {
        $device_subs_status = $device_subs_status_col;
    }

    $mysql_conn->close();

    return $device_subs_status_col;
}

function SetSubscriptionStatusForDevice($device_name, $status)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $set_subs_status_query = $mysql_conn->prepare("
        UPDATE
        features
        SET
        ENABLE_JOOKEBOX=?
        WHERE
        DEVICE_NAME=?
    ");

    $set_subs_status_query->bind_param("is", $status, $device_name);

    $set_subs_status_query->execute();

    $mysql_conn->close();
}

function StoreJingleMetadata($details)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $is_exist_query = $mysql_conn->prepare("
        SELECT
        JINGLE_FILENAME,
        JINGLE_LABEL
        FROM
        jingle_metadata
        WHERE
        (JINGLE_FILENAME=? OR JINGLE_LABEL=?)
        AND
        DEVICE_NAME=?
    ");

    $is_exist_query->bind_param("sss",
        $details[0],
        $details[1],
        $details[2]
    );

    $is_exist_query->execute();

    $is_exist_query->store_result();

    $does_exist = $is_exist_query->num_rows;

    if($does_exist > 0) {
        $is_exist_query->close();

        $mysql_conn->close();

        return 2;
    }

    $store_jingle_metadata_query = $mysql_conn->prepare("
        INSERT INTO
        jingle_metadata
        (JINGLE_FILENAME,
        JINGLE_LABEL,
        DEVICE_NAME,
        CREATED_BY,
        CREATED_ON)
        VALUES
        (?, ?, ?, ?, ?)
    ");

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $store_jingle_metadata_query->bind_param("sssss",
        $details[0],
        $details[1],
        $details[2],
        $details[3],
        $timestamp
    );

    $store_jingle_metadata_query->execute();

    $store_jingle_metadata_query->close();

    $mysql_conn->close();

    return 1;
}

function RemoveJingleWithLabel($device_name, $jingle_filename, $jingle_label)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $check_jingle_inuse_query = $mysql_conn->prepare("
        SELECT
        AD_MEDIA_FILENAME
        FROM
        jingle_scratch
        WHERE
        AD_MEDIA_FILENAME=?
        AND
        DEVICE_NAME=?
    ");

    $check_jingle_inuse_query->bind_param("ss", $jingle_filename, $device_name);

    $check_jingle_inuse_query->execute();

    $check_jingle_inuse_query->store_result();

    $is_inuse = $check_jingle_inuse_query->num_rows;

    if($is_inuse > 0) {
        $check_jingle_inuse_query->close();

        $mysql_conn->close();

        return 2;
    }

    $remove_jingle_query = $mysql_conn->prepare("
        DELETE
        FROM
        jingle_metadata
        WHERE
        JINGLE_FILENAME=?
        AND
        JINGLE_LABEL=?
        AND
        DEVICE_NAME=?
    ");

    $remove_jingle_query->bind_param("sss", $jingle_filename, $jingle_label, $device_name);

    $remove_jingle_query->execute();

    $remove_jingle_query->close();

    $mysql_conn->close();

    return 1;
}

function LoadJingleDetails($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $load_jingle_details_query = $mysql_conn->prepare("
        SELECT
        JINGLE_FILENAME,
        JINGLE_LABEL
        FROM
        jingle_metadata
        WHERE
        DEVICE_NAME=?
    ");

    $load_jingle_details_query->bind_param("s", $device_name);

    $load_jingle_details_query->execute();

    $load_jingle_details_query->bind_result(
        $jingle_filename_col,
        $jingle_label_col
    );

    $jingle_details = [];
    $i = 0;

    while ($load_jingle_details_query->fetch()) {
        $jingle_details[$i] = array(
            $jingle_filename_col,
            $jingle_label_col
        );
        $i++;
    }

    $load_jingle_details_query->close();

    $mysql_conn->close();

    return $jingle_details;
}

function AddOrUpdateJingleScheduleEntry($sched_entry_new)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');

    $check_dup_entry_status = $mysql_conn->query("
        SELECT
        DEVICE_NAME
        FROM jingle_schedule
        WHERE
        DEVICE_NAME='{$sched_entry_new[0]}'
        ")->num_rows;

    if($check_dup_entry_status > 0) {
        $savesched_query = $mysql_conn->prepare("
            UPDATE
            jingle_schedule
            SET
            JINGLE_START_TIME=?,
            JINGLE_END_TIME=?,
            JINGLE_INTERVAL=?,
            LAST_UPDATED_BY=?,
            LAST_UPDATED_ON=?
            WHERE
            DEVICE_NAME=?
        ");

        $timestamp = date("Y-m-d H:i:s");

        $savesched_query->bind_param("ssisss",
            $sched_entry_new[1],
            $sched_entry_new[2],
            $sched_entry_new[3],
            $_SESSION['emailID'],
            $timestamp,
            $sched_entry_new[0]
        );

        $savesched_query->execute();

        $savesched_query->close();
    }

    $savesched_query = $mysql_conn->prepare("
        INSERT INTO
        jingle_schedule
        (DEVICE_NAME,
        JINGLE_START_TIME,
        JINGLE_END_TIME,
        JINGLE_INTERVAL,
        LAST_UPDATED_BY,
        LAST_UPDATED_ON)
        VALUES
        (?, ?, ?, ?, ?, ?)
    ");

    $timestamp = date("Y-m-d H:i:s");

    $savesched_query->bind_param("sssiss",
        $sched_entry_new[0],
        $sched_entry_new[1],
        $sched_entry_new[2],
        $sched_entry_new[3],
        $_SESSION['emailID'],
        $timestamp
    );

    $savesched_query->execute();

    $savesched_query->close();

    $mysql_conn->close();

    return 1;
}

function LoadJingleScheduleAdmin($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $load_jingle_schedule_query = $mysql_conn->prepare("
        SELECT
        JINGLE_START_TIME,
        JINGLE_END_TIME,
        JINGLE_INTERVAL
        FROM
        jingle_schedule
        WHERE
        DEVICE_NAME=?
    ");

    $load_jingle_schedule_query->bind_param("s", $device_name);

    $load_jingle_schedule_query->execute();

    $load_jingle_schedule_query->bind_result(
        $jingle_start_time_col,
        $jingle_end_time_col,
        $jingle_interval_col
    );

    $jingle_schedule = [];

    while ($load_jingle_schedule_query->fetch()) {
        array_push(
            $jingle_schedule,
            $jingle_start_time_col,
            $jingle_end_time_col,
            $jingle_interval_col
        );
    }

    $load_jingle_schedule_query->close();

    $mysql_conn->close();

    return $jingle_schedule;
}

function LoadJingleScheduleCustomer($device_name, $email)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $load_jingle_schedule_query = $mysql_conn->prepare("
        SELECT
        JINGLE_START_TIME,
        JINGLE_END_TIME,
        JINGLE_INTERVAL
        FROM
        jingle_schedule,device,customer,userslist
        WHERE
        jingle_schedule.DEVICE_NAME = device.DEVICE_NAME
        AND
        customer.CUSTOMER_ID = device.CUSTOMER_ID
        AND
        userslist.CUSTOMER_ID = customer.CUSTOMER_ID
        AND
        userslist.EMAIL=?
        AND
        jingle_schedule.DEVICE_NAME=?
    ");

    $load_jingle_schedule_query->bind_param("ss", $email, $device_name);

    $load_jingle_schedule_query->execute();

    $load_jingle_schedule_query->bind_result(
        $jingle_start_time_col,
        $jingle_end_time_col,
        $jingle_interval_col
    );

    $jingle_schedule = [];

    while ($load_jingle_schedule_query->fetch()) {
        array_push(
            $jingle_schedule,
            $jingle_start_time_col,
            $jingle_end_time_col,
            $jingle_interval_col
        );
    }

    $load_jingle_schedule_query->close();

    $mysql_conn->close();

    return $jingle_schedule;
}

function CreateJingleSlotEntries($device_name, $schedule)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $delete_existing_query = $mysql_conn->prepare("
        DELETE
        FROM
        jingle_scratch
        WHERE
        DEVICE_NAME=?
    ");

    $delete_existing_query->bind_param("s", $device_name);

    $delete_existing_query->execute();

    $delete_existing_query->close();

    $create_schedule_entries_query = $mysql_conn->prepare("
        INSERT INTO
        jingle_scratch
        (DEVICE_NAME,
        AD_SCHED_TIME)
        VALUES
        (?, ?)
    ");

    foreach ($schedule as $slot) {
        $create_schedule_entries_query->bind_param("ss",
            $device_name,
            $slot
        );

        $create_schedule_entries_query->execute();
    }

    $create_schedule_entries_query->close();

    $mysql_conn->close();

    return 1;
}

function GetJingleSlotEntries($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_jingle_slots_query = $mysql_conn->prepare("
        SELECT
        AD_SCHED_TIME,
        AD_MEDIA_FILENAME
        FROM
        jingle_scratch
        WHERE
        DEVICE_NAME=?
    ");

    $get_jingle_slots_query->bind_param("s", $device_name);

    $get_jingle_slots_query->execute();

    $get_jingle_slots_query->bind_result(
        $ad_sched_time_col,
        $ad_media_filename_col
    );

    $jingle_slots = [];
    $i = 0;

    while ($get_jingle_slots_query->fetch()) {
        $jingle_slots[$i] = array(
            $ad_sched_time_col,
            $ad_media_filename_col
        );
        $i++;
    }

    $get_jingle_slots_query->close();

    $mysql_conn->close();

    return $jingle_slots;
}

function SetCommonJingleFileForAllSlot($device_name, $filename)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $set_common_jingle_query = $mysql_conn->prepare("
        UPDATE
        jingle_scratch
        SET
        AD_MEDIA_FILENAME=?
        WHERE
        DEVICE_NAME=?
    ");

    $set_common_jingle_query->bind_param("ss", $filename, $device_name);

    $set_common_jingle_query->execute();

    $set_common_jingle_query->close();

    $mysql_conn->close();

    return 1;
}

function SetIndividualJingleForEachSlot($device_name, $slots, $jingles, $items_count)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $delete_existing_query = $mysql_conn->prepare("
        DELETE
        FROM
        jingle_scratch
        WHERE
        DEVICE_NAME=?
    ");

    $delete_existing_query->bind_param("s", $device_name);

    $delete_existing_query->execute();

    $delete_existing_query->close();

    $set_ind_jingle_each_slot_query = $mysql_conn->prepare("
        INSERT INTO
        jingle_scratch
        (DEVICE_NAME,
        AD_SCHED_TIME,
        AD_MEDIA_FILENAME)
        VALUES
        (?, ?, ?)
    ");

    for ($i = 0; $i < $items_count; $i++) {
        $set_ind_jingle_each_slot_query->bind_param("sss",
            $device_name,
            $slots[$i],
            $jingles[$i]
        );

        $set_ind_jingle_each_slot_query->execute();
    }

    $set_ind_jingle_each_slot_query->close();

    $mysql_conn->close();

    return 1;
}

function LoadAssignedGenresForADevice($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $load_playlist_entries_query = $mysql_conn->prepare("
        SELECT
        SCHED_GENRE,
        COUNT(SCHED_GENRE) AS GENRE_USAGE
        FROM schedule_scratch
        WHERE DEVICE_NAME=?
        GROUP BY SCHED_GENRE;
    ");

    $load_playlist_entries_query->bind_param("s", $device_name);

    $load_playlist_entries_query->execute();

    $load_playlist_entries_query->bind_result(
        $playlist_name_col,
        $playlist_usage_col
    );

    $jingle_details = [];
    $i = 0;

    while ($load_playlist_entries_query->fetch()) {
        $jingle_details[$i] = array(
            $playlist_name_col,
            $playlist_usage_col
        );
        $i++;
    }

    $load_playlist_entries_query->close();

    $mysql_conn->close();

    return $jingle_details;
}

function LoadTrialEntries()
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $load_trial_entries_query = $mysql_conn->prepare("
        SELECT
        ENQUIRY_FULL_NAME,
        ENQUIRY_EMAIL,
        ENQUIRY_PHONE_NUMBER,
        ENQUIRY_TIMESTAMP
        FROM trial_enquiry
    ");

    $load_trial_entries_query->execute();

    $load_trial_entries_query->bind_result(
        $enquiry_full_name_col,
        $enquiry_email_col,
        $enquiry_phone_number_col,
        $enquiry_timestamp_col
    );

    $enquiry_details = [];
    $i = 0;

    while ($load_trial_entries_query->fetch()) {
        $enquiry_details[$i] = array(
            $enquiry_full_name_col,
            $enquiry_email_col,
            $enquiry_phone_number_col,
            $enquiry_timestamp_col
        );
        $i++;
    }

    $load_trial_entries_query->close();

    $mysql_conn->close();

    return $enquiry_details;
}

function ApproximateMusicSize($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $playlists = [];
    $i = 0;

    $approx_musicsize_query = $mysql_conn->prepare("
        SELECT
        DISTINCT SCHED_GENRE
        FROM
        schedule_scratch
        WHERE
        DEVICE_NAME=?
    ");

    $approx_musicsize_query->bind_param("s", $device_name);

    $approx_musicsize_query->execute();

    $approx_musicsize_query->bind_result(
        $playlist_col
    );

    while ($approx_musicsize_query->fetch()) {
        $playlists[$i] = $playlist_col;

        $i++;
    }

    $approx_musicsize_query->close();

    // Music size calculation part

    $s3client = new Aws\S3\S3Client([
        'version' => 'latest',
        'region' => 'sgp1',
        'endpoint' => 'https://sgp1.digitaloceanspaces.com',
        'credentials' => array(
            'key' => "5JH6DTDVPM5MXN66UUJJ",
            'secret' => "jRsjcd3XC9IwT3a3PvV9l6JOEalHrNq8cguuaE5uYcQ"
        )
    ]);

    $music_files_list = [];
    $y = 0;

    foreach ($playlists as $dev_playlist) {
        $s3client->getObject(array(
            'Bucket' => 'jookebox-playlists',
            'Key' => $dev_playlist,
            'SaveAs' => '/tmp/' . $dev_playlist
        ));
        $playlist_music = file("/tmp/" . $dev_playlist, FILE_IGNORE_NEW_LINES);
        foreach ($playlist_music as $music_file) {
            if(!strstr($music_file, "#EXT") && (strstr($music_file, ".mp3") || strstr($music_file, ".m4a") || strstr($music_file, ".M4A"))) {
                $music_files_list[$y] = $music_file;

                $y++;
            }
        }
        unlink("/tmp/" . $dev_playlist);
    }

    $final_music_list = array_unique($music_files_list, SORT_STRING);

    $file_name_list = "";
    $total_files_count = count($final_music_list);

    for ($x = 0; $x < $total_files_count; $x++) {
        if ($x == 0) {
            $file_name_list .= "'" . mysqli_real_escape_string($mysql_conn, $final_music_list[$x]) . "',";
            continue;
        }

        if ($x == ($total_files_count - 1)) {
            $file_name_list .= "'" . mysqli_real_escape_string($mysql_conn, $final_music_list[$x]) . "'";
            continue;
        }

        $file_name_list .= "'" . mysqli_real_escape_string($mysql_conn, $final_music_list[$x]) . "',";
    }

    $results = $mysql_conn->query("SELECT SUM(FILESIZE) FROM track_metadata WHERE BINARY FILENAME IN ({$file_name_list})")->fetch_array(MYSQLI_NUM);

    $mysql_conn->close();

    return $results[0];
}

function GetCurrentSongQueueAdmin($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $music_queue_details = [];

    $music_queue_query = $mysql_conn->prepare("
        SELECT
        MUSIC_QUEUE,
        MUSIC_QUEUE_COUNT
        FROM
        current_playing_track
        WHERE
        DEVICE_NAME=?
        AND
        MUSIC_QUEUE IS NOT NULL
    ");

    $music_queue_query->bind_param("s", $device_name);

    $music_queue_query->execute();

    $music_queue_query->bind_result(
        $music_queue_col,
        $music_queue_count_col
    );

    while ($music_queue_query->fetch()) {
        $music_queue_details[0] = $music_queue_col;
        $music_queue_details[1] = $music_queue_count_col;
    }

    $music_queue_query->close();

    $mysql_conn->close();

    return $music_queue_details;
}

function TotalScheduleEntries($device_name, $day_of_the_week)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $total_entries = null;

    $total_entries_query = $mysql_conn->prepare("
        SELECT
        COUNT(SCHED_GENRE) AS TOTAL_ENTRIES
        FROM
        schedule_scratch
        WHERE
        DEVICE_NAME=?
        AND
        SCHED_DAY=?
    ");

    $total_entries_query->bind_param("ss", $device_name, $day_of_the_week);

    $total_entries_query->execute();

    $total_entries_query->bind_result(
        $total_entries_col
    );

    while ($total_entries_query->fetch()) {
        $total_entries = $total_entries_col;
    }

    $total_entries_query->close();

    $mysql_conn->close();

    return $total_entries;
}

function LoadAllVenueNames($device_name_to_exclude)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $venues_list = [];
    $i = 0;

    $get_venues_query = $mysql_conn->prepare("
        SELECT
        venue.VENUE_ID,
        VENUE_NAME
        FROM
        venue,device
        WHERE
        venue.VENUE_ID = device.VENUE_ID
        AND
        DEVICE_NAME != ?
        ORDER BY VENUE_NAME
        ASC
    ");

    $get_venues_query->bind_param("s", $device_name_to_exclude);

    $get_venues_query->execute();

    $get_venues_query->bind_result(
        $venue_id_col,
        $venue_name_col
    );

    while ($get_venues_query->fetch()) {
        $venues_list[$i] = array(
            $venue_id_col,
            $venue_name_col
        );

        $i++;
    }

    $get_venues_query->close();

    $mysql_conn->close();

    return $venues_list;
}

function ClonePlaylistFromVenueToDevice($source_venue_id, $dest_device_id)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $new_schedule_data = [];
    $i = 0;

    $fetch_playlist_entries_query = $mysql_conn->prepare("
        SELECT
        SCHED_DAY,
        SCHED_TIME,
        SCHED_GENRE
        FROM
        schedule_scratch,device
        WHERE
        schedule_scratch.DEVICE_NAME = device.DEVICE_NAME
        AND
        device.VENUE_ID = ?
    ");

    $fetch_playlist_entries_query->bind_param("i", $source_venue_id);

    $fetch_playlist_entries_query->execute();

    $fetch_playlist_entries_query->bind_result(
        $sched_day_col,
        $sched_time_col,
        $sched_genre_col
    );

    while ($fetch_playlist_entries_query->fetch()) {
        $new_schedule_data[$i] = array(
            $dest_device_id,
            $sched_day_col,
            $sched_time_col,
            $sched_genre_col
        );

        $i++;
    }

    $fetch_playlist_entries_query->close();

    $delete_all_old_schedule_query = $mysql_conn->prepare("
        DELETE
        FROM
        schedule_scratch
        WHERE
        DEVICE_NAME=?
    ");

    $delete_all_old_schedule_query->bind_param("s", $dest_device_id);

    $delete_all_old_schedule_query->execute();

    $delete_all_old_schedule_query->close();

    $new_schedule_query = $mysql_conn->prepare("
        INSERT INTO
        schedule_scratch
        (DEVICE_NAME,
        SCHED_DAY,
        SCHED_TIME,
        SCHED_GENRE)
        VALUES
        (?, ?, ?, ?)
    ");

    foreach ($new_schedule_data as $schedule_entry) {
        $new_schedule_query->bind_param("ssss",
            $schedule_entry[0],
            $schedule_entry[1],
            $schedule_entry[2],
            $schedule_entry[3]
        );

        $new_schedule_query->execute();
    }

    $new_schedule_query->close();

    $mysql_conn->close();

    return 1;
}

function LoadAssignedPlaylistsForThisDevice($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $playlists_assigned = [];
    $i = 0;

    $get_assigned_playlist_query = $mysql_conn->prepare("
        SELECT
        DISTINCT SCHED_GENRE
        FROM
        schedule_scratch
        WHERE
        DEVICE_NAME=?
        ORDER BY
        SCHED_GENRE
        ASC
    ");

    $get_assigned_playlist_query->bind_param("s", $device_name);

    $get_assigned_playlist_query->execute();

    $get_assigned_playlist_query->bind_result(
        $sched_genre_col
    );

    while ($get_assigned_playlist_query->fetch()) {
        $playlists_assigned[$i] = $sched_genre_col;

        $i++;
    }

    $get_assigned_playlist_query->close();

    $mysql_conn->close();

    return $playlists_assigned;
}

function SaveNewEqualiserPreset($preset_name, $preset_settings)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $is_existing = $mysql_conn->query("SELECT 1 FROM equaliser_presets WHERE EQUALISER_PRESET_NAME='{$preset_name}'")->num_rows;

    if($is_existing > 0) {
        $update_preset_query = $mysql_conn->prepare("
            UPDATE
            equaliser_presets
            SET
            EQUALISER_PRESET_VALUES=?,
            LAST_UPDATED_BY=?,
            LAST_UPDATED_ON=?
            WHERE
            EQUALISER_PRESET_NAME=?
        ");

        $preset_settings_json = json_encode($preset_settings);

        date_default_timezone_set('Asia/Kolkata');
        $timestamp = date("Y-m-d H:i:s");

        $update_preset_query->bind_param("ssss", $preset_settings_json, $_SESSION['emailID'], $timestamp, $preset_name);

        $update_preset_query->execute();

        $update_preset_query->close();

        $mysql_conn->close();

        return 1;
    }

    $save_preset_query = $mysql_conn->prepare("
        INSERT INTO
        equaliser_presets
        (EQUALISER_PRESET_NAME,
        EQUALISER_PRESET_VALUES,
        CREATED_BY,
        CREATED_ON)
        VALUES
        (?, ?, ?, ?)
    ");

    $preset_settings_json = json_encode($preset_settings);

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $save_preset_query->bind_param("ssss", $preset_name, $preset_settings_json, $_SESSION['emailID'], $timestamp);

    $save_preset_query->execute();

    $save_preset_query->close();

    $mysql_conn->close();

    return 0;
}

function UpdateEqualiserPreset($preset_name, $preset_settings)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $is_existing = $mysql_conn->query("SELECT 1 FROM equaliser_presets WHERE EQUALISER_PRESET_NAME='{$preset_name}'")->num_rows;

    if($is_existing > 0) {
        $update_preset_query = $mysql_conn->prepare("
            UPDATE
            equaliser_presets
            SET
            EQUALISER_PRESET_VALUES=?,
            LAST_UPDATED_BY=?,
            LAST_UPDATED_ON=?
            WHERE
            EQUALISER_PRESET_NAME=?
        ");

        $preset_settings_json = json_encode($preset_settings);

        date_default_timezone_set('Asia/Kolkata');
        $timestamp = date("Y-m-d H:i:s");

        $update_preset_query->bind_param("ssss", $preset_settings_json, $_SESSION['emailID'], $timestamp, $preset_name);

        $update_preset_query->execute();

        $update_preset_query->close();

        $mysql_conn->close();

        return 1;
    }

    $mysql_conn->close();

    return 0;
}

function LoadEqualiserPreset($preset_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $save_preset_query = $mysql_conn->prepare("
        SELECT
        EQUALISER_PRESET_VALUES
        FROM
        equaliser_presets
        WHERE
        EQUALISER_PRESET_NAME=?
    ");

    $save_preset_query->bind_param("s", $preset_name);

    $save_preset_query->execute();

    $save_preset_query->bind_result(
        $equaliser_preset_values_col
    );

    while ($save_preset_query->fetch()) {
        return $equaliser_preset_values_col;
    }

    $save_preset_query->close();

    $mysql_conn->close();

    return null;
}

function LoadAllEqualiserPresets()
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $equaliser_preset_names = [];

    $load_all_presets_query = $mysql_conn->prepare("
        SELECT
        EQUALISER_PRESET_NAME
        FROM
        equaliser_presets
    ");

    $load_all_presets_query->execute();

    $load_all_presets_query->bind_result(
        $preset_name_col
    );

    $i = 0;

    while ($load_all_presets_query->fetch()) {
        $equaliser_preset_names[$i] = $preset_name_col;
        $i++;
    }

    $load_all_presets_query->close();

    $mysql_conn->close();

    return $equaliser_preset_names;
}

function SetGenreEqualiserDefaultMapping()
{
    require_once "/opt/aws-php/aws-autoloader.php";

    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $s3client = new Aws\S3\S3Client([
        'version' => 'latest',
        'region' => 'sgp1',
        'endpoint' => 'https://sgp1.digitaloceanspaces.com',
        'credentials' => array(
            'key' => "5JH6DTDVPM5MXN66UUJJ",
            'secret' => "jRsjcd3XC9IwT3a3PvV9l6JOEalHrNq8cguuaE5uYcQ"
        )
    ]);

    $aws_playlists = $s3client->getIterator('ListObjects', array(
        'Bucket' => "jookebox-playlists",
    ));

    $aws_genre_list = [];
    $x = 0;

    foreach ($aws_playlists as $playlist) {
        if ($playlist['Size'] > 0) {
            $remote = explode("/", $playlist['Key']);
            $remote_filename = array_pop($remote);

            $aws_genre_list[$x] = $remote_filename;
            $x++;
        }
    }

    $mysql_conn->query("DELETE FROM equaliser_genre_mapping");

    $default_mapping = [];

    foreach ($aws_genre_list as $genre) {
        array_push($default_mapping, array($genre, "Flat"));
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $reset_default_query = $mysql_conn->prepare("
        INSERT INTO
        equaliser_genre_mapping
        (GENRE,
        EQUALISER_PRESET_NAME,
        LAST_UPDATED_BY,
        LAST_UPDATED_ON)
        VALUES
        (?, ?, ?, ?)
    ");

    foreach ($default_mapping as $mapping) {
        $reset_default_query->bind_param("ssss",
            $mapping[0],
            $mapping[1],
            $_SESSION['emailID'],
            $timestamp);

        $reset_default_query->execute();
    }

    $reset_default_query->close();

    $mysql_conn->close();

    return 0;
}

function SetVenueEqualiserDefaultMapping()
{

    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $preset_name="Flat";

    $set_default_equaliser_for_all_venues_query = $mysql_conn->prepare("
        UPDATE
        equaliser_venue_mapping
        SET
        EQUALISER_PRESET_NAME=?,
        LAST_UPDATED_BY=?,
        LAST_UPDATED_ON=?
    ");

    $set_default_equaliser_for_all_venues_query->bind_param("sss",
        $preset_name,
        $_SESSION['emailID'],
        $timestamp);

    $set_default_equaliser_for_all_venues_query->execute();

    $set_default_equaliser_for_all_venues_query->close();

    $mysql_conn->close();

    return 0;
}

function SetGenreEqualiserCustomMapping($custom_mapping)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $mysql_conn->query("DELETE FROM equaliser_genre_mapping");

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $reset_default_query = $mysql_conn->prepare("
        INSERT INTO
        equaliser_genre_mapping
        (GENRE,
        EQUALISER_PRESET_NAME,
        LAST_UPDATED_BY,
        LAST_UPDATED_ON)
        VALUES
        (?, ?, ?, ?)
    ");

    foreach ($custom_mapping as $mapping) {
        $reset_default_query->bind_param("ssss",
            $mapping[0],
            $mapping[1],
            $_SESSION['emailID'],
            $timestamp);

        $reset_default_query->execute();
    }

    $reset_default_query->close();

    $mysql_conn->close();

    return 0;
}

function SetVenueEqualiserCustomMapping($custom_mapping)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $mysql_conn->query("DELETE FROM equaliser_venue_mapping");

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $set_venue_equaliser_custom_mapping_query = $mysql_conn->prepare("
        INSERT INTO
        equaliser_venue_mapping
        (DEVICE_NAME,
        EQUALISER_PRESET_NAME,
        LAST_UPDATED_BY,
        LAST_UPDATED_ON)
        VALUES
        (?, ?, ?, ?)
    ");

    foreach ($custom_mapping as $mapping) {
        $set_venue_equaliser_custom_mapping_query->bind_param("ssss",
            $mapping[0],
            $mapping[1],
            $_SESSION['emailID'],
            $timestamp);

        $set_venue_equaliser_custom_mapping_query->execute();
    }

    $set_venue_equaliser_custom_mapping_query->close();

    $mysql_conn->close();

    return 0;
}

function SetVenueEqualiserUpdateAvailableOnMapping($custom_mapping)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $set_venue_equaliser_update_available_query = $mysql_conn->prepare("
        UPDATE
        features
        SET
        EQUALISER_TYPE_UPDATE_AVAILABLE=1,
        LAST_UPDATED_BY=?,
        LAST_UPDATED_ON=?
        WHERE 
        DEVICE_NAME=?
    ");

    foreach ($custom_mapping as $mapping) {
        $set_venue_equaliser_update_available_query->bind_param("sss",
            $_SESSION['emailID'],
            $timestamp,
            $mapping[0]);

        $set_venue_equaliser_update_available_query->execute();
    }

    $set_venue_equaliser_update_available_query->close();

    $mysql_conn->close();

    return 0;
}

function LoadEqualiserGenreMapping()
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $load_mapping_query = $mysql_conn->prepare("
        SELECT
        GENRE,
        EQUALISER_PRESET_NAME
        FROM
        equaliser_genre_mapping
        ORDER BY
        GENRE
        ASC
    ");

    $load_mapping_query->execute();

    $load_mapping_query->bind_result(
        $genre_col,
        $equaliser_preset_name_col
    );

    $equaliser_genre_mapping = [];

    while ($load_mapping_query->fetch()) {
        array_push($equaliser_genre_mapping, array($genre_col, $equaliser_preset_name_col));
    }

    $load_mapping_query->close();

    $mysql_conn->close();

    return $equaliser_genre_mapping;
}

function GetEqualiserPerVenueList()
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $load_venue_per_equaliser = $mysql_conn->prepare("
        SELECT
        VENUE_NAME,
        features.DEVICE_NAME,
        equaliser_venue_mapping.EQUALISER_PRESET_NAME
        FROM
        venue,features,device,equaliser_venue_mapping
        WHERE 
        device.VENUE_ID = venue.VENUE_ID
        AND 
        device.DEVICE_NAME = features.DEVICE_NAME
        AND 
        device.DEVICE_NAME = equaliser_venue_mapping.DEVICE_NAME
    ");

    $load_venue_per_equaliser->execute();

    $load_venue_per_equaliser->store_result();
    if($load_venue_per_equaliser->num_rows <= 0) {
        return 0;
    }

    $meta = $load_venue_per_equaliser->result_metadata();
    while ($field = $meta->fetch_field())
    {
        $params[] = &$row[$field->name];
    }

    call_user_func_array(array($load_venue_per_equaliser, 'bind_result'), $params);

    while ($load_venue_per_equaliser->fetch()) {
        foreach($row as $key => $val)
        {
            $c[$key] = $val;
        }
        $result_arr[] = $c;
    }

    $load_venue_per_equaliser->close();

    $mysql_conn->close();

    return $result_arr;
}

function LoadExistingVenuesFromEqualiserMapping()
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $load_venue_per_equaliser = $mysql_conn->prepare("
        SELECT
        VENUE_NAME,
        features.DEVICE_NAME
        FROM
        venue,features,device
        WHERE
        device.DEVICE_NAME NOT IN (SELECT
        DEVICE_NAME
        FROM equaliser_venue_mapping)
        AND
        device.VENUE_ID = venue.VENUE_ID
        AND
        device.DEVICE_NAME = features.DEVICE_NAME
    ");

    $load_venue_per_equaliser->execute();

    $load_venue_per_equaliser->store_result();
    if($load_venue_per_equaliser->num_rows <= 0) {
        return 0;
    }

    $meta = $load_venue_per_equaliser->result_metadata();
    while ($field = $meta->fetch_field())
    {
        $params[] = &$row[$field->name];
    }

    call_user_func_array(array($load_venue_per_equaliser, 'bind_result'), $params);

    while ($load_venue_per_equaliser->fetch()) {
        foreach($row as $key => $val)
        {
            $c[$key] = $val;
        }
        $result_arr[] = $c;
    }

    $load_venue_per_equaliser->close();

    $mysql_conn->close();

    return $result_arr;
}

function LoadExistingGenresFromEqualiserMapping()
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $load_device_genre_query = $mysql_conn->prepare("
        SELECT
        GENRE
        FROM
        equaliser_genre_mapping
    ");

    $load_device_genre_query->execute();

    $load_device_genre_query->bind_result($genre_col);

    $device_genre_arr = [];
    $x = 0;

    while ($load_device_genre_query->fetch()) {
        $device_genre_arr[$x] = $genre_col;

        $x++;
    }

    $load_device_genre_query->close();

    $mysql_conn->close();

    return $device_genre_arr;
}

function AddNewGenreFromCloudToDevice($genre_name, $preset_assigned)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $add_new_preset_genre = $mysql_conn->prepare("
        INSERT INTO
        equaliser_genre_mapping
        (GENRE,
        EQUALISER_PRESET_NAME,
        LAST_UPDATED_BY,
        LAST_UPDATED_ON)
        VALUES
        (?, ?, ?, ?)
    ");

    $add_new_preset_genre->bind_param("ssss",
        $genre_name,
        $preset_assigned,
        $_SESSION['emailID'],
        $timestamp);

    $add_new_preset_genre->execute();

    $add_new_preset_genre->close();

    $mysql_conn->close();

    return 0;
}

function SetVenueEqualiserDefault($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $preset_assigned = "Flat";

    $is_venue_entry_exists = $mysql_conn->query("
        SELECT
        DEVICE_NAME
        FROM
        equaliser_venue_mapping
        WHERE
        DEVICE_NAME='{$device_name}'
    ")->num_rows;

    if($is_venue_entry_exists > 0) {

        $mysql_conn->close();

        return 0;
    }

    $set_default_preset_for_venue = $mysql_conn->prepare("
        INSERT INTO
        equaliser_venue_mapping
        (DEVICE_NAME,
        EQUALISER_PRESET_NAME,
        LAST_UPDATED_BY,
        LAST_UPDATED_ON)
        VALUES
        (?, ?, ?, ?)
    ");

    $set_default_preset_for_venue->bind_param("ssss",
        $device_name,
        $preset_assigned,
        $_SESSION['emailID'],
        $timestamp);

    $set_default_preset_for_venue->execute();

    $set_default_preset_for_venue->close();

    $mysql_conn->close();

    return 0;
}

function AddDefaultEqualiserToVenue($device_name, $preset_assigned)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $add_default_eq_to_venue = $mysql_conn->prepare("
        INSERT INTO
        equaliser_venue_mapping
        (DEVICE_NAME,
        EQUALISER_PRESET_NAME,
        LAST_UPDATED_BY,
        LAST_UPDATED_ON)
        VALUES
        (?, ?, ?, ?)
    ");

    $add_default_eq_to_venue->bind_param("ssss",
        $device_name,
        $preset_assigned,
        $_SESSION['emailID'],
        $timestamp);

    $add_default_eq_to_venue->execute();

    $add_default_eq_to_venue->close();

    $mysql_conn->close();

    return 0;
}

function EqualiserGenreMappingPreparePushToDevice()
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $load_mapping_query = $mysql_conn->prepare("
        SELECT
        GENRE,
        EQUALISER_PRESET_NAME
        FROM
        equaliser_genre_mapping
        ORDER BY
        GENRE
        ASC
    ");

    $load_mapping_query->execute();

    $load_mapping_query->bind_result(
        $genre_col,
        $equaliser_preset_name_col
    );

    $equaliser_genre_mapping = "";

    while ($load_mapping_query->fetch()) {
        $equaliser_genre_mapping[$genre_col] = $equaliser_preset_name_col;
    }

    $load_mapping_query->close();

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $mapping_push_query = $mysql_conn->prepare("
        INSERT INTO
        equaliser_genre_push
        (MAPPING_SETTING,
        LAST_UPDATED_BY,
        LAST_UPDATED_ON)
        VALUES
        (?, ?, ?) 
    ");

    $mapping_push_query->bind_param("sss", json_encode($equaliser_genre_mapping, JSON_PRETTY_PRINT), $_SESSION['emailID'], $timestamp);

    $mapping_push_query->execute();

    $mapping_push_query->close();

    $eq_set_update_query = $mysql_conn->prepare("
        UPDATE
        features
        SET
        EQUALISER_UPDATE_AVAILABLE=1,
        LAST_UPDATED_BY=?,
        LAST_UPDATED_ON=?
    ");

    $eq_set_update_query->bind_param("ss", $_SESSION['emailID'], $timestamp);

    $eq_set_update_query->execute();

    $eq_set_update_query->close();

    $mysql_conn->close();
}

function GetFeaturesListByDevice($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $features_list_query = $mysql_conn->prepare("
        SELECT
        DEVICE_NAME,
        ENABLE_JOOKEBOX,
        MUSIC,
        JINGLE,
        JINGLE_STATUS,
        WISH,
        SCHEDULE_UPDATE_AVAILABLE,
        JINGLE_UPDATE_AVAILABLE,
        EQUALISER_UPDATE_AVAILABLE,
        SOFTWARE_UPDATE_AVAILABLE,
        CONNECT_VPN,
        DISCONNECT_VPN,
        WIFI_UPDATE_AVAILABLE,
        PLAYLIST_TRACK_ACCESS
        FROM
        features
        WHERE
        DEVICE_NAME=?
    ");

    $features_list_query->bind_param("s", $device_name);

    $features_list_query->execute();

    $features_list_query->store_result();
    if($features_list_query->num_rows <= 0) {
        return 0;
    }

    $meta = $features_list_query->result_metadata();
    while ($field = $meta->fetch_field())
    {
        $params[] = &$row[$field->name];
    }

    call_user_func_array(array($features_list_query, 'bind_result'), $params);

    while ($features_list_query->fetch()) {
        foreach($row as $key => $val)
        {
            $c[$key] = $val;
        }
        $result_arr[] = $c;
    }

    $features_list_query->close();

    $mysql_conn->close();

    return $result_arr;
}

function LoadTracksListByPlaylistName($playlist)
{
    require_once "/opt/aws-php/aws-autoloader.php";

    $spacesClient = new Aws\S3\S3Client([
        'version' => 'latest',
        'region' => 'sgp1',
        'endpoint' => 'https://sgp1.digitaloceanspjookeboxdev@database.jookebox.inaces.com',
        'credentials' => array(
            'key' => "5JH6DTDVPM5MXN66UUJJ",
            'secret' => "jRsjcd3XC9IwT3a3PvV9l6JOEalHrNq8cguuaE5uYcQ"
        )
    ]);

    $spacesClient->getObject(array(
        'Bucket' => 'jookebox-playlists',
        'Key' => $playlist,
        'SaveAs' => "/tmp/" . $playlist
    ));

    $tracks_list = [];
    $x = 0;

    $music_list = file("/tmp/" . $playlist, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    foreach ($music_list as $music) {
        if (!strstr($music, "#EXT") && (strstr($music, ".mp3") || strstr($music, ".m4a") || strstr($music, ".M4A"))) {
            $tracks_list[$x] = $music;
            $x++;
        }
    }

    unlink("/tmp/" . $playlist);

    return $tracks_list;
}

function GetAdPlaybackStatusForDeviceCustomer($device_name, $email)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_ad_status_query = $mysql_conn->prepare("
        SELECT
        JINGLE_STATUS
        FROM
        device,userslist,features
        WHERE
        userslist.CUSTOMER_ID = device.CUSTOMER_ID
        AND
        features.DEVICE_NAME = device.DEVICE_NAME
        AND
        features.DEVICE_NAME=?
        AND
        userslist.EMAIL=?
    ");

    $get_ad_status_query->bind_param("ss",$device_name, $email);

    $get_ad_status_query->execute();

    $get_ad_status_query->bind_result(
        $device_ad_status_col
    );

    while ($get_ad_status_query->fetch()) {
        return $device_ad_status_col;
    }

    $mysql_conn->close();

    return -1;
}