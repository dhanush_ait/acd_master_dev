<?php

function getListOfAllCustomers()
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $list_customers_query = $mysql_conn->prepare("
        SELECT
        device.DEVICE_NAME,
        EQUALISER_TYPE,
        DEVICE_UUID,
        VENUE_NAME,
        ETH_MAC_ID,
        WLAN_MAC_ID,
        SUBSCRIPTION_START_DATE,
        SUBSCRIPTION_END_DATE,
        CONTRACT_TERMTYPE_CODE,
        COMPANY_NAME,
        VENUE_GSTIN
        FROM
        device,
        features,
        venue,
        customer,
        network_info,
        subscription
        WHERE
        device.VENUE_ID = venue.VENUE_ID
        AND
        device.CUSTOMER_ID = customer.CUSTOMER_ID
        AND 
        device.DEVICE_NAME = network_info.DEVICE_NAME
        AND
        device.DEVICE_NAME = subscription.DEVICE_NAME
        AND 
        device.DEVICE_NAME = features.DEVICE_NAME
        ORDER BY
        device.DEVICE_NAME
        ASC
    ");

    $list_customers_query->execute();

    $list_customers_query->store_result();
    if($list_customers_query->num_rows <= 0) {
        return 0;
    }

    $meta = $list_customers_query->result_metadata();
    while ($field = $meta->fetch_field())
    {
        $params[] = &$row[$field->name];
    }

    call_user_func_array(array($list_customers_query, 'bind_result'), $params);

    while ($list_customers_query->fetch()) {
        foreach($row as $key => $val)
        {
            $c[$key] = $val;
        }
        $result_arr[] = $c;
    }

    $list_customers_query->close();
    $mysql_conn->close();

    return $result_arr;
}

function GetCustomerData($venue_id)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_customer_info_query = $mysql_conn->prepare("
        SELECT
        venue.VENUE_NAME,
        venue.VENUE_TYPE,
        venue.VENUE_ADDRESS_STREET,
        venue.VENUE_CITY,
        venue.VENUE_STATE,
        venue.VENUE_COUNTRY,
        customer.COMPANY_NAME,
        customer.COMPANY_ADDRESS_STREET,
        customer.COMPANY_CITY,
        customer.COMPANY_STATE,
        customer.COMPANY_COUNTRY,
        venue.POC_FULL_NAME,
        venue.POC_EMAIL,
        venue.POC_MOBILE_NUMBER,
        venue.REMARKS,
        venue.VENUE_GSTIN
        FROM
        customer,venue
        WHERE
        customer.CUSTOMER_ID = venue.CUSTOMER_ID
        AND
        VENUE_ID = ?
    ");

    $get_customer_info_query->bind_param("i",
        $venue_id
    );

    $get_customer_info_query->execute();

    $customer_data = [];

    $get_customer_info_query->bind_result(
        $venue_name_col,
        $venue_type_col,
        $venue_address_street_col,
        $venue_city_col,
        $venue_state_col,
        $venue_country_col,
        $company_name_col,
        $company_address_street_col,
        $company_city_col,
        $company_state_col,
        $company_country_col,
        $poc_full_name_col,
        $poc_email_col,
        $poc_mobile_number_col,
        $remarks_col,
        $gstin_col
    );

    while($get_customer_info_query->fetch()) {
        $customer_data = array(
            $venue_name_col,
            $venue_type_col,
            $venue_address_street_col,
            $venue_city_col,
            $venue_state_col,
            $venue_country_col,
            $company_name_col,
            $company_address_street_col,
            $company_city_col,
            $company_state_col,
            $company_country_col,
            $poc_full_name_col,
            $poc_email_col,
            $poc_mobile_number_col,
            $remarks_col,
            $gstin_col
        );
    }

    $mysql_conn->close();

    return $customer_data;
}

function UpdateCustomerData($details, $venue_id)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $register_query = $mysql_conn->prepare("
        UPDATE
        venue
        SET
        VENUE_NAME=?,
        VENUE_TYPE=?,
        VENUE_GSTIN=?,
        VENUE_ADDRESS_STREET=?,
        VENUE_CITY=?,
        VENUE_STATE=?,
        VENUE_COUNTRY=?,
        POC_FULL_NAME=?,
        POC_EMAIL=?,
        POC_MOBILE_NUMBER=?,
        REMARKS=?,
        LAST_UPDATED_ON=?,
        LAST_UPDATED_BY=?
        WHERE
        VENUE_ID=?
    ");

    $register_query->bind_param("sisssssssssssi",
        $details[0],
        $details[1],
        $details[10],
        $details[2],
        $details[3],
        $details[4],
        $details[5],
        $details[6],
        $details[7],
        $details[8],
        $details[9],
        $timestamp,
        $_SESSION['emailID'],
        $venue_id
    );

    $register_query->execute();

    $mysql_conn->close();
}

function GetDeviceListByVenueID($venue_id)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_dev_list_query = $mysql_conn->prepare("
        SELECT
        DEVICE_NAME,
        DEVICE_UUID
        FROM
        device
        WHERE
        VENUE_ID = ?
    ");

    $get_dev_list_query->bind_param("i", $venue_id);

    $get_dev_list_query->execute();

    $get_dev_list_query->store_result();
    if($get_dev_list_query->num_rows <= 0) {
        return 0;
    }

    $meta = $get_dev_list_query->result_metadata();
    while ($field = $meta->fetch_field())
    {
        $params[] = &$row[$field->name];
    }

    call_user_func_array(array($get_dev_list_query, 'bind_result'), $params);

    while ($get_dev_list_query->fetch()) {
        foreach($row as $key => $val)
        {
            $c[$key] = $val;
        }
        $result_arr[] = $c;
    }

    $get_dev_list_query->close();
    $mysql_conn->close();

    return $result_arr;
}

function GetDeviceListByCustomerEmail($email)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_dev_list_query = $mysql_conn->prepare("
        SELECT
        device.DEVICE_NAME,
        DEVICE_UUID,
        VENUE_NAME,
        ETH_MAC_ID,
        WLAN_MAC_ID,
        SUBSCRIPTION_START_DATE,
        SUBSCRIPTION_END_DATE,
        CONTRACT_TERMTYPE_CODE,
        MUSIC,
        JINGLE,
        WISH
        FROM
        device,
        venue,
        network_info,
        subscription,
        userslist,
        features
        WHERE
        device.CUSTOMER_ID = userslist.CUSTOMER_ID
        AND
        device.VENUE_ID = venue.VENUE_ID
        AND
        device.DEVICE_NAME = network_info.DEVICE_NAME
        AND
        device.DEVICE_NAME = subscription.DEVICE_NAME
        AND
        device.DEVICE_NAME = features.DEVICE_NAME
        AND
        userslist.EMAIL=?
        ORDER BY
        device.DEVICE_NAME
        ASC
    ");

    $get_dev_list_query->bind_param("s", $email);

    $get_dev_list_query->execute();

    $get_dev_list_query->store_result();
    if($get_dev_list_query->num_rows <= 0) {
        return 0;
    }

    $meta = $get_dev_list_query->result_metadata();
    while ($field = $meta->fetch_field())
    {
        $params[] = &$row[$field->name];
    }

    call_user_func_array(array($get_dev_list_query, 'bind_result'), $params);

    while ($get_dev_list_query->fetch()) {
        foreach($row as $key => $val)
        {
            $c[$key] = $val;
        }
        $result_arr[] = $c;
    }

    $get_dev_list_query->close();
    $mysql_conn->close();

    return $result_arr;
}

function GetVenueNameByCustomerID($customer_id)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_venue_name_query = $mysql_conn->prepare("
        SELECT
        VENUE_NAME
        FROM
        customer
        WHERE
        CUSTOMER_ID=?
    ");

    $get_venue_name_query->bind_param("i", $customer_id);

    $get_venue_name_query->execute();

    $get_venue_name_query->bind_result(
        $venue_name_col
    );

    $venue_name = "";

    while ($get_venue_name_query->fetch()) {
        $venue_name = $venue_name_col;
    }

    $get_venue_name_query->close();

    $mysql_conn->close();

    return $venue_name;
}

function GetVenueIdByDeviceName($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_venue_id_query = $mysql_conn->prepare("
        SELECT
        VENUE_ID
        FROM
        device
        WHERE
        DEVICE_NAME=?
    ");

    $get_venue_id_query->bind_param("s", $device_name);

    $get_venue_id_query->execute();

    $get_venue_id_query->bind_result(
        $venue_id_col
    );

    $venue_id = "";

    while ($get_venue_id_query->fetch()) {
        $venue_id = $venue_id_col;
    }

    $get_venue_id_query->close();

    $mysql_conn->close();

    return $venue_id;
}

function GetTotalOnlineDevices()
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $get_online_devices_query = $mysql_conn->prepare("
        SELECT
        COUNT(HB_LAST_UPDATE)
        FROM
        device_health
        WHERE
        (TO_SECONDS('{$timestamp}') - TO_SECONDS(HB_LAST_UPDATE)) <= 31
    ");

    $get_online_devices_query->execute();

    $get_online_devices_query->bind_result(
        $dev_lastseen_col
    );

    $get_online_devices_query->fetch();

    $get_online_devices_query->close();

    $mysql_conn->close();

    return $dev_lastseen_col;
}

function GetTotalAvailableDevices()
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $total_devices_query = $mysql_conn->prepare("
        SELECT
        COUNT(HB_LAST_UPDATE)
        FROM
        device_health
    ");

    $total_devices_query->execute();

    $total_devices_query->bind_result(
        $total_devices_col
    );

    $total_devices_query->fetch();

    $total_devices_query->close();

    $mysql_conn->close();

    return $total_devices_col;
}

function RecordTrialEnquiry($enquiry_details)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $trial_enquiry_query = $mysql_conn->prepare("
        INSERT INTO
        trial_enquiry
        (ENQUIRY_FULL_NAME,
        ENQUIRY_EMAIL,
        ENQUIRY_PHONE_NUMBER,
        ENQUIRY_TIMESTAMP)
        VALUES
        (?, ?, ?, ?)
    ");

    $trial_enquiry_query->bind_param("ssss",
        $enquiry_details[0],
        $enquiry_details[1],
        $enquiry_details[2],
        $timestamp
    );

    $trial_enquiry_query->execute();

    $trial_enquiry_query->close();

    $mysql_conn->close();

    return $enquiry_details;
}
