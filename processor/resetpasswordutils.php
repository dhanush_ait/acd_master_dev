<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '/opt/PHPMailer/Exception.php';
require '/opt/PHPMailer/PHPMailer.php';
require '/opt/PHPMailer/SMTP.php';

include "registrationutils.php";

function GetUserTypeByEmailID($email)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    // Admin account
    $get_usertype_query = $mysql_conn->prepare("
        SELECT
        EMAIL
        FROM
        adminslist
        WHERE
        EMAIL=?
    ");

    $get_usertype_query->bind_param("s", $email);

    $get_usertype_query->execute();

    $get_usertype_query->bind_result(
        $email_col
    );

    while ($get_usertype_query->fetch()) {
        if(strcmp($email_col, $email) == 0) {
            $get_usertype_query->close();

            return 0;
        }
        else {
            $get_usertype_query->close();
        }
    }

    // Customer account
    $get_usertype_query = $mysql_conn->prepare("
        SELECT
        EMAIL
        FROM
        userslist
        WHERE
        EMAIL=?
    ");

    $get_usertype_query->bind_param("s", $email);

    $get_usertype_query->execute();

    $get_usertype_query->bind_result(
        $email_col
    );

    while ($get_usertype_query->fetch()) {
        if(strcmp($email_col, $email) == 0) {
            $get_usertype_query->close();

            return 1;
        }
        else {
            $get_usertype_query->close();
        }
    }

    $mysql_conn->close();

    return -1;
}

function SendResetPasswordLink($email, $code)
{
    $emailbody = "<!DOCTYPE html>
<html lang=\"en\" xmlns=\"http://www.w3.org/1999/html\">
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
	<meta http-equiv=\"Content-Type\"  content=\"text/html charset=UTF-8\" />
    <title>JookeBox - Reset Password</title>
</head>
<body>
	<p>Hello there,<br />
	<br />
	<p>We received a request to reset your account password. If this was not requested by you please get in touch with us immediately to help us secure your account.</p>
	Please click the link below to generate a new password for your JookeBox account.<br />
	<a href=\"https://jookebox.in/newpassword.php?email=$email&amp;reset=$code\">Reset Password</a><br />
	<p>In case the above link is now working, please copy and paste the below URL in your browser,</p>
	<p>https://jookebox.in/newpassword.php?email=$email&amp;reset=$code</p>
	<br />
	Best regards,<br/>
	<a href=\"https://jookebox.in\">JookeBox</a>
	</p>
</body>
</html>";

    $host = "smtp-relay.gmail.com";
    $port = "587";
    $username = "hello@jookebox.in";
    $password = "jookebox123";

    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->Host = $host;
    $mail->Port = $port;
    $mail->SMTPSecure = 'tls';
    $mail->SMTPAuth = true;
    $mail->Username = $username;
    $mail->Password = $password;

    $mail->addAddress($email, 'JookeBox User');
    $mail->Subject = 'JookeBox - Reset Password';
    $mail->msgHTML($emailbody);

    try {
        $mail->setFrom('noreply@jookebox.in', 'JookeBox Alerts');
        if (!$mail->send()) {
            echo "";
            //echo "Mailer sending failed: " . $mail->ErrorInfo;
        }
    } catch (Exception $ex) {
        //echo "Mail sending failed";
        //$ex->getMessage();
    }
}

function GenerateResetPasswordCode($email, $user_type)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $code = generateVerificationCode($email);

    if($user_type == 0) {
        $store_reset_code_query = $mysql_conn->prepare("
            UPDATE
            adminslist
            SET
            PASSWORD_RESET=?,
            PASSWORD_RESET_ON=?
            WHERE
            EMAIL=?
        ");

        $store_reset_code_query->bind_param("sss", $code, $timestamp, $email);

        $store_reset_code_query->execute();

        $store_reset_code_query->close();
    }

    if($user_type == 1) {
        $store_reset_code_query = $mysql_conn->prepare("
            UPDATE
            userslist
            SET
            PASSWORD_RESET=?,
            PASSWORD_RESET_ON=?
            WHERE
            EMAIL=?
        ");

        $store_reset_code_query->bind_param("sss", $code, $timestamp, $email);

        $store_reset_code_query->execute();

        $store_reset_code_query->close();
    }

    $mysql_conn->close();

    return $code;
}

function GetResetPasswordLinkAge($email, $code, $user_type)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $reset_link_age = 0;

    if($user_type == 0) {
        $reset_link_age_query = $mysql_conn->prepare("
            SELECT
            PASSWORD_RESET_ON
            FROM
            adminslist
            WHERE
            PASSWORD_RESET=?
            AND
            EMAIL=?
        ");

        $reset_link_age_query->bind_param("ss", $code, $email);

        $reset_link_age_query->execute();

        $reset_link_age_query->bind_result(
            $password_reset_on_col
        );

        while ($reset_link_age_query->fetch()) {
            $reset_link_age = $password_reset_on_col;
        }

        $reset_link_age_query->close();
    }

    if($user_type == 1) {
        $reset_link_age_query = $mysql_conn->prepare("
            SELECT
            PASSWORD_RESET_ON
            FROM
            userslist
            WHERE
            PASSWORD_RESET=?
            AND
            EMAIL=?
        ");

        $reset_link_age_query->bind_param("ss", $code, $email);

        $reset_link_age_query->execute();

        $reset_link_age_query->bind_result(
            $password_reset_on_col
        );

        while ($reset_link_age_query->fetch()) {
            $reset_link_age = $password_reset_on_col;
        }

        $reset_link_age_query->close();
    }

    $mysql_conn->close();

    return $reset_link_age;
}

function RestoreResetPasswordStateAfterLinkExpiration($email, $user_type)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    if($user_type == 0) {
        $store_reset_code_query = $mysql_conn->prepare("
            UPDATE
            adminslist
            SET
            PASSWORD_RESET=NULL,
            PASSWORD_RESET_ON=NUll
            WHERE
            EMAIL=?
        ");

        $store_reset_code_query->bind_param("s", $email);

        $store_reset_code_query->execute();

        $store_reset_code_query->close();
    }

    if($user_type == 1) {
        $store_reset_code_query = $mysql_conn->prepare("
            UPDATE
            userslist
            SET
            PASSWORD_RESET=NULL,
            PASSWORD_RESET_ON=NUll
            WHERE
            EMAIL=?
        ");

        $store_reset_code_query->bind_param("s", $email);

        $store_reset_code_query->execute();

        $store_reset_code_query->close();
    }

    $mysql_conn->close();

    return 1;
}

function VerifyResetCodeByEmailID($email, $code, $user_type)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    if($user_type == 0) {
        $verify_reset_code_query = $mysql_conn->prepare("
            SELECT
            EMAIL,
            PASSWORD_RESET
            FROM
            adminslist
            WHERE
            EMAIL=?
            AND
            PASSWORD_RESET=?
        ");

        $verify_reset_code_query->bind_param("ss", $email, $code);

        $verify_reset_code_query->execute();

        $verify_reset_code_query->bind_result(
            $email_col,
            $password_reset_col
        );

        while ($verify_reset_code_query->fetch()) {
            if(strcmp($email_col, $email) == 0 && strcmp($password_reset_col, $code) == 0) {
                $verify_reset_code_query->close();

                $mysql_conn->close();

                return 1;
            }
        }

        $verify_reset_code_query->close();

        $mysql_conn->close();

        return 0;
    }

    if($user_type == 1) {
        $verify_reset_code_query = $mysql_conn->prepare("
            SELECT
            EMAIL,
            PASSWORD_RESET
            FROM
            userslist
            WHERE
            EMAIL=?
            AND
            PASSWORD_RESET=?
        ");

        $verify_reset_code_query->bind_param("ss", $email, $code);

        $verify_reset_code_query->execute();

        $verify_reset_code_query->bind_result(
            $email_col,
            $password_reset_col
        );

        while ($verify_reset_code_query->fetch()) {
            if(strcmp($email_col, $email) == 0 && strcmp($password_reset_col, $code) == 0) {
                $verify_reset_code_query->close();

                $mysql_conn->close();

                return 1;
            }
        }

        $verify_reset_code_query->close();

        $mysql_conn->close();

        return 0;
    }

    $mysql_conn->close();

    return null;
}

function ResetAdminPasswordByEmailID($email, $new_password)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $pw_salt_code = generateSalt($email);
    $salted_pw = $pw_salt_code . $new_password;
    $pw_hash_code = hash("sha256", $salted_pw, FALSE);

    $reset_admin_password_query = $mysql_conn->prepare("
        UPDATE
        adminslist
        SET
        PASSWORD_SALT=?,
        PASSWORD_HASH=?,
        PASSWORD_RESET=NULL,
        PASSWORD_RESET_ON=NULL
        WHERE
        EMAIL=?
    ");

    $reset_admin_password_query->bind_param("sss", $pw_salt_code, $pw_hash_code, $email);

    $reset_admin_password_query->execute();

    $reset_admin_password_query->close();

    $mysql_conn->close();

    return 1;
}

function ResetCustomerPasswordByEmailID($email, $new_password)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $pw_salt_code = generateSalt($email);
    $salted_pw = $pw_salt_code . $new_password;
    $pw_hash_code = hash("sha256", $salted_pw, FALSE);

    $reset_cust_password_query = $mysql_conn->prepare("
        UPDATE
        userslist
        SET
        PASSWORD_SALT=?,
        PASSWORD_HASH=?,
        PASSWORD_RESET=NULL,
        PASSWORD_RESET_ON=NULL
        WHERE
        EMAIL=?
    ");

    $reset_cust_password_query->bind_param("sss", $pw_salt_code, $pw_hash_code, $email);

    $reset_cust_password_query->execute();

    $reset_cust_password_query->close();

    $mysql_conn->close();

    return 1;
}