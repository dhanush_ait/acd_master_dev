<?php

function UserLoginValidate($email, $name, $password)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $login_validation_query = $mysql_conn->prepare("
        SELECT
        EMAIL,
        USERNAME,
        PASSWORD_SALT,
        PASSWORD_HASH,
        EMAIL_VERIFY_STATUS,
        ACTIVE_STATUS
        FROM
        userslist
        WHERE
        EMAIL=?
    ");

    $login_validation_query->bind_param("s", $email);

    $login_validation_query->execute();

    $login_validation_query->bind_result($email_col, $username_col, $pw_salt_col, $pw_hash_col, $email_verify_col, $active_status_col);

    while($login_validation_query->fetch()) {
        $tmp_salted_pw =  $pw_salt_col . $password;
        $tmp_salted_hash = hash("sha256", $tmp_salted_pw);

        if ((($email_col == $email) && ($pw_hash_col == $tmp_salted_hash)) || ($email_col == $email)) {
            if($email_verify_col == 1 && $active_status_col == 1) {
                $_SESSION['username']=$username_col;
                return 1; // Valid account - Success
            }
            if($email_verify_col == 0 && $active_status_col == 1) {
                return 2; // Valid account - Email not verified
            }
            if($active_status_col == 0) {
                return 3; // Valid account - Account is disabled
            }
        }
    }

    $mysql_conn->close();

    return -1;
}

function AcdwebAdminLoginValidate($email, $password)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $login_validation_query = $mysql_conn->prepare("
        SELECT
        EMAIL,
        PASSWORD_SALT,
        PASSWORD_HASH
        FROM
        adminlist
        WHERE
        EMAIL=?
    ");

    $login_validation_query->bind_param("s", $email);

    $login_validation_query->execute();

    $login_validation_query->bind_result($email_col, $pw_salt_col, $pw_hash_col);

    while($login_validation_query->fetch()) {
        $tmp_salted_pw =  $pw_salt_col . $password;
        $tmp_salted_hash = hash("sha256", $tmp_salted_pw);

        if (($email_col == $email) || ($pw_hash_col == $tmp_salted_hash)) {
            return 1;
            exit();
        }
    }

    $mysql_conn->close();

    return 1;
}