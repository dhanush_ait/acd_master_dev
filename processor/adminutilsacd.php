<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '/opt/PHPMailer/Exception.php';
require '/opt/PHPMailer/PHPMailer.php';
require '/opt/PHPMailer/SMTP.php';

function fetchCustomerId($email)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_customer_id_query = $mysql_conn->prepare("
        SELECT
        CUSTOMER_ID
        FROM
        userslist
        WHERE
        EMAIL=?
    ");

    $get_customer_id_query->bind_param("s", $email);

    $get_customer_id_query->execute();

    $get_customer_id_query->bind_result(
        $customer_id_col
    );

    $customer_id = "";

    while ($get_customer_id_query->fetch()) {
        $customer_id = $customer_id_col;
    }

    $get_customer_id_query->close();

    $mysql_conn->close();

    return $customer_id;
}

//Send mail to client
function SendEmailToClient($details){
        $emailbody = "  <!DOCTYPE html>
                        <html lang=\"en\" xmlns=\"http://www.w3.org/1999/html\">
                        <head>
                            <meta charset=\"utf-8\">
                            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
                            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
                            <meta http-equiv=\"Content-Type\"  content=\"text/html charset=UTF-8\" />
                            <title>Case Status Update</title>
                        </head>
                        <body>
                            <p>Hi {$details[4]},
                            <br />
                            <p>I would like to inform you on your case,</p>
                            CASE ID : {$details[0]}
                            Next Date : {$details[3]}.
                            Status : {$details[2]}.                            
                            <br />
                            Best regards,<br/>
                            <a href=\"https://acdweb.in\">ACDWEB</a>
                            </p>
                        </body>
                        </html>";
    
        $host = "smtp.gmail.com";
        $port = "587";
        $username = "acdweb.notify@gmail.com";
        $password = "callmed5";

        $mail = new PHPMailer;
        $mail->isSMTP();
        //$mail->SMTPDebug = 3; 
        $mail->Host = $host;
        $mail->Port = $port;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = $username;
        $mail->Password = $password;
    
        $mail->isHTML(true);
        $mail->addAddress($details[1], $details[4]);
        $mail->Subject = 'Case Status - Reminder';
        $mail->msgHTML($emailbody);
    
        try {
            $mail->setFrom('acdweb.notify@gmail.com', 'Advocates Case Diary ACD');
            if (!$mail->send()) {
                //echo "Email sent";
                //echo "Mailer Error: " . $mail->ErrorInfo;
            }
            else{
                return 1;
            }
        } catch (Exception $ex) {
            echo "Email sending failed";
            $ex->getMessage();
        }
}