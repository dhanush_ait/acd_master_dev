<?php

function CreateAccount($email, $name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if ($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");
    $admin ="admin";
    $actStatus =0;
    $emailStatus = 1;

    $device_query = $mysql_conn->prepare("
        INSERT INTO
        userslist
        (EMAIL,
        USERNAME,
        CREATED_ON,
        CREATED_BY,
        ACTIVE_STATUS,
        EMAIL_VERIFY_STATUS)
        VALUES
        (?, ?, ?, ?, ?, ?)
    ");
    echo $mysql_conn->error;
    $device_query->bind_param("ssssii", $email, $name, $timestamp, $admin, $actStatus, $emailStatus);

    $device_query->execute();

    $mysql_conn->close();
}

//Add new case
function AddNewCase($details)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if ($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $register_query = $mysql_conn->prepare("
        INSERT INTO
        case_list
        (CASE_TYPE,
        CASE_NUMBER,
        CASE_YEAR,
        CASE_DEFENDANT,
        CASE_OPPONENT,
        COURT_COMPLEX,
        CUSTOMER_ID,
        CASE_ADDED_ON)
        VALUES
        (?, ?, ?, ?, ?, ?, ?, ?)
    ");

    $register_query->bind_param(
        "ssssssss",
        $details[0],
        $details[1],
        $details[2],
        $details[3],
        $details[4],
        $details[5],
        $details[6],
        $timestamp
    );

    $register_query->execute();

    $genCaseId = mysqli_insert_id($mysql_conn);

    $mysql_conn->close();

    return $genCaseId;
}

//Add case from file data
function AddNewCaseFromFile($details)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if ($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $register_query = $mysql_conn->prepare("
        INSERT INTO
        case_list
        (CASE_TYPE,
        CASE_NUMBER,
        CASE_YEAR,
        CASE_DEFENDANT,
        CASE_OPPONENT,
        COURT_COMPLEX,
        CUSTOMER_ID,
        CASE_ADDED_ON,
        CNR_NUMBER)
        VALUES
        (?, ?, ?, ?, ?, ?, ?, ?, ?)
    ");

    $register_query->bind_param(
        "sssssssss",
        $details[0],
        $details[1],
        $details[2],
        $details[3],
        $details[4],
        $details[5],
        $details[6],
        $timestamp,
        $details[8]
    );

    $register_query->execute();

    $genCaseId = mysqli_insert_id($mysql_conn);

    $mysql_conn->close();

    return $genCaseId;
}



function AddClientDetails($details)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if ($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }
    $register_query = $mysql_conn->prepare("
    UPDATE
    clients
    SET
    CLIENT_EMAIL=?,
    CLIENT_CONTACT=?,
    CLIENT_CITY=?,
    CLIENT_ADDRESS=?,
    CLIENT_STATE=?,
    CLIENT_COUNTRY=?
    WHERE
    CLIENT_NAME=?
    ");

    $register_query->bind_param(
        "sssssss",
        $details[1],
        $details[2],
        $details[3],
        $details[4],
        $details[5],
        $details[6],
        $details[0]
    );

    $register_query->execute();

    $mysql_conn->close();
}

function AddClientEntry($email, $caseId)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if ($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $register_query = $mysql_conn->prepare("
        INSERT INTO
        clients
        (
        CLIENT_EMAIL,
        CASE_ID,
        CLIENT_ADDED_ON)
        VALUES
        (?, ?, ?)
    ");

    $register_query->bind_param("sss", $email, $caseId, $timestamp);

    $register_query->execute();

    $genClientId = mysqli_insert_id($mysql_conn);

    $mysql_conn->close();

    return $genClientId;
}

function AddInitialCaseProceeding($caseId, $ndate, $status)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if ($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $dateCreated = date("Y-m-d");
    if(!$ndate)
        $nextDate = date("Y-m-d");    
    
    else
        $nextDate = $ndate;
    
    if(!$status)
        $caseStatus = "Newly Added";
    
    else
        $caseStatus = $status;

    $register_query = $mysql_conn->prepare("
        INSERT INTO
        case_proceeding
        (
        CASE_ID,
        CASE_SUMMARY,
        PREVIOUS_DATE,
        NEXT_DATE,
        LAST_UPDATE_ON)
        VALUES
        (?, ?, ?, ?, ?)
    ");

    $register_query->bind_param(
        "sssss",
        $caseId,
        $caseStatus,
        $dateCreated,
        $nextDate,
        $timestamp
    );

    $register_query->execute();

    $mysql_conn->close();
}

function AddInitialPaymentDetails($caseId, $clientId, $customerID)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if ($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $dateCreated = date("Y-m-d");
    $reason = "Newly Added";

    $register_query = $mysql_conn->prepare("
        INSERT INTO
        payment
        (
            CASE_ID,
            CLIENT_ID,
            REASON,
            LAST_PAID_ON,
            ADDED_ON,
            LAST_UPDATED_ON,
            CUSTOMER_ID)
        VALUES(?, ?, ?, ?, ?, ?, ?)
    ");

    $register_query->bind_param(
        "sssssss",
        $caseId,
        $clientId,
        $reason,
        $dateCreated,
        $timestamp,
        $timestamp,
        $customerID
    );

    $register_query->execute();

    $mysql_conn->close();
}

//Check repeated case entry
function CheckCaseExists($details)
{

    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if ($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_case_id_query = $mysql_conn->prepare("
            SELECT
            CASE_ID
            FROM
            case_list
            WHERE
            CASE_TYPE=? AND
            CASE_NUMBER=? AND
            CASE_YEAR=? AND
            COURT_COMPLEX=?
        ");

    $get_case_id_query->bind_param(
        "ssss",
        $details[0],
        $details[1],
        $details[2],
        $details[5]
    );

    $get_case_id_query->execute();

    $get_case_id_query->store_result();

    $is_exist = $get_case_id_query->num_rows;

    if ($is_exist > 0) {
        $get_case_id_query->close();

        $mysql_conn->close();

        return true;
    }

    $get_case_id_query->close();

    $mysql_conn->close();

    return false;
}

//check case exists when loading case list from json file
function CheckCaseExistsFromFile($details)
{

    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if ($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_case_id_query = $mysql_conn->prepare("
            SELECT
            CASE_ID
            FROM
            case_list
            WHERE
            CNR_NUMBER = ?
        ");

    $get_case_id_query->bind_param(
        "s",
        $details[8]
    );

    $get_case_id_query->execute();

    $get_case_id_query->store_result();

    $is_exist = $get_case_id_query->num_rows;

    if ($is_exist > 0) {
        $get_case_id_query->close();

        $mysql_conn->close();

        return true;
    }

    $get_case_id_query->close();

    $mysql_conn->close();

    return false;
}

//Add email details 
function AddMailDetails($caseId, $clientId, $emailId)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if ($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $dateCreated = date("Y-m-d");
    $reason = "Newly Added";

    $register_query = $mysql_conn->prepare("
        INSERT INTO
        notification
        (
            CASE_ID,
            CLIENT_ID,
            EMAIL_ID)
        VALUES(?, ?, ?)
    ");

    $register_query->bind_param(
        "sss",
        $caseId,
        $clientId,
        $emailId
    );

    $register_query->execute();

    $mysql_conn->close();
}

function CheckUserEmailExists($email)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_email_exists_query = $mysql_conn->prepare("
        SELECT
        EMAIL,
        CUSTOMER_ID
        FROM
        userslist
        WHERE
        EMAIL=?
    ");

    $get_email_exists_query->bind_param("s", $email);

    $get_email_exists_query->execute();

    $get_email_exists_query->store_result();

    $is_exist = $get_email_exists_query->num_rows;

    if($is_exist > 0) {
        $get_email_exists_query->close();

        $mysql_conn->close();

        return true;
    }

    $get_email_exists_query->close();

    $mysql_conn->close();

    return false;
}