<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/

session_start();

require_once '../vendor/autoload.php';

require_once "verifylogin.php";
require "adminutilsacd.php";
require "registrationutilsacd.php";

$tokenId = $_POST['tokenId'];
$CLIENT_ID = "709502795016-qbft7m0mc6m6ekkdu5esdefsvlsbj0s5.apps.googleusercontent.com";

// Get $id_token via HTTPS POST.

$client = new Google_Client(['client_id' => $CLIENT_ID]); // Specify the CLIENT_ID of the app that accesses the backend
$payload = $client->verifyIdToken($tokenId);
$email = $payload['email'];
//echo $email;
//print_r($payload);
if ($payload) {
    //$userid = $payload['sub'];
    // If request specified a G Suite domain:
    //$domain = $payload['hd'];
    if(CheckUserEmailExists($email)){
        echo 1;
        exit();
    }
    CreateAccount($payload['email'], $payload['name']);
    echo 1;
} else {
    // Invalid ID token
    echo 0;
}

exit();
