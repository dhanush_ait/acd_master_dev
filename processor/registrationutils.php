<?php

function GenerateDeviceName()
{
    $dev_id_file = file_get_contents("../processor/device_number.txt");

    $dev_id_as_int = intval($dev_id_file);
    $dev_id_as_int += 1;
    $dev_name = sprintf("JB%010d", $dev_id_as_int);

    usleep(rand(2000, 8000));

    file_put_contents("../processor/device_number.txt", $dev_id_as_int);

    return $dev_name;
}

function ShowNewDeviceName()
{
    $dev_id_file = file_get_contents("../processor/device_number.txt", FALSE, NULL);

    $dev_id_as_int = intval($dev_id_file);
    $dev_id_as_int += 1;
    $dev_name = sprintf("JB%010d", $dev_id_as_int);

    return $dev_name;
}

function generateSalt($email_id) {
    $_salt = NULL;

    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $email_mobile_rand = $email_id . rand(10000, 99999);
    $generate_salt_query = $mysql_conn->prepare("SELECT SHA2(CONCAT(UUID(), ?), 256) LIMIT 1");
    $generate_salt_query->bind_param("s", $email_mobile_rand);
    $generate_salt_query->execute();
    $generate_salt_query->bind_result($salt_from_query);

    if($generate_salt_query->fetch()) {
        $_salt = $salt_from_query;
    }

    $generate_salt_query->close();
    $mysql_conn->close();

    return $_salt;
}

function GetCustomerIDByDeviceName($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_cust_id_query = $mysql_conn->prepare("
        SELECT
        CUSTOMER_ID
        FROM
        device
        WHERE
        DEVICE_NAME=?
    ");

    $get_cust_id_query->bind_param("s", $device_name);

    $get_cust_id_query->execute();

    $get_cust_id_query->bind_result(
        $customer_id_col
    );

    $customer_id = null;

    while ($get_cust_id_query->fetch()) {
        $customer_id = $customer_id_col;
    }

    $get_cust_id_query->close();

    $mysql_conn->close();

    return $customer_id;
}

function GetCustomerIDByVenueName($venue_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_cust_id_query = $mysql_conn->prepare("
        SELECT
        CUSTOMER_ID
        FROM
        venue
        WHERE
        VENUE_NAME=?
    ");

    $get_cust_id_query->bind_param("s", $venue_name);

    $get_cust_id_query->execute();

    $get_cust_id_query->bind_result(
        $customer_id_col
    );

    while ($get_cust_id_query->fetch()) {
        return $customer_id_col;
    }

    $mysql_conn->close();

    return null;
}

function GetVenueIDByVenueName($venue_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_venue_id_query = $mysql_conn->prepare("
        SELECT
        VENUE_ID
        FROM
        venue
        WHERE
        VENUE_NAME=?
    ");

    $get_venue_id_query->bind_param("s", $venue_name);

    $get_venue_id_query->execute();

    $get_venue_id_query->bind_result(
        $customer_id_col
    );

    while ($get_venue_id_query->fetch()) {
        return $customer_id_col;
    }

    $mysql_conn->close();

    return null;
}

function GetCustomerIDByCustomerName($customer_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_cust_id_query = $mysql_conn->prepare("
        SELECT
        CUSTOMER_ID
        FROM
        customer
        WHERE
        COMPANY_NAME=?
    ");

    $get_cust_id_query->bind_param("s", $customer_name);

    $get_cust_id_query->execute();

    $get_cust_id_query->bind_result(
        $customer_id_col
    );

    while ($get_cust_id_query->fetch()) {
        return $customer_id_col;
    }

    $mysql_conn->close();

    return null;
}

function AddNewCompany($details)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $register_query = $mysql_conn->prepare("
        INSERT INTO
        customer
        (COMPANY_NAME,
        COMPANY_ADDRESS_STREET,
        COMPANY_CITY,
        COMPANY_STATE,
        COMPANY_COUNTRY,
        POC_FULL_NAME,
        POC_EMAIL,
        POC_MOBILE_NUMBER,
        REMARKS,
        CREATED_ON,
        CREATED_BY)
        VALUES
        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
    ");

    $register_query->bind_param("sssssssssss",
        $details[0],
        $details[1],
        $details[2],
        $details[3],
        $details[4],
        $details[5],
        $details[6],
        $details[7],
        $details[8],
        $timestamp,
        $_SESSION['emailID']
    );

    $register_query->execute();

    $mysql_conn->close();
}

function AddNewVenue($details)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $register_query = $mysql_conn->prepare("
        INSERT INTO
        venue
        (CUSTOMER_ID,
        VENUE_NAME,
        VENUE_TYPE,
        VENUE_GSTIN,
        VENUE_ADDRESS_STREET,
        VENUE_CITY,
        VENUE_STATE,
        VENUE_COUNTRY,
        POC_FULL_NAME,
        POC_EMAIL,
        POC_MOBILE_NUMBER,
        REMARKS,
        CREATED_ON,
        CREATED_BY)
        VALUES
        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
    ");

    $register_query->bind_param("isisssssssssss",
        $details[0],
        $details[1],
        $details[2],
        $details[11],
        $details[3],
        $details[4],
        $details[5],
        $details[6],
        $details[7],
        $details[8],
        $details[9],
        $details[10],
        $timestamp,
        $_SESSION['emailID']
    );

    $register_query->execute();

    $mysql_conn->close();
}

function AddOrUpdateNewSubscriptionWithInvoice($details)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $is_exist = $mysql_conn->query("SELECT DEVICE_NAME FROM subscription WHERE DEVICE_NAME='{$details[7]}'")->num_rows;

    if($is_exist == 0) {

        $subscription_query = $mysql_conn->prepare("
            INSERT INTO
            subscription
            (CUSTOMER_ID,
            DEVICE_NAME,
            SUBSCRIPTION_START_DATE,
            CONTRACT_TERMTYPE_CODE,
            CONTRACT_TERM,
            SUBSCRIPTION_BASE_AMOUNT,
            SUBSCRIPTION_DISCOUNT,
            SUBSCRIPTION_FINAL_AMOUNT,
            INVOICE_FILE,
            INVOICE_FILEEXT,
            INVOICE_FILEMIME,
            INVOICE_FILESIZE,
            DEVICE_SECURITY_DEPOSIT,
            CREATED_ON,
            CREATED_BY,
            SUBSCRIPTION_END_DATE)
            VALUES
            (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        ");

        $null = NULL;

        $filename = $_FILES["subcInvoice"]["tmp_name"];
        $subcInvoice_mime = $_FILES["subcInvoice"]["type"];
        $subcInvoice_filename = $_FILES["subcInvoice"]["name"];
        $_filename_expl = explode(".", $subcInvoice_filename);
        $subcInvoice_ext = end($_filename_expl);
        $subcInvoice_size = $_FILES["subcInvoice"]["size"];

        $subscription_query->bind_param("issiidddbssiisss",
            $details[0],
            $details[7],
            $details[1],
            $details[2],
            $details[3],
            $details[4],
            $details[5],
            $details[6],
            $null,
            $subcInvoice_ext,
            $subcInvoice_mime,
            $subcInvoice_size,
            $details[9],
            $timestamp,
            $_SESSION['emailID'],
            $details[8]
        );

        $subscription_query->send_long_data(9, file_get_contents($filename));

        $subscription_query->execute();
    }

    if($is_exist > 0) {

        $subscription_query = $mysql_conn->prepare("
            UPDATE
            subscription
            SET
            CUSTOMER_ID=?,
            SUBSCRIPTION_START_DATE=?,
            SUBSCRIPTION_END_DATE=?,
            CONTRACT_TERMTYPE_CODE=?,
            CONTRACT_TERM=?,
            SUBSCRIPTION_BASE_AMOUNT=?,
            SUBSCRIPTION_DISCOUNT=?,
            SUBSCRIPTION_FINAL_AMOUNT=?,
            INVOICE_FILE=?,
            INVOICE_FILEEXT=?,
            INVOICE_FILEMIME=?,
            INVOICE_FILESIZE=?,
            DEVICE_SECURITY_DEPOSIT=?,
            LAST_UPDATED_ON=?,
            LAST_UPDATED_BY=?
            WHERE
            DEVICE_NAME=?
        ");

        $null = NULL;

        $filename = $_FILES["subcInvoice"]["tmp_name"];
        $subcInvoice_mime = $_FILES["subcInvoice"]["type"];
        $subcInvoice_filename = $_FILES["subcInvoice"]["name"];
        $_filename_expl = explode(".", $subcInvoice_filename);
        $subcInvoice_ext = end($_filename_expl);
        $subcInvoice_size = $_FILES["subcInvoice"]["size"];

        $subscription_query->bind_param("issiidddsbssiisss",
            $details[0],
            $details[1],
            $details[8],
            $details[2],
            $details[3],
            $details[4],
            $details[5],
            $details[6],
            $null,
            $subcInvoice_ext,
            $subcInvoice_mime,
            $subcInvoice_size,
            $details[9],
            $timestamp,
            $_SESSION['emailID'],
            $details[7]
        );

        $subscription_query->send_long_data(9, file_get_contents($filename));

        $subscription_query->execute();
    }

    $mysql_conn->close();
}

function AddOrUpdateNewSubscriptionWithoutInvoice($details)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $is_exist = $mysql_conn->query("SELECT DEVICE_NAME FROM subscription WHERE DEVICE_NAME='{$details[7]}'")->num_rows;

    if($is_exist == 0) {

        $subscription_query = $mysql_conn->prepare("
            INSERT INTO
            subscription
            (CUSTOMER_ID,
            DEVICE_NAME,
            SUBSCRIPTION_START_DATE,
            CONTRACT_TERMTYPE_CODE,
            CONTRACT_TERM,
            SUBSCRIPTION_BASE_AMOUNT,
            SUBSCRIPTION_DISCOUNT,
            SUBSCRIPTION_FINAL_AMOUNT,
            DEVICE_SECURITY_DEPOSIT,
            CREATED_ON,
            CREATED_BY,
            SUBSCRIPTION_END_DATE)
            VALUES
            (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        ");

        $subscription_query->bind_param("issiidddisss",
            $details[0],
            $details[7],
            $details[1],
            $details[2],
            $details[3],
            $details[4],
            $details[5],
            $details[6],
            $details[9],
            $timestamp,
            $_SESSION['emailID'],
            $details[8]
        );

        $subscription_query->execute();
    }

    if($is_exist > 0) {

        $subscription_query = $mysql_conn->prepare("
            UPDATE
            subscription
            SET
            CUSTOMER_ID=?,
            SUBSCRIPTION_START_DATE=?,
            SUBSCRIPTION_END_DATE=?,
            CONTRACT_TERMTYPE_CODE=?,
            CONTRACT_TERM=?,
            SUBSCRIPTION_BASE_AMOUNT=?,
            SUBSCRIPTION_DISCOUNT=?,
            SUBSCRIPTION_FINAL_AMOUNT=?,
            DEVICE_SECURITY_DEPOSIT=?,
            LAST_UPDATED_ON=?,
            LAST_UPDATED_BY=?
            WHERE
            DEVICE_NAME=?
        ");

        $subscription_query->bind_param("issiidddisss",
            $details[0],
            $details[1],
            $details[8],
            $details[2],
            $details[3],
            $details[4],
            $details[5],
            $details[6],
            $details[9],
            $timestamp,
            $_SESSION['emailID'],
            $details[7]
        );

        $subscription_query->execute();
    }

    $mysql_conn->close();
}

function AddNewDevice($details)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $uuid = null;
    exec("uuid -v4", $comm_out);
    if(strlen($comm_out[0]) == 36) {
        $uuid = $comm_out[0];
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $device_query = $mysql_conn->prepare("
        INSERT INTO
        device
        (CUSTOMER_ID,
        VENUE_ID,
        DEVICE_UUID,
        INSTALLATION_DATE,
        DEVICE_NAME,
        DEVICE_MODEL_CODE,
        DEVICE_MEMORYCARD_CODE,
        DEVICE_MACID_LAN,
        DEVICE_MACID_WIFI,
        REMARKS,
        DEVICE_CHARGER_TYPE_CODE,
        CREATED_ON,
        CREATED_BY,
        DEVICE_AD_STATUS,
        DEVICE_SUB_STATUS)
        VALUES
        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
    ");

    $ad_status = 0;
    $subscription_status = 0;

    $device_query->bind_param("iisssiisssissii",
        $details[0],
        $details[1],
        $uuid,
        $details[2],
        $details[3],
        $details[4],
        $details[5],
        $details[6],
        $details[7],
        $details[8],
        $details[9],
        $timestamp,
        $_SESSION['emailID'],
        $ad_status,
        $subscription_status
    );

    $device_query->execute();

    $mysql_conn->close();

    GenerateDefaultHealthEntryForDevice($uuid);
}

function GenerateDefaultScheduleForDevice($device)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $days_week = array(
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
    );

    $time_day = array(
        "10:00",
        "13:00",
        "17:00",
        "20:00"
    );

    $genre = array(
        "rock.m3u",
        "retro.m3u",
        "Hiphop.m3u",
        "Country.m3u"
    );

    $gen_default_sched_query = $mysql_conn->prepare("
        INSERT INTO
        schedule_scratch
        (DEVICE_NAME,
        SCHED_DAY,
        SCHED_TIME,
        SCHED_GENRE)
        VALUES
        (?, ?, ?, ?)
    ");

    foreach ($days_week as $day) {
        for($i = 0; $i < 4; $i++) {
            $gen_default_sched_query->bind_param("ssss",
                $device,
                $day,
                $time_day[$i],
                $genre[$i]
            );
            $gen_default_sched_query->execute();
        }
    }

    $gen_default_sched_query->close();

    $mysql_conn->close();
}

function GenerateDefaultHealthEntryForDevice($uuid)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $health_default_entry_query = $mysql_conn->prepare("
        INSERT INTO
        device_health
        (DEVICE_UUID,
        HB_LAST_UPDATE)
        VALUES
        (?, ?)
    ");

    $health_default_entry_query->bind_param("ss", $uuid, $timestamp);

    $health_default_entry_query->execute();

    $health_default_entry_query->close();

    $mysql_conn->close();

    return 1;
}

function GenerateDefaultNetworkInfoEntryForDevice($device)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli();
    $mysql_conn->real_connect($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $def_ip = "0.0.0.0";
    $def_mac = "00:00:00:00:00:00";
    $null = null;

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $network_default_entry_query = $mysql_conn->prepare("
        INSERT INTO
        network_info
        (DEVICE_NAME,
        PUBLIC_IPV4,
        PRIVATE_IPV4,
        OPENVPN_IPV4,
        GEOLOC_LAT,
        GEOLOC_LONG,
        GEOLOC_CITY,
        GEOLOC_REGION,
        GEOLOC_COUNTRY,
        ETH_MAC_ID,
        WLAN_MAC_ID,
        LAST_UPDATED_ON)
        VALUES
        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
    ");

    $network_default_entry_query->bind_param("ssssssssssss",
        $device,
        $def_ip,
        $def_ip,
        $def_ip,
        $null,
        $null,
        $null,
        $null,
        $null,
        $def_mac,
        $def_mac,
        $timestamp);

    $network_default_entry_query->execute();

    $network_default_entry_query->close();

    $mysql_conn->close();

    return 1;
}

function GenerateDefaultCurrentPlayingEntryForDevice($device)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli();
    $mysql_conn->real_connect($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $default_track = [];
    $default_track[0] = "Default Track.mp3";
    $default_queue = json_encode($default_track);
    $default_genre = "Default Genre.m3u";
    $count = 0;

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $network_default_entry_query = $mysql_conn->prepare("
        INSERT INTO
        current_playing_track
        (DEVICE_NAME,
        FILENAME,
        GENRE,
        PLAYBACK_START_TIME,
        MUSIC_QUEUE,
        MUSIC_QUEUE_COUNT)
        VALUES
        (?, ?, ?, ?, ?, ?)
    ");

    $network_default_entry_query->bind_param("sssssi", $device, $default_track[0], $default_genre, $timestamp, $default_queue, $count);

    $network_default_entry_query->execute();

    $network_default_entry_query->close();

    $mysql_conn->close();

    return 1;
}

function GenerateDefaultFeaturesForDevice($device)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli();
    $mysql_conn->real_connect($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $no = 0;
    $yes = 1;
    $null = null;

    date_default_timezone_set("Asia/Kolkata");
    $timestamp = date("Y-m-d H:i:s");

    $default_features_query = $mysql_conn->prepare("
        INSERT INTO
        features
        (DEVICE_NAME,
        ENABLE_JOOKEBOX,
        MUSIC,
        JINGLE,
        JINGLE_STATUS,
        WISH,
        SCHEDULE_UPDATE_AVAILABLE,
        EQUALISER_UPDATE_AVAILABLE,
        LAST_UPDATED_BY,
        LAST_UPDATED_ON,
        CREATED_BY,
        CREATED_ON)
        VALUES
        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
    ");

    $default_features_query->bind_param("siiiiiiissss",
        $device,
        $yes,
        $yes,
        $no,
        $no,
        $yes,
        $yes,
        $yes,
        $null,
        $null,
        $_SESSION['emailID'],
        $timestamp
    );

    $default_features_query->execute();

    $default_features_query->close();

    $mysql_conn->close();

    return 1;
}

function GenerateDefaultSubscriptionForDevice($device)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli();
    $mysql_conn->real_connect($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');

    $cont_type = 5;
    $cont_term = 1;
    $amt = 0.00;
    $filesize = 0;
    $today = date("Y-m-d");
    $timestamp = date("Y-m-d H:i:s");
    $dev_deposit = 1;

    $cust_id = GetCustomerIDByDeviceName($device);

    $start_date = new DateTime($today, new DateTimeZone("Asia/Kolkata"));
    $end_date = $start_date->modify("+ 1 week")->format("Y-m-d");

    $network_default_entry_query = $mysql_conn->prepare("
        INSERT INTO
        subscription
        (CUSTOMER_ID,
        DEVICE_NAME,
        CONTRACT_TERMTYPE_CODE,
        CONTRACT_TERM,
        SUBSCRIPTION_START_DATE,
        SUBSCRIPTION_END_DATE,
        SUBSCRIPTION_BASE_AMOUNT,
        SUBSCRIPTION_DISCOUNT,
        SUBSCRIPTION_FINAL_AMOUNT,
        INVOICE_FILESIZE,
        DEVICE_SECURITY_DEPOSIT,
        CREATED_ON,
        CREATED_BY)
        VALUES
        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
    ");

    $network_default_entry_query->bind_param("isiissdddiiss",
        $cust_id,
        $device,
        $cont_type,
        $cont_term,
        $today,
        $end_date,
        $amt,
        $amt,
        $amt,
        $filesize,
        $dev_deposit,
        $timestamp,
        $_SESSION['emailID']
    );

    $network_default_entry_query->execute();

    $network_default_entry_query->close();

    $mysql_conn->close();

    return 1;
}

function CreateJookeboxAccount($details)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $email_verif_code = generateVerificationCode($details[1]);
    $email_verif_status = 0;

    $pw_salt_code = generateSalt($details[1]);
    $salted_pw = $pw_salt_code . $details[2];
    $pw_hash_code = hash("sha256", $salted_pw, FALSE);

    $active_status = 0;

    $device_query = $mysql_conn->prepare("
        INSERT INTO
        userslist
        (CUSTOMER_ID,
        EMAIL,
        CREATED_BY,
        CREATED_ON,
        PASSWORD_SALT,
        PASSWORD_HASH,
        EMAIL_VERIFY_CODE,
        EMAIL_VERIFY_STATUS,
        ACTIVE_STATUS)
        VALUES
        (?, ?, ?, ?, ?, ?, ?, ?, ?)
    ");

    $device_query->bind_param("issssssii",
        $details[0],
        $details[1],
        $details[3],
        $timestamp,
        $pw_salt_code,
        $pw_hash_code,
        $email_verif_code,
        $email_verif_status,
        $active_status);

    $device_query->execute();

    $mysql_conn->close();
}

function GetJookeBoxAccountStatus($email)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_cust_acc_status_query = $mysql_conn->prepare("
        SELECT
        ACTIVE_STATUS,
        EMAIL_VERIFY_STATUS
        FROM
        userslist
        WHERE
        EMAIL=?
    ");

    $get_cust_acc_status_query->bind_param("s", $email);

    $get_cust_acc_status_query->execute();

    $get_cust_acc_status_query->bind_result(
        $active_status_col,
        $email_verify_col
    );

    while ($get_cust_acc_status_query->fetch()) {
        return array(   $active_status_col,
            $email_verify_col
        );
    }

    $mysql_conn->close();

    return null;
}

function CheckVenueIDExistForNewDevice($venue_id)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_venue_id_query = $mysql_conn->prepare("
        SELECT
        VENUE_ID
        FROM
        device
        WHERE
        VENUE_ID=?
    ");

    $get_venue_id_query->bind_param("i", $venue_id);

    $get_venue_id_query->execute();

    $get_venue_id_query->store_result();

    $is_exist = $get_venue_id_query->num_rows;

    if($is_exist > 0) {
        $get_venue_id_query->close();

        $mysql_conn->close();

        return true;
    }

    $get_venue_id_query->close();

    $mysql_conn->close();

    return false;
}

function EditJookeboxAccountSetAccountStatus($details)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $cust_set_active = $mysql_conn->prepare("
        UPDATE
        userslist
        SET
        ACTIVE_STATUS=?,
        EMAIL_VERIFY_STATUS=?,
        LAST_UPDATED_BY=?,
        LAST_UPDATED_ON=?
        WHERE
        EMAIL=?
    ");

    $cust_set_active->bind_param("iisss",
        $details[0],
        $details[3],
        $details[1],
        $timestamp,
        $details[2]
    );

    $cust_set_active->execute();

    $mysql_conn->close();
}

function EditJookeboxAccountSetAccountNewPassword($details)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $pw_salt_code = generateSalt($details[0]);
    $salted_pw = $pw_salt_code . $details[1];
    $pw_hash_code = hash("sha256", $salted_pw, FALSE);

    $cust_new_password = $mysql_conn->prepare("
        UPDATE
        userslist
        SET
        PASSWORD_SALT=?,
        PASSWORD_HASH=?,
        LAST_UPDATED_BY=?,
        LAST_UPDATED_ON=?,
        PASSWORD_RESET_ON=?
        WHERE
        EMAIL=?
    ");

    $cust_new_password->bind_param("ssssss",
        $pw_salt_code,
        $pw_hash_code,
        $details[2],
        $timestamp,
        $timestamp,
        $details[0]
    );

    $cust_new_password->execute();

    $mysql_conn->close();
}

function CreateJookeboxAdminAccount($details)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    date_default_timezone_set('Asia/Kolkata');
    $timestamp = date("Y-m-d H:i:s");

    $pw_salt_code = generateSalt($details[0]);
    $salted_pw = $pw_salt_code . $details[1];
    $pw_hash_code = hash("sha256", $salted_pw, FALSE);

    $device_query = $mysql_conn->prepare("
        INSERT INTO
        adminslist
        (EMAIL,
        PASSWORD_SALT,
        PASSWORD_HASH,
        CREATED_ON)
        VALUES
        (?, ?, ?, ?)
    ");

    $device_query->bind_param("ssss",
        $details[0],
        $pw_salt_code,
        $pw_hash_code,
        $timestamp);

    $device_query->execute();

    $mysql_conn->close();
}

function getListOfAllUsers()
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $list_users_query = $mysql_conn->prepare("
        SELECT
        COMPANY_NAME,
        EMAIL,
        EMAIL_VERIFY_STATUS,
        ACTIVE_STATUS
        FROM
        userslist,customer
        WHERE
        customer.CUSTOMER_ID = userslist.CUSTOMER_ID
        ORDER BY
        COMPANY_NAME
    ");

    $list_users_query->execute();

    $list_users_query->store_result();
    if($list_users_query->num_rows <= 0) {
        return 0;
    }

    $meta = $list_users_query->result_metadata();
    while ($field = $meta->fetch_field())
    {
        $params[] = &$row[$field->name];
    }

    call_user_func_array(array($list_users_query, 'bind_result'), $params);

    while ($list_users_query->fetch()) {
        foreach($row as $key => $val)
        {
            $c[$key] = $val;
        }
        $result_arr[] = $c;
    }

    $list_users_query->close();
    $mysql_conn->close();

    return $result_arr;
}

function GetSubscriptionInfoFromDeviceNameAdmin($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $subscription_info_query = $mysql_conn->prepare("
        SELECT
        DEVICE_NAME,
        CONTRACT_TERMTYPE_CODE,
        CONTRACT_TERM,
        SUBSCRIPTION_START_DATE,
        SUBSCRIPTION_END_DATE,
        SUBSCRIPTION_BASE_AMOUNT,
        SUBSCRIPTION_DISCOUNT,
        SUBSCRIPTION_FINAL_AMOUNT,
        REMARKS,
        DEVICE_SECURITY_DEPOSIT
        FROM
        subscription
        WHERE
        DEVICE_NAME=?
    ");

    $subscription_info_query->bind_param("s", $device_name);

    $subscription_info_query->execute();

    $subscription_info_query->bind_result(
        $device_name_col,
        $contract_termtype_code_col,
        $contract_term_col,
        $subscription_start_date_col,
        $subscription_end_date_col,
        $subscription_base_amount_col,
        $subscription_discount_col,
        $subscription_final_amount_col,
        $remarks_col,
        $device_security_deposit_col
    );

    while ($subscription_info_query->fetch()) {
        return array(
            $device_name_col,
            $contract_termtype_code_col,
            $contract_term_col,
            $subscription_start_date_col,
            $subscription_end_date_col,
            $subscription_base_amount_col,
            $subscription_discount_col,
            $subscription_final_amount_col,
            $remarks_col,
            $device_security_deposit_col
        );
    }

    $mysql_conn->close();

    return null;
}

function GetSubscriptionInfoFromDeviceNameCustomer($device_name, $email)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $subscription_info_query = $mysql_conn->prepare("
        SELECT
        subscription.DEVICE_NAME,
        CONTRACT_TERMTYPE_CODE,
        CONTRACT_TERM,
        SUBSCRIPTION_START_DATE,
        SUBSCRIPTION_END_DATE,
        SUBSCRIPTION_BASE_AMOUNT,
        SUBSCRIPTION_DISCOUNT,
        SUBSCRIPTION_FINAL_AMOUNT,
        subscription.REMARKS,
        DEVICE_SECURITY_DEPOSIT
        FROM
        subscription,userslist,customer,device,venue
        WHERE
        subscription.CUSTOMER_ID = userslist.CUSTOMER_ID
        AND
        device.VENUE_ID = venue.VENUE_ID
        AND
        userslist.CUSTOMER_ID = customer.CUSTOMER_ID
        AND
        subscription.DEVICE_NAME=?
        AND
        userslist.EMAIL=?
    ");

    $subscription_info_query->bind_param("ss", $device_name, $email);

    $subscription_info_query->execute();

    $subscription_info_query->bind_result(
        $device_name_col,
        $contract_termtype_code_col,
        $contract_term_col,
        $subscription_start_date_col,
        $subscription_end_date_col,
        $subscription_base_amount_col,
        $subscription_discount_col,
        $subscription_final_amount_col,
        $remarks_col,
        $device_security_deposit_col
    );

    while ($subscription_info_query->fetch()) {
        return array(
            $device_name_col,
            $contract_termtype_code_col,
            $contract_term_col,
            $subscription_start_date_col,
            $subscription_end_date_col,
            $subscription_base_amount_col,
            $subscription_discount_col,
            $subscription_final_amount_col,
            $remarks_col,
            $device_security_deposit_col
        );
    }

    $mysql_conn->close();

    return null;
}

function DownloadInvoiceFromDeviceName($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $invoice_download_query = $mysql_conn->prepare("
        SELECT
        DEVICE_NAME,
        INVOICE_FILE,
        INVOICE_FILEMIME,
        INVOICE_FILEEXT,
        INVOICE_FILESIZE
        FROM
        subscription
        WHERE
        DEVICE_NAME=?
    ");

    $invoice_download_query->bind_param("s", $device_name);

    $invoice_download_query->execute();

    $invoice_download_query->bind_result(
        $device_name_col,
        $invoice_file_col,
        $invoice_filemime_col,
        $invoice_fileext_col,
        $invoice_filesize_col
    );

    while ($invoice_download_query->fetch()) {
        return array(
            "dev_name" => $device_name_col,
            "invoice"  => $invoice_file_col,
            "mimetype" => $invoice_filemime_col,
            "file_ext" => $invoice_fileext_col,
            "filesize" => $invoice_filesize_col
        );
    }

    return null;
}

function generateVerificationCode($email_id) {
    $_code = NULL;

    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $generate_verifycode_query = $mysql_conn->prepare("SELECT SHA2(CONCAT(UUID(), ?), 256) LIMIT 1");
    $generate_verifycode_query->bind_param("s", $email_id);
    $generate_verifycode_query->execute();
    $generate_verifycode_query->bind_result($verifycode_from_query);

    if($generate_verifycode_query->fetch()) {
        $_code = $verifycode_from_query;
    }

    $generate_verifycode_query->close();
    $mysql_conn->close();

    return $_code;
}

function getVerificationCodeByEmail($email_id) {
    $_code = NULL;

    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $get_verifycode_query = $mysql_conn->prepare("SELECT EMAIL_VERIFY_CODE FROM userslist WHERE EMAIL=?");
    $get_verifycode_query->bind_param("s", $email_id);
    $get_verifycode_query->execute();
    $get_verifycode_query->bind_result($code_from_query_col);

    if($get_verifycode_query->fetch()) {
        $_code = $code_from_query_col;
    }

    return $_code;
}

function IsInvoiceUploaded($device_name)
{
    $db_conn = parse_ini_file("PHPDBConnect.ini");
    $mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

    if($mysql_conn->connect_error) {
        die("FATAL ERROR: Unable to create a connection to the database");
    }

    $invoice_status = 0;

    $check_invoice_query = $mysql_conn->prepare("
        SELECT
        INVOICE_FILESIZE
        FROM
        subscription
        WHERE
        DEVICE_NAME=?
        AND
        INVOICE_FILESIZE IS NOT NULL
        AND
        INVOICE_FILESIZE > 1024
    ");

    $check_invoice_query->bind_param("s", $device_name);

    $check_invoice_query->execute();

    $check_invoice_query->bind_result(
        $invoice_filesize_col
    );

    while ($check_invoice_query->fetch()) {
        if($invoice_filesize_col > 1024) {
            $invoice_status = 1;
        }
    }

    $check_invoice_query->close();

    $mysql_conn->close();

    return $invoice_status;
}

/*function sendEmailVerification($verifycode) {
    $emailbody = "<!DOCTYPE html>
<html lang=\"en\" xmlns=\"http://www.w3.org/1999/html\">
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
	<meta http-equiv=\"Content-Type\"  content=\"text/html charset=UTF-8\" />
    <title>Jookebox - Email Verification</title>
</head>
<body>
	<p>Dear User,<br />
	<br />
	Your music subscription has been activated successfully.<br />
	Please verify your email address to enjoy uninterrupted music service.<br />
	<br />
	To complete the verification process, please click the link below.<br />
	You can also copy the URL and paste it in a browser to complete the verification.<br>
	<a href=\"https://www.jookebox.com/verifyemail.php?email=$emailid&amp;code=$verifycode\">Verify Email</a><br />
	<br />
	Best regards,<br/>
	<a href=\"https://www.jookebox.com\">Jookebox</a>
	</p>
</body>
</html>";

    $to = "Jookebox" . " " . "User" . " <" . $emailid . ">";
    $subject = "Jookebox | Email Verification";

    $headers[] = 'MIME-Version: 1.0';
    $headers[] = 'Content-type: text/html; charset=utf-8';
    $headers[] = 'From: Jookebox Support <support@jookebox.com>';

    // GoDaddy settings
    if(mail($to, $subject, $emailbody, implode("\r\n", $headers))) {
        echo "";
    }
    else {
        echo "";
    }
}*/