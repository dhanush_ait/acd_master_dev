<?php

include_once "registrationutils.php";

$db_conn = parse_ini_file("PHPDBConnect.ini");
$mysql_conn = new mysqli();
//$mysql_conn->ssl_set(NULL, NULL, "rds-combined-ca-bundle.pem", NULL, NULL);
$mysql_conn->real_connect($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

if($mysql_conn->connect_error) {
    die("FATAL ERROR: Unable to create a connection to the database");
}

$details = [];

$details[0] = 3; // Customer ID
$details[1] = "karthikbharadwaj16@gmail.com"; // Login Email ID
$details[2] = "password"; // Password
$details[3] = "Admin"; // Updated by
$details[4] = "Karthik Bharadwaj";//User name

date_default_timezone_set('Asia/Kolkata');
$timestamp = date("Y-m-d H:i:s");

$email_verif_code = generateVerificationCode($details[1]);
$email_verif_status = 1;

$pw_salt_code = generateSalt($details[1]);
$salted_pw = $pw_salt_code . $details[2];
$pw_hash_code = hash("sha256", $salted_pw, FALSE);

$active_status = 1;

$device_query = $mysql_conn->prepare("
    UPDATE
    userslist
    SET
    EMAIL=?,
    USERNAME=?,
    LAST_UPDATED_BY=?,
    LAST_UPDATED_ON=?,
    PASSWORD_SALT=?,
    PASSWORD_HASH=?,
    EMAIL_VERIFY_CODE=?,
    EMAIL_VERIFY_STATUS=?,
    ACTIVE_STATUS=?
    WHERE
    CUSTOMER_ID=?
");

$device_query->bind_param("sssssssiii",
    $details[1],
    $details[4],
    $details[3],
    $timestamp,
    $pw_salt_code,
    $pw_hash_code,
    $email_verif_code,
    $email_verif_status,
    $active_status,
    $details[0]);

$device_query->execute();

$mysql_conn->close();