<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>ACDWEB | Administrator Login</title>

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css">
    <link rel="stylesheet" href="css/defaults.css" type="text/css">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

</head>

<body>

<?php

/*if(session_status() === PHP_SESSION_ACTIVE) {
    $_SESSION = array();

    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]
        );
    }

    session_unset();

    session_destroy();
}

if(session_status() === PHP_SESSION_NONE) {
    session_start();
}*/

error_reporting(0);
ini_set("display_errors", 0);

include "processor/verifylogin.php";

$login_message = "";

if(isset($_SESSION['loginOK']) && ($_SESSION['loginOK'] == 1)) {
    echo "<script type=\"text/javascript\">
               window.location = \"authuser/adminlanding.php\";
          </script>";
    exit();
}

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    $login_email = strtolower($_POST['AcdwebEmailID']);
    $login_password = $_POST['AcdwebPassword'];

    $auth_status = AcdwebAdminLoginValidate($login_email, $login_password);

    if($auth_status == 1) {
        header('Location: authuser/adminlanding.php');
        session_start();
        $_SESSION['emailID'] = $login_email;
        $_SESSION['loginAdminOK'] = 1;
        $_SESSION['userType'] = 0;

        exit();
    }
    else {
        $login_message = "&nbsp;&nbsp;&nbsp;&nbsp;Invalid email or password";
    }
}

?>

<nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="index.php" class="navbar-brand">
                    <img src="images/advlogo.jpg" width="28" height="24" />
                </a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav">
                    <li><a href="index.php">Home<span class="sr-only">(current)</span></a></li>
                    <li><a href="about.php">About Us</a></li>
                    <li class="active"><a href="applogin.php">App Login</a></li>
                </ul>
            </div>
        </div>
    </nav>

<div class="container" style="padding-top: 3%">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="panel-group" id="panel-774858">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <p class="panel-title">Acdweb | <strong>Administrator Login</strong></p>
                    </div>
                    <div id="panel-element-344374" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <form id="acdwebLoginForm" name="acdwebLoginForm" data-toggle="validator" method="post" action="<?php echo $_SERVER['PHP_SELF']?>" >
                                <div class="form-group">
                                    <label class="control-label">E-mail *</label>
                                    <input type="email" name="acdwebEmailID" id="acdwebEmailID" class="form-control" maxlength="254" placeholder="Registered e-mail address" pattern="^[\w\-\.]+@[a-zA-Z_]+?((\.[a-zA-Z]{2,3})|(\.[a-zA-Z]{2,3}\.[a-zA-Z]{2,3}))$" data-pattern-error="Invalid email address" data-required-error="This is a mandatory field" required="true" />
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Password *</label>
                                    <input type="password" name="acdwebPassword" id="acdwebPassword" class="form-control" data-minlength="8" maxlength="24" placeholder="Your password" data-required-error="This is a mandatory field" required />
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-lg btn-success" name="loginButton" id="loginButton" value="Login" />
                                            <label class="control-label text-warning text-left"> <?php if(isset($login_message)) { echo $login_message; } ?> </label>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>

</body>

</html>