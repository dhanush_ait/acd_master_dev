<?php

session_start();

if(!isset($_SESSION['emailID']) || $_SESSION['userActive'] != 1){
    echo "<script type=\"text/javascript\">
                  bootbox.alert({
                  size: \"large\",
                  title: \"Sorry\",
                  message: \"There was an error while processing your request. You may try logging-in again.\",
                  callback: function() {
                      window.location = \"index.php\";
                  }
                  })
                  </script>";
      exit();
}

include "../../processor/updateutilsacd.php";

$details = [];

// Case Information
$details[0] = $_POST['caseID'];
$details[1] = $_POST['caseAppelant'];
$details[2] = $_POST['caseOpponent'];
$details[3] = $_POST['caseNextDate'];
$details[4] = $_POST['caseSummary'];
$details[5] = $_SESSION['customerId'];

UpdateCaseDetails($details);

echo $details[0];