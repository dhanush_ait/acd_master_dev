<?php

session_start();

if (!isset($_SESSION['emailID']) || $_SESSION['userActive'] != 1) {
    echo "<script type=\"text/javascript\">
                bootbox.alert({
                size: \"large\",
                title: \"Sorry\",
                message: \"There was an error while processing your request. You may try logging-in again.\",
                callback: function() {
                    window.location = \"index.php\";
                }
                })
                </script>";
    echo "1";
    exit();
}

$case_type_code[3] = "AA";
$case_type_code[5] = "Appl.10(1)(c)";
$case_type_code[6] = "Appl.10(4)(A)";
$case_type_code[7] = "Appl.33(2)b";
$case_type_code[8] = "APPL.33(C)(2)";
$case_type_code[10] = "APPLN";
$case_type_code[15] = "Cr";
$case_type_code[9] = "Appl.u/s 11";
$case_type_code[2] = "A.S.";
$case_type_code[11] = "C.C.";
$case_type_code[12] = "C.O.A.";
$case_type_code[13] = "C.O.P.";
$case_type_code[16] = "CRL.A";
$case_type_code[17] = "CRL.M.A.";
$case_type_code[18] = "Crl.Misc.";
$case_type_code[19] = "Crl.Misc.(DVA)";
$case_type_code[20] = "CRL.R.P.";
$case_type_code[21] = "E.A.T.";
$case_type_code[67] = "E.C.A.";
$case_type_code[22] = "ELEC.C ";
$case_type_code[65] = "Ele.Misc.";
$case_type_code[66] = "Ele.Petn.";
$case_type_code[23] = "EX";
$case_type_code[24] = "Ex.A.";
$case_type_code[25] = "Ex.C";
$case_type_code[26] = "FDP";
$case_type_code[28] = "G and W.C.";
$case_type_code[27] = "G  and WC CASE";
$case_type_code[29] = "H.R.C.";
$case_type_code[30] = "H.R.C.A.";
$case_type_code[31] = "I.C.";
$case_type_code[34] = "IDact";
$case_type_code[64] = "IDact";
$case_type_code[35] = "IDact";
$case_type_code[32] = "IDact";
$case_type_code[36] = "IDact";
$case_type_code[37] = "IDact";
$case_type_code[33] = "IDact";
$case_type_code[38] = "IID.1947,Sec.32 (A).";
$case_type_code[39] = "Ind Emp";
$case_type_code[40] = "J.C.";
$case_type_code[41] = "KID 10";
$case_type_code[42] = "L.A.C.";
$case_type_code[44] = "LAC(APPL)";
$case_type_code[45] = "M.A.";
$case_type_code[48] = "MA(EAT)";
$case_type_code[46] = "M.C.";
$case_type_code[49] = "Misc";
$case_type_code[50] = "Misc.Appln.";
$case_type_code[47] = "M.V.C.";
$case_type_code[51] = "O.L.";
$case_type_code[52] = "O.S.";
$case_type_code[54] = "P and Sc";
$case_type_code[55] = "P.C.R.";
$case_type_code[56] = "P.MIS.";
$case_type_code[53] = "P  SC";
$case_type_code[57] = "R.A.";
$case_type_code[58] = "R.C.(E)";
$case_type_code[59] = "R.E.V.";
$case_type_code[68] = "REVIEW  PETITION";
$case_type_code[60] = "R.E.V.";
$case_type_code[62] = "SC";
$case_type_code[61] = "S.C.";
$case_type_code[63] = "SPL.C";
$case_type_code[1] = "A.C.";
$case_type_code[0] = " ";

$db_conn = parse_ini_file("../../processor/PHPDBConnect.ini");
$mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

$customer_Id = $_SESSION['customerId'];

$fetch_case_list_query = $mysql_conn->prepare("
    SELECT
    case_list.CASE_ID,
    CASE_TYPE,
    CLIENT_ID,
    CASE_NUMBER,
    CASE_YEAR,
    CASE_DEFENDANT,
    CASE_OPPONENT,
    PREVIOUS_DATE,
    NEXT_DATE,
    CASE_SUMMARY,
    COURT_COMPLEX,
    CASE_DISPOSAL,
    CASE_RESULT,
    REMINDER_STATUS,
    clients.CLIENT_EMAIL,
    CLIENT_CONTACT
    FROM
    case_list,
    case_proceeding,
    clients
    WHERE
    CUSTOMER_ID=?
    AND
    case_list.CASE_ID = case_proceeding.CASE_ID
    AND
    case_list.CASE_ID = clients.CASE_ID
");

$fetch_case_list_query->bind_param("s", $customer_Id);

$fetch_case_list_query->execute();

$fetch_case_list_query->store_result();

if ($fetch_case_list_query->num_rows <= 0) {
    echo "0";
    return 0;
}

$meta = $fetch_case_list_query->result_metadata();
while ($field = $meta->fetch_field()) {
    $params[] = &$row[$field->name];
}

call_user_func_array(array($fetch_case_list_query, 'bind_result'), $params);

$i=0;
while ($fetch_case_list_query->fetch()) {
    $c['count']=$i;
    foreach ($row as $key => $val) {
        if ($key == 'CASE_TYPE' && $val != null) {
            $c[$key] = $case_type_code[$val];
        }
        if ($key == 'CASE_ID' && $val != null) {
            $c[$key] = $val;
        }
        if ($key == 'CLIENT_ID' && $val != null) {
            $c[$key] = $val;
        }
        if ($key == 'COURT_COMPLEX' && $val != null) {
            $c[$key] = $val;
        }
        if ($key == 'CLIENT_CONTACT' && $val != null) {
            $c[$key] = $val;
        }
        if ($key == 'CASE_NUMBER' && $val != null) {
            //$c[$key] = $c['CASE_TYPE'] . " " . $val . "/" . $c['CASE_YEAR'];
            $c[$key] = $val;
        }
        if ($key == 'CASE_YEAR' && $val != null) {
            $c[$key] = $val;
            //$c[$key] = $c['CASE_TYPE'] . " " . $val . "/" . $c['CASE_YEAR'];
        }
        if ($key == 'CASE_DEFENDANT' && $val != null) {
            $c[$key] = $val;
        }
        if ($key == 'REMINDER_STATUS') {
            $c[$key] = $val;
        }
        if ($key == 'CASE_OPPONENT' && $val != null) {
            $c[$key] = $val;
        }
        if ($key == 'PREVIOUS_DATE' && $val != null) {
            $c[$key] = date("d-m-Y", strtotime($val));
        }
        if ($key == 'NEXT_DATE' && $val != null) {
            $c[$key] = date("d-m-Y", strtotime($val));
        }
        if ($key == 'CASE_SUMMARY' && $val != null) {
            $c[$key] = $val;
        }
        if ($key == 'CLIENT_EMAIL' && $val != null) {
            $c[$key] = $val;
        }
        if ($key == 'CASE_DISPOSAL' && $val != null) {
            $c[$key] = date("d-m-Y", strtotime($val));
        }
        if ($key == 'CASE_RESULT' && $val != null) {
            $c[$key] = $val;
        }
        $c['FULL_ID'] = $c['CASE_TYPE'] . " " . $c['CASE_NUMBER'] . "/" . $c['CASE_YEAR'];
        //$c['BUTTON'] = "<td style='vertical-align: middle; text-align: center; white-space: nowrap; width: 1%;'><button class='btn btn-sm btn-warning' id='openEmailModal' onclick='populateEmailDetails(\"{$c['CLIENT_EMAIL']}\",\"{$c['CASE_ID']}\",\"{$i}\")'>Send Mail</button></td>";
        //$i++;
    }
    $c['BUTTON'] = "<td style='vertical-align: middle; text-align: center; white-space: nowrap; width: 1%;'><button class='btn btn-sm btn-warning' id='openEmailModal' onclick='populateEmailDetails(\"{$c['CLIENT_EMAIL']}\",\"{$c['CASE_ID']}\",\"{$i}\")'>Send Mail</button></td>";
    $i++;
    $result_arr[] = $c;
}

$fetch_case_list_query->close();

$mysql_conn->close();

$jresult["data"] = $result_arr;

echo json_encode($jresult);