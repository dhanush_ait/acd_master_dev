<?php


$db_conn = parse_ini_file("../../processor/PHPDBConnect.ini");
$mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

if($mysql_conn->connect_error) {
    die("FATAL ERROR: Unable to create a connection to the database");
}

$searchtoken = $_GET['term'];

$query = $mysql_conn->prepare("SELECT CASE_DEFENDANT FROM case_list WHERE CASE_DEFENDANT LIKE '%" . $searchtoken . "%' ORDER BY CASE_DEFENDANT ASC LIMIT 0, 12");
$query->execute();
$query->bind_result($result_col);

while($query->fetch()) {
    $result_arr[] = $result_col;
}

echo json_encode($result_arr);