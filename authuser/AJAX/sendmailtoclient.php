<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();

if(!isset($_SESSION['emailID']) || $_SESSION['userActive'] != 1){
    echo "<script type=\"text/javascript\">
                  bootbox.alert({
                  size: \"large\",
                  title: \"Sorry\",
                  message: \"There was an error while processing your request. You may try logging-in again.\",
                  callback: function() {
                      window.location = \"index.php\";
                  }
                  })
                  </script>";
      exit();
}

include "../../processor/updateutilsacd.php";
include "../../processor/adminutilsacd.php";

$details = [];

// Case Information
$details[0] = $_POST['caseID'];
$details[1] = $_POST['clientEmail'];
$details[2] = $_POST['caseStatus'];
$details[3] = $_POST['nextDate'];
$details[4] = $_POST['clientName'];
$details[5] = $_POST['clientId'];

$status = SendEmailToClient($details);

if($status){
    UpdateSentMail($details);
}
    
echo $status;