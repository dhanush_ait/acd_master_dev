<?php

session_start();

include "../../processor/registrationutils.php";

if($_SESSION['userType'] != 0) {
    exit();
}

if(!isset($_SESSION['loginAdminOK']) || ($_SESSION['loginAdminOK'] != 1)) {
    echo "<script type='text/javascript'>window.location = '../../logout.php'</script>";
    exit();
}

$details = [];

$details[0] = intval($_POST['activeStatus']);
$details[1] = $_SESSION['emailID'];
$details[2] = $_POST['hiddenUsrEmailStatus'];
$details[3] = intval($_POST['emailVerifyStatus']);

EditJookeboxAccountSetAccountStatus($details);

if (strlen($_POST['hiddenUsrEmailNewPassword']) > 4) {
    $details_password = [];

    $details_password[0] = $_POST['hiddenUsrEmailStatus'];
    $details_password[1] = $_POST['hiddenUsrEmailNewPassword'];
    $details_password[2] = $_SESSION['emailID'];

    EditJookeboxAccountSetAccountNewPassword($details_password);
}