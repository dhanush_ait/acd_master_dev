<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


session_start();

if (!isset($_SESSION['emailID']) || $_SESSION['loginAdminOK'] != 1) {
    echo "<script type=\"text/javascript\">
                bootbox.alert({
                size: \"large\",
                title: \"Sorry\",
                message: \"There was an error while processing your request. You may try logging-in again.\",
                callback: function() {
                    window.location = \"index.php\";
                }
                })
                </script>";
    echo "1";
    exit();
}

include "../../processor/updateutilsacd.php";

$details = [];

// Case Information
$details[0] = $_POST['userId'];
$details[1] = $_POST['emailStatus'];
$details[2] = $_POST['accStatus'];

UpdateUserDetails($details);

echo $details[0];