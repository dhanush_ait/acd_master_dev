<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

if (!isset($_SESSION['emailID']) || $_SESSION['userActive'] != 1) {
    echo "<script type=\"text/javascript\">
                  bootbox.alert({
                  size: \"large\",
                  title: \"Sorry\",
                  message: \"There was an error while processing your request. You may try logging-in again.\",
                  callback: function() {
                      window.location = \"index.php\";
                  }
                  })
                  </script>";
    exit();
}

include "../../processor/registrationutilsacd.php";

$details = [];

// Case Information
$details[0] = $_POST['caseType'];
$details[1] = $_POST['caseNumber'];
$details[2] = $_POST['caseYear'];
$details[3] = ucwords($_POST['caseAppelant']);
$details[4] = ucwords($_POST['caseOpponent']);
$details[5] = $_POST['caseComplex'];
$details[6] = $_SESSION['customerId'];
$details[7] = $_POST['clientEmail'];

$caseExists = CheckCaseExists($details);

if ($caseExists) {
    echo json_encode(1);
    exit;
}

$genCaseId = AddNewCase($details);

AddInitialCaseProceeding($genCaseId);

$genClientId = AddClientEntry($details[7], $genCaseId);

AddMailDetails($genCaseId, $genClientId, $details[7]);

AddInitialPaymentDetails($genCaseId, $genClientId, $details[6]);

echo json_encode($details[0]);

exit;