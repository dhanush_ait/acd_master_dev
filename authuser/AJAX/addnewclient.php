<?php

session_start();

error_reporting(1);
ini_set("display_errors", 1);

if(!isset($_SESSION['emailID']) || $_SESSION['userActive'] != 1){
    echo "<script type=\"text/javascript\">
                  bootbox.alert({
                  size: \"large\",
                  title: \"Sorry\",
                  message: \"There was an error while processing your request. You may try logging-in again.\",
                  callback: function() {
                      window.location = \"index.php\";
                  }
                  })
                  </script>";
      echo "1";
      exit();
  }

include "../../processor/registrationutilsacd.php";

$details = [];

// Case Information
$details[0] = ucwords($_POST['clientName']);
$details[1] = ucwords($_POST['clientEmail']);
$details[2] = ucwords($_POST['clientContact']);
$details[3] = ucwords($_POST['clientCity']);
$details[4] = ucwords($_POST['clientAddress']);
$details[5] = ucwords($_POST['clientState']);
$details[6] = ucwords($_POST['clientCountry']);

AddClientDetails($details);

echo json_encode($details[0]);
