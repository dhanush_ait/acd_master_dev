<?php

session_start();

if (!isset($_SESSION['emailID']) || $_SESSION['userActive'] != 1) {
    echo "<script type=\"text/javascript\">
                bootbox.alert({
                size: \"large\",
                title: \"Sorry\",
                message: \"There was an error while processing your request. You may try logging-in again.\",
                callback: function() {
                    window.location = \"index.php\";
                }
                })
                </script>";
    echo "1";
    exit();
}

$db_conn = parse_ini_file("../../processor/PHPDBConnect.ini");
$mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

$fetch_user_data_query = $mysql_conn->prepare("
    SELECT
    CUSTOMER_ID,
    BAR_CODE,
    REG_NAME,
    GIVE_UPDATE_STATUS,
    LAST_UPDATED_ON,
    COURT_COMPLEX
    FROM
    userdata
    WHERE
    CUSTOMER_ID=?
");

$fetch_user_data_query->bind_param("s", $_SESSION['customerId']);

$fetch_user_data_query->execute();

$fetch_user_data_query->store_result();

if($fetch_user_data_query->num_rows <= 0) {
    return 0;
}

$meta = $fetch_user_data_query->result_metadata();
while ($field = $meta->fetch_field())
{
    $params[] = &$row[$field->name];
}

call_user_func_array(array($fetch_user_data_query, 'bind_result'), $params);

while ($fetch_user_data_query->fetch()) {
    foreach($row as $key => $val)
    {
        $c[$key] = $val;
    }
    $result_arr[] = $c;
}

$fetch_user_data_query->close();

$mysql_conn->close();

echo json_encode($result_arr);