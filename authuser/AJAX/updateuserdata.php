<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/

session_start();

include "../../processor/pythonscripts.php";

if (!isset($_SESSION['emailID']) || $_SESSION['userActive'] != 1) {
    echo "<script type=\"text/javascript\">
                bootbox.alert({
                size: \"large\",
                title: \"Sorry\",
                message: \"There was an error while processing your request. You may try logging-in again.\",
                callback: function() {
                    window.location = \"index.php\";
                }
                })
                </script>";
    echo "1";
    exit();
}

$db_conn = parse_ini_file("../../processor/PHPDBConnect.ini");
$mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

date_default_timezone_set('Asia/Kolkata');
$timestamp = date("Y-m-d H:i:s");

if ($mysql_conn->connect_error) {
    die("FATAL ERROR: Unable to create a connection to the database");
}

$user_data = [];
$user_data[0] = $_SESSION['customerId'];
$user_data[1] = $_POST['barCode'];
$user_data[2] = $_POST['regName'];
$user_data[3] = $_POST['courtComplex'];

$update_user_data_query = $mysql_conn->prepare("
            UPDATE
            userdata
            SET
            BAR_CODE=?,
            REG_NAME=?,
            COURT_COMPLEX=?,
            LAST_UPDATED_ON=?
            WHERE 
            CUSTOMER_ID=?
");

$update_user_data_query->bind_param(
    "sssss",
    $user_data[1],
    $user_data[2],
    $user_data[3],
    $timestamp,
    $user_data[0]
);

$update_user_data_query->execute();

$mysql_conn->close();

$status = getCaseListFile($user_data);

//$status=1;

if($status){
    $status1 = writeCaseListToDBFromFile($user_data[0], "Name");
    if($status1 != "no")
        $status2 = writeCaseListToDBFromFile($user_data[0], "Bar");
    if($status1 || $status2){
        echo "1";
    }
    else{
        echo "0";
    }
}
else{
    echo "0";
}

