<?php

session_start();

if($_SESSION['userType'] != 0) {
    exit();
}

include "../../processor/registrationutils.php";

if(!isset($_SESSION['loginAdminOK']) || ($_SESSION['loginAdminOK'] != 1)) {
    echo "<script type='text/javascript'>window.location = '../../logout.php'</script>";
    exit();
}

$details = [];

// Company Information
$details[0] = ucwords($_POST['jbCompanyName']);
$details[1] = ucwords($_POST['jbCompanyStreet']);
$details[2] = ucwords($_POST['jbCompanyCity']);
$details[3] = ucwords($_POST['jbCompanyState']);
$details[4] = ucwords($_POST['jbCompanyCountry']);

// Contact Details
$details[5] = ucwords($_POST['jbContactName']);
$details[6] = strtolower($_POST['jbContactEmail']);
$details[7] = $_POST['jbContactNumber'];
$details[8] = null;

AddNewCompany($details);

echo json_encode($details[0]);
