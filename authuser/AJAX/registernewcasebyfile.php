<?php

function writeCaseListToDBFromFile(){
include "../../processor/registrationutilsacd.php";

// Get the contents of the JSON file 
$strJsonFileContents = file_get_contents("../../python_scraping/ecourtScrapy/spiders/test.json");
// Convert to array 
$array = json_decode($strJsonFileContents, true);

//var_dump($array);

$c = [];

foreach ($array['con'] as $k => $v) {
    //print_r($k);
    $v = json_decode($v, true);
    foreach ($v as $k1 => $v1) {
        //$v1 = json_decode($v1, true);
        //print_r($v);
        foreach ($v1 as $k2 => $v2) {
            //print_r($k2 . ":".$v2);
            if ($k2 == "cino" && $v2 != "") {
                $c[$k2] = $v2;
            }
            if ($k2 == "case_type" && $v2 != "") {
                $c[$k2] = $v2;
            }
            if ($k2 == "case_year" && $v2 != "") {
                $c[$k2] = $v2;
            }
            if ($k2 == "case_no2" && $v2 != "") {
                $c[$k2] = $v2;
            }
            if ($k2 == "pet_name" && $v2 != "") {
                $c[$k2] = $v2;
            }
            if ($k2 == "res_name" && $v2 != "") {
                $c[$k2] = $v2;
            }
            if ($k2 == "case_type" && $v2 != "") {
                $c[$k2] = $v2;
            }
            $c['court_code'] = $k;
        }
        $result[] = $c;

        $details[0] = $c["case_type"];
        $details[1] = $c["case_no2"];
        $details[2] = $c["case_year"];
        $details[3] = $c["pet_name"];
        $details[4] = $c["res_name"];
        $details[5] = $c["court_code"];
        $details[6] = "1";
        $details[7] = "no data";
        $details[8] = $c["cino"];

        $caseExists = CheckCaseExistsFromFile($details);

        if ($caseExists) {
            echo json_encode(1);
            continue;
        }

        $genCaseId = AddNewCaseFromFile($details);

        AddInitialCaseProceeding($genCaseId);

        $genClientId = AddClientEntry($details[7], $genCaseId);

        AddMailDetails($genCaseId, $genClientId, $details[7]);
        
        AddInitialPaymentDetails($genCaseId, $genClientId, $details[6]);
    }
}
//print_r($result[0]['cino']);


/* Case Information
$details[0] = $_POST['caseType'];
$details[1] = $_POST['caseNumber'];
$details[2] = $_POST['caseYear'];
$details[3] = ucwords($_POST['caseAppelant']);
$details[4] = ucwords($_POST['caseOpponent']);
$details[5] = $_POST['caseComplex'];
$details[6] = $_SESSION['customerId'];
$details[7] = $_POST['clientEmail'];

$caseExists = CheckCaseExists($details);

if ($caseExists) {
    echo json_encode(1);
    exit;
}

$genCaseId = AddNewCase($details);

AddInitialCaseProceeding($genCaseId);

$genClientId = AddClientEntry($details[7], $genCaseId);

AddMailDetails($genCaseId, $genClientId, $details[7]);

AddInitialPaymentDetails($genCaseId, $genClientId, $details[6]);

echo json_encode($details[0]);

exit;*/
}