<?php

session_start();

if($_SESSION['userType'] != 0) {
    exit();
}

include "../../processor/registrationutils.php";

if(!isset($_SESSION['loginAdminOK']) || ($_SESSION['loginAdminOK'] != 1)) {
    echo "<script type='text/javascript'>window.location = '../../logout.php'</script>";
    exit();
}

$cust_id = GetCustomerIDByVenueName($_POST['usrVenueName']);

$details = [];

$details[0] = $cust_id;
$details[1] = strtolower($_POST['usrEmail']);
$details[2] = $_POST['usrConfirmPassword'];
$details[3] = strtolower($_SESSION['emailID']);

CreateJookeboxAccount($details);