<?php

/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/

session_start();

if (!isset($_SESSION['emailID']) || $_SESSION['loginAdminOK'] != 1) {
    echo "<script type=\"text/javascript\">
                bootbox.alert({
                size: \"large\",
                title: \"Sorry\",
                message: \"There was an error while processing your request. You may try logging-in again.\",
                callback: function() {
                    window.location = \"index.php\";
                }
                })
                </script>";
    echo "1";
    exit();
}

$db_conn = parse_ini_file("../../processor/PHPDBConnect.ini");
$mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

if ($mysql_conn->connect_error) {
    die("FATAL ERROR: Unable to create a connection to the database");
}

$query = $mysql_conn->prepare("
    DELETE
    FROM
    userslist
    WHERE
    CUSTOMER_ID=?
");

$query->bind_param("s", $_POST['userId']);

$query->execute();


$query->close();

$mysql_conn->close();

echo $_POST['userName'];

exit;