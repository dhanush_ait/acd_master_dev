<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/

session_start();

if (!isset($_SESSION['emailID']) || $_SESSION['userActive'] != 1) {
    echo "<script type=\"text/javascript\">
                bootbox.alert({
                size: \"large\",
                title: \"Sorry\",
                message: \"There was an error while processing your request. You may try logging-in again.\",
                callback: function() {
                    window.location = \"index.php\";
                }
                })
                </script>";
    echo "1";
    exit();
}

//$_SESSION['customerId'] = 1;
$_SESSION['caseUpdate'] = "true";

$db_conn = parse_ini_file("../../processor/PHPDBConnect.ini");
$mysql_conn = new mysqli($db_conn['host'], $db_conn['username'], $db_conn['password'], $db_conn['instance']);

$curDate = date("Y-m-d");

$fetch_user_data_query = $mysql_conn->prepare("
    SELECT
    case_list.CASE_ID,
    CNR_NUMBER,
    COURT_COMPLEX
    FROM
    case_list,
    case_proceeding
    WHERE
    case_list.CASE_ID = case_proceeding.CASE_ID
    AND
    NEXT_DATE < ?
    AND
    case_list.CUSTOMER_ID = ?");

$fetch_user_data_query->bind_param("ss", $curDate, $_SESSION['customerId']);

$fetch_user_data_query->execute();

$fetch_user_data_query->store_result();

if($fetch_user_data_query->num_rows <= 0) {
    return 0;
}

$meta = $fetch_user_data_query->result_metadata();
while ($field = $meta->fetch_field())
{
    $params[] = &$row[$field->name];
}

call_user_func_array(array($fetch_user_data_query, 'bind_result'), $params);

while ($fetch_user_data_query->fetch()) {
    foreach($row as $key => $val)
    {
        $c[$key] = $val;
    }
    $result_arr[] = $c;
}

$fetch_user_data_query->close();

$mysql_conn->close();

//echo json_encode($result_arr);

chdir('../../python_scraping/ecourtScrapy/spiders');

foreach($result_arr as $k=>$v){
    foreach($v as $k1=>$v1){
        //echo $k1."->".$v1;
        if($k1=="CNR_NUMBER")
            $cnr = $v1;
        if($k1=="COURT_COMPLEX")
            $cmp = $v1;
        if($k1=="CASE_ID")
            $cid = $v1;
    }
    //scrapy crawl caseHistoryDate -a court=26 -a cnr=KATK630001822019 -a cid=1 -a caseNo=678
    
    exec('scrapy crawl caseHistoryDate -a court='."$cmp".' -a cnr='."$cnr".' -a cid='."$_SESSION[customerId]".' -a caseNo='."$cid");
    //echo $cid."->".$cnr."->".$cmp."\n";
}

echo 1;