<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>Jookebox | User Management</title>

    <link rel="apple-touch-icon" sizes="57x57" href="../images/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../images/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../images/favicons/favicon-16x16.png">
    <link rel="manifest" href="../images/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../css/bootstrap-theme.min.css" type="text/css">
    <link rel="stylesheet" href="../css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="../js/datatables/DataTables-1.10.16/css/dataTables.bootstrap.css" type="text/css">
    <link rel="stylesheet" href="../css/autocomplete.css" type="text/css">
    <link rel="stylesheet" href="../css/defaults.css" type="text/css">

    <script type="application/javascript" src="../js/jquery.min.js"></script>
    <script type="application/javascript" src="../js/bootstrap.min.js"></script>
    <script type="application/javascript" src="../js/bootbox.min.js"></script>
    <script type="application/javascript" src="../js/bootstrap-slider.js"></script>
    <script type="application/javascript" src="../js/datatables/datatables.js"></script>
    <script type="application/javascript" src="../js/jquery.lazy.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../js/touchspin.min.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

</head>

<body>

<?php

session_start();

error_reporting(0);
ini_set("display_errors", 0);

include "../processor/registrationutils.php";

if(!isset($_SESSION['loginAdminOK']) || ($_SESSION['loginAdminOK'] != 1)) {
    echo "<script type=\"text/javascript\">
                bootbox.alert({
                size: \"large\",
                title: \"Sorry\",
                message: \"There was an error while processing your request. You may try logging-in again.\",
                callback: function() {
                    window.location = \"../index.php\";
                }
                })
                </script>";
    exit();
}

$userlist_field_names = array(
    "COMPANY_NAME" => "Company Name",
    "EMAIL" => "Email",
    "EMAIL_VERIFY_STATUS" => "Email Verified",
    "ACTIVE_STATUS" => "Login Status"
);

$userlist = getListOfAllUsers();

?>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="../index.php" class="navbar-brand">
                <img src="../images/jookebox_logo_big.png" width="28" height="24" />
            </a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <ul class="nav navbar-nav">
                <li><a href="../index.php">Home<span class="sr-only">(current)</span></a></li>
                <li><a href="../index.php#Tab2">About Us</a></li>
                <li><a href="../index.php#Tab3">Discover</a></li>
                <li><a href="../index.php#Tab4">JookeBox</a></li>
                <li><a href="../index.php#Tab5">Engage</a></li>
                <li><a href="../index.php#Tab6">Contact Us</a></li>
            </ul>
            <form class="navbar-form navbar-right" id="recruiterLogout" name="recruiterLogout" method="post" action="../logout.php">
                <div class="form-group">
                    <input type="submit" class="btn btn-sm btn-danger pull-right" name="logoutButtonLargeScreen" id="logoutButtonLargeScreen" value="Logout" />
                    <input type="submit" class="btn btn-sm btn-danger center-block" name="logoutButtonSmallScreen" id="logoutButtonSmallScreen" value="Logout" />
                </div>
            </form>
        </div>
    </div>
</nav>

<div style="padding-bottom: 1%;">
</div>

<?php

if($userlist) {
    $email_verif_status_to_str = array(
        "Unverified",
        "Verified"
    );

    $acc_active_status_to_str = array(
        "Disabled",
        "Active"
    );

    echo "<div class='container' id='UsersList' style='background-color: white;'>";
    echo "<p style='font-size: large; text-align: center'>Customer Accounts:</p>";
    echo "<div class=\"row\">";
    echo "   <div class=\"form-group\">";
    echo "       <button data-toggle='modal' id='addNewUserButton' data-target='#createCustomerAccModal' class=\"btn btn-success center-block\">Create New Account</button>";
    echo "   </div>";
    echo "</div>";
    echo "<div class='row'>";
    echo "<div class='col-md-12 col-xs-12 col-lg-12'>";
    echo "<div class='form-group'>";
    echo "<table class='table table-bordered table-hover display' id='usersListTable' style='background-color: white;'>";
    echo "<thead>";
    echo "<tr style='height: 20px; background-color: lightseagreen; vertical-align: middle'>";
    $tbl_col_names = [];
    $tbl_col_names = array_keys($userlist[0]);
    $fmt_col_names = array_keys($userlist_field_names);
    $table_header_open = "<th style='vertical-align: middle; white-space: nowrap; width: 1%;'>";
    $table_header_close = "</th>";
    $column_name = "";

    $column_name .= "<th style='vertical-align: middle; text-align: center;'>Edit" . $table_header_close;

    for ($x = 0; $x < count($fmt_col_names); $x++) {
        foreach ($tbl_col_names as $value) {
            if ($value == $fmt_col_names[$x]) {
                $column_name .= $table_header_open . $userlist_field_names[$value] . $table_header_close;
            }
        }
    }

    print_r($column_name);

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    $table_data_open = "<td style='vertical-align: middle; white-space: nowrap; width: 1%;'>";
    $table_data_close = "</td>";
    $column_value = "";

    for ($i = 0; $i < count($userlist); $i++) {
        $column_value .= "<tr>";
        $column_value .= "<td style='vertical-align: middle; text-align: center; white-space: nowrap; width: 1%;'><button class='btn btn-sm btn-info' data-toggle='modal' id='editUserAcc_{$i}' data-target='#adminEditCompanyDetailsModal' onclick='PopulateCompanyDetails(\"{$userlist[$i]['COMPANY_NAME']}\", \"{$userlist[$i]['EMAIL']}\")'>Edit</button><input type='hidden' id='editUserEmail_{$i}' value='{$userlist[$i]['EMAIL']}' /></td>";
        for ($j = 0; $j < count($tbl_col_names); $j++) {
            $key_name = $tbl_col_names[$j];
            if ($key_name == 'EMAIL_VERIFY_STATUS') {
                $key_val = $userlist[$i][$key_name];
                $userlist[$i][$key_name] = $email_verif_status_to_str[$key_val];
            }
            if ($key_name == 'ACTIVE_STATUS') {
                $key_val = $userlist[$i][$key_name];
                $userlist[$i][$key_name] = $acc_active_status_to_str[$key_val];
            }
            $column_value .= $table_data_open . $userlist[$i][$key_name] . $table_data_close;
        }
        $column_value .= "</tr>";
    }

    print_r($column_value);

    echo "</tbody>";
    echo "</table>";
    echo "</div>";
    echo "</div>";
    echo "</div>";
    echo "</div>";

    echo "<script type='application/javascript'>
            $(document).ready(function() {
                $('#usersListTable').DataTable( { order: [1, 'asc'], paging: true, responsive: true, stateSave: true, pageLength: 20 } );
            });
          </script>";
}

?>

<div class="modal fade" data-backdrop="static" data-keyboard="false" id="createCustomerAccModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                <h4 class="modal-title">Create Customer Account</h4>
            </div>
            <div class="modal-body" style="max-height: 60vh; overflow: auto;">
                <form id="customerAccountCreateForm" name="customerAccountCreateForm">
                    <div class="form-group">
                        <label class="control-label">Venue Name *</label>
                        <input type="text" name="usrVenueName" id="usrVenueName" class="form-control" maxlength="128" placeholder="Provide full name of the venue" data-required-error="This is a mandatory field" required />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div id="venuename">
                    </div>
                    <div class="form-group">
                        <label class="control-label">E-Mail ID *</label>
                        <input type="email" name="usrEmail" id="usrEmail" class="form-control" maxlength="254" placeholder="Valid e-mail address" data-required-error="This is a mandatory field" required />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Password *</label>
                        <input type="password" name="usrPassword" id="usrPassword" class="form-control" data-minlength="8" maxlength="24" placeholder="Password" data-minlength-error="Password length must be at least 8 characters" data-required-error="This is a mandatory field" required="true" />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Confirm Password *</label>
                        <input type="password" name="usrConfirmPassword" id="usrConfirmPassword" class="form-control" data-minlength="8" maxlength="24" placeholder="Confirm password" data-match="#usrPassword" data-match-error="Passwords do not match" data-required-error="This is a mandatory field" required />
                        <div class="help-block with-errors"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="addCustomerAccount" class="btn btn-success pull-left">Create Account</button>
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" data-backdrop="static" data-keyboard="false" id="adminEditCompanyDetailsModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Company Details</h4>
            </div>
            <div class="modal-body" style="max-height: 60vh; overflow: auto;">
                <form id="custAccStatus">
                    <div class="form-group">
                        <label class="control-label">E-Mail ID</label>
                        <input type="email" name="customerEmail" id="customerEmail" class="form-control" value="" disabled />
                        <input type="hidden" name="hiddenCompanyName" id="hiddenCompanyName" value="" />
                        <input type="hidden" name="hiddenCompanyId" id="hiddenCompanyId" value="" />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <div class="form-group col-xs-12 col-md-6 col-lg-6" id="activeStatusForm">
                            <label class="control-label" for="activeStatus">Set Active Status</label>
                            <div class="radio">
                                <label class="radio-inline">
                                    <input type="radio" name="activeStatus" value="0" />Inactive</label>
                                <label class="radio-inline">
                                    <input type="radio" name="activeStatus" value="1" />Active</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-group col-xs-12 col-md-6 col-lg-6" id="emailVerifyStatusForm">
                            <label class="control-label" for="emailVerifyStatus">Set E-Mail Verification Status</label>
                            <div class="radio">
                                <label class="radio-inline">
                                    <input type="radio" name="emailVerifyStatus" value="0" />Unverified</label>
                                <label class="radio-inline">
                                    <input type="radio" name="emailVerifyStatus" value="1" />Verified</label>
                            </div>
                        </div>
                    </div>
                    <button class='btn-danger center-block' data-toggle='collapse' href='#passwordField'><strong>SET NEW PASSWORD</strong></button>
                    <div class="panel-collapse collapse" id="passwordField">
                        <div class="col-md-3 col-lg-3"></div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label">Password *</label>
                            <input type="text" name="newUserPassword" id="newUserPassword" class="form-control" data-minlength="8" maxlength="24" placeholder="Password" data-minlength-error="Password length must be at least 8 characters" data-required-error="This is a mandatory field" required="true" />
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-md-3 col-lg-3"></div>
                    </div>
                </form>
            </div>
            <div class="modal-body" style="max-height: 60vh; overflow: auto;">
                <form name="editCompanyDetailsForm" id="editCompanyDetailsForm">
                    <div class="form-group col-xs-12 col-md-6 col-lg-6">
                        <label class="control-label" for="jbEditCompanyName">Company Name</label>
                        <input type="text" name="jbEditCompanyName" id="jbEditCompanyName" class="form-control" maxlength="128" placeholder="Provide full name of the company" data-required-error="This is a mandatory field" required />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-xs-12 col-md-6 col-lg-6">
                        <label class="control-label" for="jbEditContactName">Name of the Contact Person</label>
                        <input type="text" name="jbEditContactName" id="jbEditContactName" class="form-control" maxlength="128" placeholder="Provide full name" data-required-error="This is a mandatory field" required />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-xs-12 col-md-6 col-lg-6">
                        <label class="control-label" for="jbEditContactNumber">Contact Number</label>
                        <input type="text" name="jbEditContactNumber" id="jbEditContactNumber" class="form-control" maxlength="18" placeholder="Provide a valid contact number" data-required-error="This is a mandatory field" required />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-xs-12 col-md-6 col-lg-6">
                        <label class="control-label" for="jbEditContactEmail">Email</label>
                        <input type="text" name="jbEditContactEmail" id="jbEditContactEmail" class="form-control" maxlength="256" placeholder="Provide a valid email" data-required-error="This is a mandatory field" required />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-xs-12 col-md-12 col-lg-12">
                        <label class="control-label" for="jbEditCompanyStreet">Address</label>
                        <input type="text" name="jbEditCompanyStreet" id="jbEditCompanyStreet" class="form-control" maxlength="64" placeholder="Provide street address of the company" data-required-error="This is a mandatory field" required />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-xs-12 col-md-6 col-lg-6">
                        <label class="control-label" for="jbEditCompanyCity">City</label>
                        <input type="text" name="jbEditCompanyCity" id="jbEditCompanyCity" class="form-control" maxlength="64" placeholder="Provide city" data-required-error="This is a mandatory field" required />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-xs-12 col-md-6 col-lg-6">
                        <label class="control-label" for="jbEditCompanyState">State</label>
                        <input type="text" name="jbEditCompanyState" id="jbEditCompanyState" class="form-control" maxlength="64" placeholder="Provide state" data-required-error="This is a mandatory field" required />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-xs-12 col-md-6 col-lg-6">
                        <label class="control-label" for="jbEditCompanyCountry">Country</label>
                        <input type="text" name="jbEditCompanyCountry" id="jbEditCompanyCountry" class="form-control" maxlength="64" placeholder="Provide country" data-required-error="This is a mandatory field" required />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-xs-12 col-md-6 col-lg-6">
                        <label class="control-label">Remarks (optional)</label>
                        <input type="text" name="jbEditCompanyRemarks" id="jbEditCompanyRemarks" class="form-control" style="resize: none" rows="5" placeholder="Provide remarks if any..." />
                        <div class="help-block with-errors"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="updateCompanyUserDetailsButton" class="btn btn-danger" onclick="">Update</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript">
    $('#addCustomerAccount').click(function () {
        $.ajax({
            url: "AJAX/addcustomeraccount.php",
            type: "POST",
            data:
                {
                    'usrVenueName': $('#usrVenueName').val(),
                    'usrEmail': $('#usrEmail').val(),
                    'usrConfirmPassword': $('#usrConfirmPassword').val(),
                    'emailID': $('#emailID').val()
                },
            success: function (results) {
                bootbox.alert({
                    size: "small",
                    title: "Success",
                    message: "Account created",
                    callback: function() {
                        window.location.reload(true);
                    }
                });
            }
        });
        document.getElementById('customerAccountCreateForm').reset();
    });

    function editUserPopulateUserDetails(user_email) {
        $('#customerEmail').val(user_email);
        $.ajax({
            url: 'AJAX/fetchuseraccountstatus.php',
            type: 'POST',
            data: {
                'useremail': user_email
            },
            success: function (results) {
                var userstatus = $.parseJSON(results);
                if (userstatus[0]['ACTIVE_STATUS'] === 0) {
                    $('input:radio[name=activeStatus]')[0].checked = true;
                }
                else {
                    $('input:radio[name=activeStatus]')[1].checked = true;
                }
                if (userstatus[0]['EMAIL_VERIFY_STATUS'] === 0) {
                    $('input:radio[name=emailVerifyStatus]')[0].checked = true;
                }
                else {
                    $('input:radio[name=emailVerifyStatus]')[1].checked = true;
                }
            }
        });
    }

    function updateUserAccountStatus()
    {
        var customerEmail = $('#customerEmail').val();
        var newpassword = $('#newUserPassword').val();
        $.ajax({
            url: 'AJAX/updateuseraccountstatus.php',
            type: 'POST',
            data: {
                'hiddenUsrEmailStatus': customerEmail,
                'activeStatus': $('input:radio[name=activeStatus]:checked').val(),
                'emailVerifyStatus': $('input:radio[name=emailVerifyStatus]:checked').val(),
                'hiddenUsrEmailNewPassword' : newpassword
            }
        });
        document.getElementById('newUserPassword').value ='';
    }

    $(document).ready(function() {
        $("#usrVenueName").autocomplete({
            source: "AJAX/autocomp_venuename.php",
            minLength: 1,
            appendTo: "#venuename"
        });
    });

    function PopulateCompanyDetails(company_name,email_id)
    {
        $('#hiddenCompanyName').val(company_name);
        editUserPopulateUserDetails(email_id);
        $.ajax({
            url: 'AJAX/fetchcompanydetails.php',
            type: 'POST',
            data: {
                'companyName' : company_name,
                'useremail': email_id
            },
            success: function (results) {
                var companyinfo = $.parseJSON(results);
                $('#jbEditCompanyName').val(companyinfo[0]['COMPANY_NAME']);
                $('#jbEditContactName').val(companyinfo[0]['POC_FULL_NAME']);
                $('#jbEditContactNumber').val(companyinfo[0]['POC_MOBILE_NUMBER']);
                $('#jbEditContactEmail').val(companyinfo[0]['POC_EMAIL']);
                $('#jbEditCompanyCity').val(companyinfo[0]['COMPANY_CITY']);
                $('#jbEditCompanyStreet').val(companyinfo[0]['COMPANY_ADDRESS_STREET']);
                $('#jbEditCompanyState').val(companyinfo[0]['COMPANY_STATE']);
                $('#jbEditCompanyCountry').val(companyinfo[0]['COMPANY_COUNTRY']);
                $('#jbEditCompanyRemarks').val(companyinfo[0]['REMARKS']);
                $('#hiddenCompanyId').val(companyinfo[0]['CUSTOMER_ID']);
            }
        });
    }

    $('#updateCompanyUserDetailsButton').click( function ()
    {
        updateUserAccountStatus();
        $.ajax({
            url: 'AJAX/updatecompanydetails.php',
            type: 'POST',
            data: {
                'companyId' : $('#hiddenCompanyId').val(),
                'companyName' : $('#jbEditCompanyName').val(),
                'companyAddress': $('#jbEditCompanyStreet').val(),
                'companyCity' : $('#jbEditCompanyCity').val(),
                'companyState' : $('#jbEditCompanyState').val(),
                'companyCountry' : $('#jbEditCompanyCountry').val(),
                'pocFullName' : $('#jbEditContactName').val(),
                'pocEmail' : $('#jbEditContactEmail').val(),
                'pocMobileNumber' : $('#jbEditContactNumber').val(),
                'remarks' : $('#jbEditCompanyRemarks').val()
            },
            success: function (results) {
                bootbox.alert({
                    size: "small",
                    title: "Success",
                    message: "Account Details Updated Successfully",
                    callback: function() {
                        window.location.reload(true);
                    }
                });
            }
        });
    });
</script>

</body>

</html>