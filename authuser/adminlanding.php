<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>ACDWEB | Admin Homepage</title>

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <meta name="google-signin-client_id" content="709502795016-qbft7m0mc6m6ekkdu5esdefsvlsbj0s5.apps.googleusercontent.com">

    <link rel="stylesheet" href="../css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../css/bootstrap-theme.min.css" type="text/css">
    <link rel="stylesheet" href="../css/defaults.css" type="text/css">
    <link rel="stylesheet" href="../css/bootstrap-datetimepicker.css" type="text/css">
    <link rel="stylesheet" href="../css/autocomplete.css" type="text/css">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <script type="application/javascript" src="../js/jquery.min.js"></script>
    <script type="application/javascript" src="../js/jquery-ui.min.js"></script>
    <script type="application/javascript" src="../js/bootstrap.min.js"></script>
    <script type="application/javascript" src="../js/validator.min.js"></script>
    <script type="application/javascript" src="../js/jquery.lazy.min.js"></script>
    <script type="application/javascript" src="../js/bootbox.min.js"></script>
    <script type="application/javascript" src="../js/moment.js"></script>
    <script type="application/javascript" src="../js/docked-link.min.js"></script>
    <script type="application/javascript" src="../js/datatables/datatables.js"></script>
    <script type="text/javascript" src="../js/touchspin.min.js"></script>
    <script type="application/javascript" src="../js/caseTypeLoader.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script type="application/javascript">
        $(document).ready(function() {
            $('#updateUserListDataTable').DataTable({
                "ajax": {
                    "url": "AJAX/fetchalluserslist.php",
                    "type": "POST"
                },
                columns: [{
                        "data": "CUSTOMER_ID"
                    },
                    {
                        "data": "USERNAME"
                    },
                    {
                        "data": "EMAIL"
                    },
                    {
                        "data": "Email_Status"
                    },
                    {
                        "data": "Active_Status"
                    },
                    {
                        "data": "BUTTON"
                    }
                ],
                responsive: true
            });
            loadData();
        });

        function loadData() {
            $.ajax({
                url: "AJAX/fetchalluserslist.php",
                type: "POST",
                success: function(results) {
                    window.data = JSON.parse(results);
                    //console.log(data);
                }
            });
        }

        function populateUserlistsDetails(userId) {
            $('#userIdSelector').val(data['data'][userId]['CUSTOMER_ID']);
            //console.log(data['data'][userId]['CUSTOMER_ID']);
            $('#userName').val(data['data'][userId]['USERNAME']);
            $('#userEmail').val(data['data'][userId]['EMAIL']);
            if(data['data'][userId]['EMAIL_VERIFY_STATUS']==1)
                $('#userMailStatus').prop('checked', true).change();
            else
                $('#userMailStatus').prop('checked', false).change();
            //$('#userMailStatus').val(data['data'][userId]['EMAIL_VERIFY_STATUS']);
            if(data['data'][userId]['ACTIVE_STATUS']==1)
                $('#userActiveStatus').prop('checked', true).change();
            else
                $('#userActiveStatus').prop('checked', false).change();
            //$('#userActiveStatus').val(data['data'][userId]['ACTIVE_STATUS']);
        }

        function populateDeleteUserDetails(userID) {
            //console.log(data['data'][caseID]['CASE_ID']);
            $('#deleteUserIDSelector').val(data['data'][userID]['CUSTOMER_ID']);
            $('#deleteUserName').val(data['data'][userID]['USERNAME']);
            $('#deleteUserEmail').val(data['data'][userID]['EMAIL']);
        }
        // console.log($('#userMailStatus').prop());
    </script>
</head>

<body>

    <?php

    session_start();

    error_reporting(0);
    ini_set("display_errors", 0);

    require_once "../processor/adminutils.php";
    require_once "../processor/schedulingutils.php";
    require_once "../processor/registrationutils.php";

    if ((!isset($_SESSION['loginAdminOK']) || ($_SESSION['loginAdminOK'] != 1))) {
        echo "<script type=\"text/javascript\">
                bootbox.alert({
                size: \"large\",
                title: \"Sorry\",
                message: \"There was an error while processing your request. You may try logging-in again.\",
                callback: function() {
                    window.location = \"../index.php\";
                }
                })
                </script>";
        exit();
    }

    ?>

    <!-- Navbar -->

    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="../index.php" class="navbar-brand">
                    <img src="../images/advlogo.jpg" width="28" height="24" />
                </a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav">
                </ul>
                <form class="navbar-form navbar-right" id="recruiterLogout" name="recruiterLogout" method="" action="">
                    <div class="form-group">
                        <input type="button" class="btn btn-sm btn-danger pull-righ;t" onclick="logoutAccount()" name="logoutButtonLargeScreen" id="logoutButtonLargeScreen" value="Logout" />
                        <div class="g-signin2 btn" data-onsuccess="onSignIn" style="display:none"></div>
                        <input type="button" class="btn btn-sm btn-danger center-block" onclick="logoutAccount()" name="logoutButtonSmallScreen" id="logoutButtonSmallScreen" value="Logout" />
                    </div>
                </form>
            </div>
        </div>
    </nav>


    <div class="container" style="">
        <div class="row" id="updateUserListDataRow" style="">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="text-align: center">
                            <strong>
                                <p class="panel-title" id="datePanel"></p>Users List
                            </strong>
                        </div>
                        <div id="" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row" id="updateUserListDataPanel">
                                    <div class='col-md-12 col-xs-12 col-lg-12'>
                                        <div class='form-group'>
                                            <table style="width:100%" class='table table-bordered table-hover table-responsive table-striped' id='updateUserListDataTable'>
                                                <thead>
                                                    <tr style='height: 20px; background-color: lightseagreen; vertical-align: middle'>
                                                        <th style='vertical-align: middle; width: 1%;'>Customer ID</th>
                                                        <th style='vertical-align: middle; width: 1%;'>User Name</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Email ID</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Email Verification Status</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Account Status</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="updateUserListDataTableBody">
                                                </tbody>
                                                <tfoot>
                                                <tr style='height: 20px; background-color: lightseagreen; vertical-align: middle'>
                                                <th style='vertical-align: middle; width: 1%;'>Customer ID</th>
                                                        <th style='vertical-align: middle; width: 1%;'>User Name</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Email ID</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Email Verification Status</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Account Status</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Action</th>
                                                </tr>
                                                </tfoot>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="editUserlistsDetailsModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update User List Data</h4>
                </div>
                <div class="modal-body" style="max-height: 60vh; overflow: auto;">
                    <form name="updateCaseDetailsForm" id="updateCaseDetailsForm">
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="caseNumber">User Name</label>
                            <input type="text" maxlength="7" size="28" name="userName" class="form-control" id="userName" title="Enter Maximum 7 digit of Case Number Compulsory Field" autocomplete="off" disabled="disabled">
                            <input type="hidden" name="userIdSelector" id="userIdSelector" value="">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="Appelant">Email ID</label>
                            <input type="text" class="form-control" name="userEmail" id="userEmail" value="" title="Please enter valid Name" autocomplete="off" disabled="disabled">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="userMailStatus">Email Verification Status</label>
                            <!--input type="text" class="form-control" name="userMailStatus" id="userMailStatus" value="" title="Please enter valid Name" autocomplete="off"-->
                            <input id="userMailStatus" type="checkbox" data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" data-width="50" >
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="userActiveStatus">Account Status</label>
                            <!--input type="text" class="form-control" maxlength="10" name="userActiveStatus" id="userActiveStatus" value="" title="Please enter valid Name" autocomplete="off"-->
                            <input id="userActiveStatus" type="checkbox" data-toggle="toggle" data-on="Active" data-off="Inactive" data-width="100" data-onstyle="success" data-offstyle="danger" >
                            <div class="help-block with-errors"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="updateUserListData" class="btn btn-success pull-left">Update</button>
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="deleteUserDetailsModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Are you sure to delete ?</h4>
                </div>
                <div class="modal-body" style="max-height: 60vh; overflow: auto;">
                    <form name="deleteUserDetailsForm" id="deleteUserDetailsForm">
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="caseNumber">User Name</label>
                            <input type="text" maxlength="7" size="28" name="deleteUserName" class="form-control" id="deleteUserName" title="Enter Maximum 7 digit of Case Number Compulsory Field" autocomplete="off" disabled="disabled">
                            <input type="hidden" name="deleteUserIDSelector" id="deleteUserIDSelector" value="">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="Appelant">Email ID</label>
                            <input type="text" class="form-control" name="deleteUserEmail" id="deleteUserEmail" value="" title="Please enter valid Name" autocomplete="off" disabled="disabled">
                            <div class="help-block with-errors"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="deleteUserDetails" class="btn btn-danger pull-left">Confirm</button>
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script type="application/javascript">
        $('#updateUserListData').click(function() {
            var isCheckedMail = document.getElementById("userMailStatus").checked;
            var isCheckedAcc = document.getElementById("userActiveStatus").checked;
            if(isCheckedMail)
                isCheckedMail = 1;
            else    
                isCheckedMail = 0;
            if(isCheckedAcc)
                isCheckedAcc = 1;
            else    
                isCheckedAcc = 0;
        //console.log(isChecked);
        $.ajax({
                url: "AJAX/updateuserslistdata.php",
                type: "POST",
                data: {
                    'userId': $('#userIdSelector').val(),
                    'emailStatus': isCheckedMail,
                    'accStatus': isCheckedAcc
                },
                success: function(results) {
                    bootbox.alert({
                        size: "small",
                        title: "Success",
                        message: "User List " + results + " has been successfully updated.",
                    });
                    var table = $('#updateUserListDataTable').DataTable();
                    table.ajax.reload();
                    loadData();
                    $("#editUserlistsDetailsModal").modal('hide');
                }
            });
        });

        $('#deleteUserDetails').click(function() {
            $.ajax({
                url: "AJAX/deleteuserdetails.php",
                type: "POST",
                data: {
                    'userId': $('#deleteUserIDSelector').val(),
                    'userName' : $('#deleteUserName').val()
                },
                success: function(results) {
                    bootbox.alert({
                        size: "small",
                        title: "Success",
                        message: "User " + results + " has been successfully deleted.",
                    });
                    var table = $('#updateUserListDataTable').DataTable();
                    table.ajax.reload();
                    loadData();
                    $("#deleteUserDetailsModal").modal('hide');
                }
            });
        });

        function logoutAccount() {
            window.open("../logout.php", '_self');
        }
    </script>

</body>

</html>