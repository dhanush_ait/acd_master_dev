-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 21, 2018 at 02:45 AM
-- Server version: 5.7.21-log
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `case dairy`
--

-- --------------------------------------------------------

--
-- Table structure for table `case_entry`
--

DROP TABLE IF EXISTS `case_entry`;
CREATE TABLE IF NOT EXISTS `case_entry` (
  `Ca_id` int(11) NOT NULL,
  `C_id` varchar(11) NOT NULL,
  `C_name` varchar(55) NOT NULL,
  `Ca_type` varchar(55) NOT NULL,
  `Court_name` varchar(55) NOT NULL,
  `Opp_cause` varchar(50) NOT NULL,
  `ca_year` year(4) NOT NULL,
  PRIMARY KEY (`Ca_id`,`ca_year`),
  UNIQUE KEY `C_id` (`C_id`),
  KEY `C_name` (`C_name`),
  KEY `Opp_cause` (`Opp_cause`),
  KEY `ca_year` (`ca_year`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `case_entry`
--

INSERT INTO `case_entry` (`Ca_id`, `C_id`, `C_name`, `Ca_type`, `Court_name`, `Opp_cause`, `ca_year`) VALUES
(34, 'C003', 'Bharath', 'OS', 'JRDIV', 'Charan', 2012),
(123, 'C002', 'Sooraj', 'CR', 'SRDIV', 'Sal', 2008),
(189, 'C005', 'Harshith', 'CC', 'CIVIL', 'Rakshith', 2015),
(234, 'C001', 'Dhanush', 'OS', 'SRDIV', 'None', 2005);

-- --------------------------------------------------------

--
-- Table structure for table `case_proceeding`
--

DROP TABLE IF EXISTS `case_proceeding`;
CREATE TABLE IF NOT EXISTS `case_proceeding` (
  `Ca_id` int(11) NOT NULL,
  `ca_year` year(4) NOT NULL,
  `Proc_Summ` varchar(55) DEFAULT NULL,
  `Proc_date` date DEFAULT NULL,
  `N_hearing_date` date DEFAULT NULL,
  `C_name` varchar(55) NOT NULL,
  `Disposal` date DEFAULT NULL,
  `Result` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`Ca_id`),
  KEY `C_name` (`C_name`),
  KEY `case_proceeding_ibfk_3` (`ca_year`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `case_proceeding`
--

INSERT INTO `case_proceeding` (`Ca_id`, `ca_year`, `Proc_Summ`, `Proc_date`, `N_hearing_date`, `C_name`, `Disposal`, `Result`) VALUES
(34, 2012, 'sdkhakjh', '2018-04-19', '2018-04-20', 'Bharath', '2018-04-30', 'None'),
(123, 2008, 'gfdgfdgf', '2018-04-11', '2018-04-22', 'Sooraj', '2018-04-24', 'NA'),
(189, 2015, 'kjkjh', '2018-04-14', '2018-04-21', 'Harshith', '2018-04-30', 'hgfhgfy'),
(234, 2005, 'gafrsdda', '2018-04-28', '2018-04-21', 'Dhanush', '2018-04-28', 'Result');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `C_id` varchar(11) NOT NULL,
  `C_name` varchar(55) NOT NULL,
  `C_Address` varchar(55) DEFAULT NULL,
  `City` varchar(55) DEFAULT NULL,
  `C_state` varchar(25) DEFAULT NULL,
  `C_gender` varchar(10) DEFAULT NULL,
  `C_contact` int(11) DEFAULT NULL,
  `C_email` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`C_id`),
  KEY `C_name` (`C_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`C_id`, `C_name`, `C_Address`, `City`, `C_state`, `C_gender`, `C_contact`, `C_email`) VALUES
('C001', 'Dhanush', 'KST', 'Madhugiri', 'Karnataka', 'MALE', 879368, 'ddgajg@gmail.com'),
('C002', 'Sooraj', 'dsnbjadhjv', 'jhvjhv', 'hvhjvjh', 'MALE', 8767676, 'bsjdjhadajjha'),
('C003', 'Bharath', 'sjdsahgdsg', 'hjhvghgv', 'jhhjvjhv', 'mALE', 65456456, 'hggjhgjghj'),
('C005', 'Harshith', 'jhhjh', 'hjhj', 'gjhgg', 'mLE', 8055, 'jhhvhvhg');

-- --------------------------------------------------------

--
-- Table structure for table `fee`
--

DROP TABLE IF EXISTS `fee`;
CREATE TABLE IF NOT EXISTS `fee` (
  `C_id` varchar(11) NOT NULL,
  `C_name` varchar(55) NOT NULL,
  `Reciept_id` varchar(11) NOT NULL,
  `T_amt` int(11) NOT NULL,
  `amt_paid` int(11) DEFAULT NULL,
  `balance` int(11) DEFAULT NULL,
  `pay_mode` varchar(10) NOT NULL,
  PRIMARY KEY (`C_id`),
  UNIQUE KEY `C_id` (`C_id`),
  KEY `Reciept_id` (`Reciept_id`),
  KEY `C_name` (`C_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fee`
--

INSERT INTO `fee` (`C_id`, `C_name`, `Reciept_id`, `T_amt`, `amt_paid`, `balance`, `pay_mode`) VALUES
('C001', 'Dhanush', '101', 2000, 1500, 500, 'cash'),
('C002', 'Sooraj', '102', 6000, 4000, 2000, 'cheque'),
('C003', 'Bharath', '103', 2300, 1800, 500, 'cash'),
('C005', 'Harshith', '105', 4000, 3000, 4000, 'card ');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_at`) VALUES
(1, 'darklord', '$2y$10$PQJIu1MqJm.b.ArI2PJQCOlvnFMKoPrQeqhvJsPWLh4xTLGFYnfOW', '2018-04-11 22:28:42'),
(2, 'dhanu', '$2y$10$oskG9w7RkhADA70uEzHPUemo13zjiFh5AfYV1A2fSrC/NpId1Ui56', '2018-04-11 22:37:16'),
(3, 'vinayig', '$2y$10$/hM7R2hqfi/xkZyKN5ENg.v6Kz9VVsu/aGq7aRr2WlHbuXeItrjbu', '2018-04-12 07:42:44'),
(4, 'karthik', '$2y$10$iv17CiC3OA5H90ZNYbhZR.jMzKH7VPGkaS09da6hwcwD6kRtkiR5.', '2018-04-12 11:03:17');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

DROP TABLE IF EXISTS `user_details`;
CREATE TABLE IF NOT EXISTS `user_details` (
  `A_name` varchar(55) DEFAULT NULL,
  `A_id` int(11) NOT NULL,
  `U_age` int(11) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `gender` varchar(5) DEFAULT NULL,
  `qualification` varchar(55) DEFAULT NULL,
  `salary` int(11) DEFAULT NULL,
  PRIMARY KEY (`A_id`),
  KEY `A_name` (`A_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`A_name`, `A_id`, `U_age`, `email`, `gender`, `qualification`, `salary`) VALUES
('Darshan', 1, 25, 'darshan@hotmail.com', 'MALE', 'B.A', 5000),
('Dhanush', 2, 27, 'dhanush257@gmail.com', 'MALE', 'B.E LLB', 15000),
('Vinay Kumar I G', 3, 22, 'vinayig@gmail.com', 'MALE', 'B.E ', 5000),
('KArthik', 4, 27, 'karthik@hotmail.ocm', 'male', 'be', 2300);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `case_proceeding`
--
ALTER TABLE `case_proceeding`
  ADD CONSTRAINT `case_proceeding_ibfk_1` FOREIGN KEY (`Ca_id`) REFERENCES `case_entry` (`Ca_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `case_proceeding_ibfk_3` FOREIGN KEY (`ca_year`) REFERENCES `case_entry` (`ca_year`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `case_proceeding_ibfk_4` FOREIGN KEY (`C_name`) REFERENCES `case_entry` (`C_name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `client_ibfk_1` FOREIGN KEY (`C_id`) REFERENCES `case_entry` (`C_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `client_ibfk_2` FOREIGN KEY (`C_name`) REFERENCES `case_entry` (`C_name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fee`
--
ALTER TABLE `fee`
  ADD CONSTRAINT `fee_ibfk_1` FOREIGN KEY (`C_id`) REFERENCES `case_entry` (`C_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_details`
--
ALTER TABLE `user_details`
  ADD CONSTRAINT `user_details_ibfk_1` FOREIGN KEY (`A_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
