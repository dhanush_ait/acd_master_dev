<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>ACDWeb | Welcome</title>

    <meta name="google-signin-client_id" content="709502795016-qbft7m0mc6m6ekkdu5esdefsvlsbj0s5.apps.googleusercontent.com">

    <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css">

    <link rel="stylesheet" href="css/defaults.css" type="text/css">
    <link rel="stylesheet" href="css/php_pages/landing_php.css" type="text/css">
    <link rel="stylesheet" href="css/bootstrap-datetimepicker.css" type="text/css">
    <link rel="stylesheet" href="css/autocomplete.css" type="text/css">

    <link href='fullcalendar/packages/core/main.css' rel='stylesheet' />
    <link href='fullcalendar/packages/daygrid/main.css' rel='stylesheet' />
    <link href='fullcalendar/packages/list/main.css' rel='stylesheet' />
    <link href='fullcalendar/packages/timegrid/main.css' rel='stylesheet' />
    <link href='fullcalendar/packages/bootstrap/main.css' rel='stylesheet' />

    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script type="application/javascript" src="js/jquery.min.js"></script>
    <script type="application/javascript" src="js/jquery-ui.min.js"></script>
    <script type="application/javascript" src="js/bootstrap.min.js"></script>
    <script type="application/javascript" src="js/validator.min.js"></script>
    <script type="application/javascript" src="js/jquery.lazy.min.js"></script>
    <script type="application/javascript" src="js/bootbox.min.js"></script>
    <script type="application/javascript" src="js/docked-link.min.js"></script>
    <script type="application/javascript" src="js/moment.js"></script>
    <script type="application/javascript" src="js/caseTypeLoader.js"></script>
    <script type="application/javascript" src="js/datatables/datatables.js"></script>
    <script type="application/javascript" src="https://apis.google.com/js/platform.js" async defer></script>


    <script src='fullcalendar/packages/core/main.js'></script>
    <script src='fullcalendar/packages/interaction/main.js'></script>
    <script src='fullcalendar/packages/google-calendar/main.js'></script>
    <script src='fullcalendar/packages/daygrid/main.js'></script>
    <script src='fullcalendar/packages/timegrid/main.js'></script>
    <script src='fullcalendar/packages/list/main.js'></script>
    <script src='fullcalendar/packages/bootstrap/main.js'></script>

    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


    <link href='https://use.fontawesome.com/releases/v5.0.6/css/all.css' rel='stylesheet'>

    <script defer src="https://apis.google.com/js/api.js" onload="this.onload=function(){};handleClientLoad()" onreadystatechange="if (this.readyState === 'complete') this.onload()">
    </script>

    <script type="application/javascript">
        var eventlist = [];
        var CLIENT_ID = '709502795016-qbft7m0mc6m6ekkdu5esdefsvlsbj0s5.apps.googleusercontent.com';
        var API_KEY = 'AIzaSyB1vsZc8ZVzD37jvKfvWVXN9pFfL0Khyrg';
        var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];
        var SCOPES = "https://www.googleapis.com/auth/calendar.events.readonly";

        function handleClientLoad() {
            gapi.load('client:auth2', initClient);
        }

        function initClient() {
            gapi.client.init({
                apiKey: API_KEY,
                clientId: CLIENT_ID,
                discoveryDocs: DISCOVERY_DOCS,
                scope: SCOPES
            }).then(function() {
                gapi.auth2.getAuthInstance();
                //console.log(gapi.auth2.getAuthInstance().isSignedIn.get());
                gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
                updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
            }, function(error) {
                appendPre(JSON.stringify(error, null, 2));
            });
        }

        function updateSigninStatus(isSignedIn) {
            if (isSignedIn) {
                listUpcomingEvents();
            } else {
                gapi.auth2.getAuthInstance().signIn();
                //console.log("not signed in");
                /*bootbox.alert({
                          size: "medium",
                          title: "Sorry",
                          message: "Please login with Google",
                  });*/
            }
        }

        function listUpcomingEvents() {
            gapi.client.calendar.events.list({
                'calendarId': 'primary',
                //'timeMin': (new Date()).toISOString(),
                'showDeleted': false,
                'singleEvents': true,
                'maxResults': 500,
                'orderBy': 'startTime'
            }).then(function(response) {
                    var events = response.result.items;
                    //appendPre('Upcoming events:');
                    if (events.length > 0) {
                        for (i = 0; i < events.length; i++) {
                            var event = events[i];
                            var when = event.start.dateTime;
                            if (when) {
                                when = moment(when).format('DD-MM-YYYY');
                            }
                            if (!when) {
                                when = event.start.date;
                                when = moment(when).format('DD-MM-YYYY');
                            }
                            if (eventlist[when]) {
                                eventlist[when] = event.summary + " , " + eventlist[when];
                            } else {
                                eventlist[when] = event.summary;
                            }
                            //appendPre(when + " : "+ event.summary)
                        }
                    } else {
                        eventlist[0] = "No events found";
                    }
                    //console.logconsole.log(eventlist);
                },
                function(error) {
                    //console.error("Execute error", error);
                    if (error.code == 403) {
                        //console.log("No access")
                    }
                });
        }

        function calldatatable() {
            $.fn.dataTable.ext.errMode = 'none';
            var table = $('#caseListTable').DataTable({
                "ajax": {
                    "url": "authuser/AJAX/getcaselistfordateclicked.php",
                    "type": "POST"
                },
                columns: [{
                        "data": "FULL_ID"
                    },
                    {
                        "data": "CASE_DEFENDANT"
                    },
                    {
                        "data": "CASE_OPPONENT"
                    },
                    {
                        "data": "NEXT_DATE"
                    },
                    {
                        "data": "CLIENT_CONTACT"
                    },
                    {
                        "data": "CASE_SUMMARY"
                    },
                    {
                        "data": "BUTTON"
                    }
                ],
                responsive: true,
                select: true,
                columnDefs: [{
                        responsivePriority: 1,
                        targets: 0
                    },
                    {
                        responsivePriority: 2,
                        targets: 1
                    },
                    {
                        visible: false,
                        targets: 3
                    }
                ]
            });
            return table;
        }
    </script>

</head>

<body>

    <?php
    session_start();

    if (!isset($_SESSION['emailID']) || $_SESSION['userActive'] != 1) {
        echo "<script type=\"text/javascript\">
                bootbox.alert({
                size: \"large\",
                title: \"Sorry\",
                message: \"There was an error while processing your request. You may try logging-in again.\",
                callback: function() {
                    window.location = \"index.php\";
                }
                })
                </script>";
        echo "1";
        exit();
    }
    ?>

    <!-- Navbar -->

    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="../index.php" class="navbar-brand">
                    <img src="images/advlogo.jpg" width="28" height="24" />
                </a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav">
                    <li><a href="welcome.php">HOME</a></li>
                    <li><a href="updatecaselist.php">SEARCH</a></li>
                    <li><a href="updateclient.php">CLIENTS</a></li>
                    <li><a href="cupdate.php">NOTIFICATIONS</a></li>
                </ul>
                <form class="navbar-form navbar-right" id="recruiterLogout" name="recruiterLogout" method="" action="">
                    <div class="form-group">
                        <input type="button" class="btn btn-sm btn-danger pull-righ;t" onclick="logoutAccount()" name="logoutButtonLargeScreen" id="logoutButtonLargeScreen" value="Logout" />
                        <div class="g-signin2 btn" data-onsuccess="onSignIn" style="display:none"></div>
                        <input type="button" class="btn btn-sm btn-danger center-block" onclick="logoutAccount()" name="logoutButtonSmallScreen" id="logoutButtonSmallScreen" value="Logout" />
                    </div>
                </form>
            </div>
        </div>
    </nav>

    <div class="container" style="padding-top: 1%;">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <p class="panel-title" style='text-align: center'>Admin Tasks</p>
                    </div>
                    <div id="panel-element-344374" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class='row' id='largescreenAdminTask'>
                                <div class="col-lg-2">
                                </div>
                                <div class="col-xs-12 col-lg-3">
                                    <div class='form-group'>
                                        <button data-toggle='modal' id='addNewCase' data-target='#addNewCaseModal' class="btn btn-info form-control">New Case</button>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-lg-3">
                                    <div class='form-group'>
                                        <button data-toggle='modal' id='addUserData' data-target='#updateUserDataModal' onclick="populateUserDataModal()" class="btn btn-success form-control">Update User Data</button>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-lg-3">
                                    <div class='form-group'>
                                        <button data-toggle='modal' id='updateCaseProceeding' data-target='#updateCaseProceedingModal' class="btn btn-warning form-control">Update Case Proceeding</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container" style="padding-top: 1%;">
        <div class="row">
            <div class="col-md-2 col-lg-2"></div>
            <div class="col-xs-12 col-md-8 col-lg-8">
                <div class="panel-group" id="panel-venue">
                    <div class="panel panel-default" data-toggle="collapse">
                        <div class="panel-heading">
                            <p class="panel-title" style="text-align: center">Calendar of Events</strong></p>
                        </div>
                        <div id="panel-element-venue" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-lg-12">
                                        <div id="calendar" class="fullcalendar">
                                            <input type="hidden" value="" id="preDayClickName" />

                                            <script type="application/javascript">
                                                //New full calendar work
                                                document.addEventListener('DOMContentLoaded', function() {
                                                    var calendarEl = document.getElementById('calendar');
                                                    var calendar = new FullCalendar.Calendar(calendarEl, {
                                                        plugins: ['interaction', 'dayGrid', 'timeGrid', 'googleCalendar', 'list', 'bootstrap'],
                                                        googleCalendarApiKey: 'AIzaSyB1vsZc8ZVzD37jvKfvWVXN9pFfL0Khyrg',
                                                        eventSources: [{
                                                            googleCalendarId: 'en.indian#holiday@group.v.calendar.google.com',
                                                            color: 'yellow', // an option!
                                                            textColor: 'black' // an option!
                                                        }],
                                                        header: { // layout header
                                                        },
                                                        eventClick: function(info) {
                                                            alert('Event: ' + info.event.title);
                                                            // change the border color just for fun
                                                            info.el.style.borderColor = 'red';
                                                        },
                                                        firstDay: 1,
                                                        themeSystem: 'bootstrap',
                                                        dateClick: function(info) {
                                                            DateClicked = moment(info.date).format('DD-MM-YYYY');;
                                                            $("#datePanel").empty();
                                                            $("#datePanel").append(DateClicked);
                                                            $("#todayEvents").empty();
                                                            //console.log(eventlist[DateClicked]);
                                                            if (eventlist[DateClicked]) {
                                                                $("#todayEvents").append("<li><strong>" + eventlist[DateClicked] + "</li>");
                                                            } else {
                                                                $("#todayEvents").append("<li><strong>No events in Calendar for this date.</li>");
                                                            }
                                                            DateClicked = moment(info.date).format('DD-MM-YYYY');
                                                            //OpenCaseList(DateClicked);
                                                            filterGlobal(DateClicked);
                                                            // Scroll to panel
                                                            $('html, body').animate({
                                                                scrollTop: $("#caseListPanel").offset().top
                                                            }, 1000);
                                                        }
                                                    });
                                                    calendar.render();
                                                });
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-lg-2"></div>
        </div>
    </div>

    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="addNewCaseModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add New Case</h4>
                </div>
                <div class="modal-body" style="max-height: 60vh; overflow: auto;">
                    <form name="registerNewCaseForm" id="registerNewCaseForm" data-toggle="validator" onsubmit="">
                        <div class="form-group col-xs-12 col-md-12 col-lg-6">
                            <label class="control-label">Court Complex *</label>
                            <select name="court_complex_code" id="court_complex_code" class="form-control" onchange="">
                                <option value="0">Select Court Complex</option>
                                <option value="1@12,19">Court Complex- C.N.Halli</option>
                                <option value="2@5,6">Court Complex- Gubbi</option>
                                <option value="3@4">Court Complex- Koratagere</option>
                                <option value="4@10,11">Court Complex- Kunigal</option>
                                <option value="5@23,24,25,26">Court Complex- Madhugiri</option>
                                <option value="6@9,18">Court Complex- Pavagada</option>
                                <option value="7@7,8">Court Complex- Sira</option>
                                <option value="9@15,14">Court Complex- Turuvekere</option>
                                <option value="101@1,2,3,13">District Court Complex-Tumakuru</option>
                                <option value="102@20,21,22">Court Complex Tiptur</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="caseType">Case Type*</label>
                            <select name="case_type" id="case_type" class="form-control" onchange="">
                                <option value="0">Select Case Type</option>
                                <option value="3">AA - Arbitration Application</option>
                                <option value="1">A.C. - Arbitration Cases</option>
                                <option value="5">Appl.10(1)(c) - Application u/s 10(1)(c)</option>
                                <option value="6">Appl.10(4)(A) - Application u/s 10(4)(A)</option>
                                <option value="7">Appl.33(2)b - Application u/s 33(2)b</option>
                                <option value="8">APPL.33(C)(2)</option>
                                <option value="10">APPLN - Application for Wakf Board</option>
                                <option value="9">Appl.u/s 11 - Application u/s 11</option>
                                <option value="2">A.S. - Arbitration Suits</option>
                                <option value="11">C.C. - CRIMINAL CASES</option>
                                <option value="12">C.O.A. - Company Applications</option>
                                <option value="13">C.O.P. - Company Petitions</option>
                                <option value="15">Cr - Crime Case</option>
                                <option value="16">CRL.A - CRIMINAL APPEAL</option>
                                <option value="17">CRL.M.A. - CRIMINAL MISC.APPEAL</option>
                                <option value="18">Crl.Misc. - CRIMINAL MISC.CASES</option>
                                <option value="19">Crl.Misc.(DVA) - CRL MISC(Domestic Voilence)</option>
                                <option value="20">CRL.R.P. - CRIMINAL REVISION PETITIONS</option>
                                <option value="21">E.A.T. - Education Appellate Tribunal C</option>
                                <option value="67">E.C.A. - Employees Compensation Act</option>
                                <option value="22">ELEC.C - ELECTION PETITIONS</option>
                                <option value="65">Ele.Misc. - 0/12</option>
                                <option value="66">Ele.Petn. - Election Petition</option>
                                <option value="23">EX - Execution Petition Under Order</option>
                                <option value="24">Ex.A. - Execution Appeals</option>
                                <option value="25">Ex.C - Execution Cases</option>
                                <option value="26">FDP - Petitioner For Final Decree pr</option>
                                <option value="28">G and W.C. - Guardian and Wards Cases</option>
                                <option value="27">G and WC CASE - Appointment Of Guardian, Other</option>
                                <option value="29">H.R.C. - House Rent Control Cases</option>
                                <option value="30">H.R.C.A. - House Rent Control Appeals</option>
                                <option value="31">I.C. - Insolvency Cases</option>
                                <option value="34">IDact-S10 - Under Sec 10 of ID Act (Ref.Ca</option>
                                <option value="64">IDact-S10(1)(C) - Industrial Disputes u/s 10(1)(</option>
                                <option value="35">IDact-S10(1)(d) - Industrial Disputes u/s 10(1)(</option>
                                <option value="32">IDact-S.33 - Serial Applications Under Sec.</option>
                                <option value="36">IDact-S33(2)(b) - Approval Application u/s 33(2)</option>
                                <option value="37">IDact-S33(A) - Under Sec 33(A) of ID Act (Com</option>
                                <option value="33">IDact-S.A - Complaints under Sec.A of the</option>
                                <option value="38">IID.1947,Sec.32 (A).</option>
                                <option value="39">Ind Emp-SO Act - Appeal under Industrial Employ</option>
                                <option value="40">J.C. - JUVENILE CASES</option>
                                <option value="41">KID 10-4(A)</option>
                                <option value="42">L.A.C. - Land Acquisition Cases</option>
                                <option value="44">LAC(APPL) - L.A.C.APPEAL</option>
                                <option value="45">M.A. - Miscellanuous Appeals</option>
                                <option value="48">MA(EAT) - Appeal Under Education Act</option>
                                <option value="46">M.C. - MATRIMONIAL CASES</option>
                                <option value="49">Misc - Miscellaneous Cases</option>
                                <option value="50">Misc.Appln. - Miscelleneous Application</option>
                                <option value="47">M.V.C. - Accident Claim Cases u/r M.V.</option>
                                <option value="51">O.L. - OTHER LAW CASES</option>
                                <option value="52">O.S. - Original Suit</option>
                                <option value="54">P and Sc - Probate and Succession Cases</option>
                                <option value="55">P.C.R. - PRIVATE COMPLAINTS</option>
                                <option value="56">P.MIS. - Petition Filed Indegent Person</option>
                                <option value="53">P SC - Petition For Succession Certif</option>
                                <option value="57">R.A. - Regular Appeals</option>
                                <option value="58">R.C.(E)</option>
                                <option value="59">R.E.V. - Revision Petitions</option>
                                <option value="68">REVIEW PETITION</option>
                                <option value="60">R.E.V. (RENT) - Revision Petition Under Rent C</option>
                                <option value="62">SC - SESSION CASES</option>
                                <option value="61">S.C. - Small Cause Suit</option>
                                <option value="63">SPL.C - SPECIAL CASES</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="CaseNumber">Case Number *</label>
                            <input type="text" maxlength="7" size="28" name="search_case_no" class="form-control" id="search_case_no" title="Enter Maximum 7 digit of Case Number Compulsory Field" autocomplete="off">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label">Year *</label>
                            <select name="case_rgyear" id="case_rgyear" class="form-control">
                                <option value="0">Select Year</option>
                                <option value="2000">2000</option>
                                <option value="2001">2001</option>
                                <option value="2002">2002</option>
                                <option value="2003">2003</option>
                                <option value="2004">2004</option>
                                <option value="2005">2005</option>
                                <option value="2006">2006</option>
                                <option value="2007">2007</option>
                                <option value="2008">2008</option>
                                <option value="2009">2009</option>
                                <option value="2010">2010</option>
                                <option value="2011">2011</option>
                                <option value="2012">2012</option>
                                <option value="2013">2013</option>
                                <option value="2014">2014</option>
                                <option value="2015">2015</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="jbContactEmail">Appelant *</label>
                            <input type="text" class="form-control" name="case_appelant" id="case_appelant" value="" title="Please enter valid Name" autocomplete="off">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label">Client Email ID*</label>
                            <input type="email" name="clientEmail" id="clientEmail" class="form-control" maxlength="254" placeholder="Valid e-mail address" pattern="^[\w\-\.]+@[a-zA-Z_0-9]+?((\.[a-zA-Z]{2,3})|(\.[a-zA-Z]{2,3}\.[a-zA-Z]{2,3}))$" data-pattern-error="Invalid email address" data-required-error="This is a mandatory field" required="true" />
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="jbContactEmail">Opponent *</label>
                            <input type="text" class="form-control" name="case_opponent" id="case_opponent" value="" title="Please enter valid Name" autocomplete="off">
                            <div class="help-block with-errors"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="saveNewCase" class="btn btn-success pull-left">Save</button>
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="updateUserDataModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update User Data</h4>
                </div>
                <div class="modal-body" style="max-height: 60vh; overflow: auto;">
                    <form name="updateCaseForm" id="updateCaseForm">
                        <div class="form-group col-xs-12 col-md-12 col-lg-6">
                            <label class="control-label">Court Complex *</label>
                            <select name="user_court_complex_code" id="user_court_complex_code" class="form-control" onchange="">
                                <option value="0">Select Court Complex</option>
                                <option value="1@12,19">Court Complex- C.N.Halli</option>
                                <option value="2@5,6">Court Complex- Gubbi</option>
                                <option value="3@4">Court Complex- Koratagere</option>
                                <option value="4@10,11">Court Complex- Kunigal</option>
                                <option value="5@23,24,25,26">Court Complex- Madhugiri</option>
                                <option value="6@9,18">Court Complex- Pavagada</option>
                                <option value="7@7,8">Court Complex- Sira</option>
                                <option value="9@15,14">Court Complex- Turuvekere</option>
                                <option value="101@1,2,3,13">District Court Complex-Tumakuru</option>
                                <option value="102@20,21,22">Court Complex Tiptur</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="userBarCode">BAR Code*</label>
                            <input type="text" maxlength="3" size="3" class="form-control col-xs-4 col-md-4 col-lg-4" name="user_bar_code_1" id="user_bar_code_1" placeholder="State Code Ex: KAR" autocomplete="off">
                            <input type="text" maxlength="5" size="5" class="form-control col-xs-4 col-md-4 col-lg-4" name="user_bar_code_2" id="user_bar_code_2" placeholder="BAR CODE" autocomplete="off">
                            <input type="text" maxlength="4" size="4" class="form-control col-xs-4 col-md-4 col-lg-4" name="user_bar_code_3" id="user_bar_code_3" placeholder="YEAR" autocomplete="off">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="regUserName">Registered Name*</label>
                            <input type="text" class="form-control" name="regUserName" id="regUserName" value="" title="Please enter valid Name" autocomplete="off">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="jbContactEmail">Last Update On</label>
                            <input type="text" class="form-control" name="userLastUpdate" id="userLastUpdate" value="" title="" autocomplete="off" disabled="disabled">
                            <div class="help-block with-errors"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="updateUserData" class="btn btn-success pull-left">Update</button>
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="emailClientModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Email Body</h4>
                </div>
                <div class="modal-body" style="max-height: 62vh; overflow: auto;">
                    <form name="emailClientForm" id="emailClientForm">
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label">Case No.</label>
                            <input type="text" name="emailCaseNo" id="emailCaseNo" class="form-control" maxlength="64" placeholder="Provide city" data-required-error="This is a mandatory field" readonly />
                            <input type="hidden" name="emailCaseIDSelector" id="emailCaseIDSelector" value="">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="emailClientName">Client Name</label>
                            <input type="text" name="emailClientName" id="emailClientName" class="form-control" maxlength="128" placeholder="Provide full name of the company" data-required-error="This is a mandatory field" readonly />
                            <input type="hidden" name="emailClientIDSelector" id="emailClientIDSelector" value="">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label">Email ID</label>
                            <input type="text" name="emailID" id="emailID" class="form-control" maxlength="35" placeholder="Provide a valid Email address" data-required-error="This is a mandatory field" disabled />
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label">Date of Call*</label>
                            <input type="text" name="emailNextDate" id="emailNextDate" class="form-control" maxlength="64" placeholder="Provide state" data-required-error="This is a mandatory field" readonly />
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-12 col-lg-12">
                            <label class="control-label">Case Summary*</label>
                            <input type="text" name="emailCaseStatus" id="emailCaseStatus" class="form-control" maxlength="64" placeholder="Provide street address of the venue" data-required-error="This is a mandatory field" required />
                            <div class="help-block with-errors"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="sendEmailButton" class="btn btn-success pull-left">Send</button>
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row" id="caseListPanel" style="">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="text-align: center">
                            <strong>
                                <p class="panel-title" id="datePanel"></p>
                            </strong>
                        </div>
                        <div id="" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-lg-12">
                                        <p>Google Calendar Events :
                                            <strong>
                                                <ul id="todayEvents">
                                                </ul>
                                            </strong>
                                        </p>
                                    </div>
                                </div>
                                <div class="row" id="caseListPanel">
                                    <div class='col-md-12 col-xs-12 col-lg-12'>
                                        <div class='form-group'>
                                            <table style="width:100%" class='table table-bordered table-hover table-responsive table-striped' id='caseListTable'>
                                                <thead style="width:100%">
                                                    <tr style='height: 20px; background-color: lightseagreen; vertical-align: middle'>
                                                        <th style='vertical-align: middle; width: 1%;'>Case No.</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Defendant</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Opponent</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Next Hearing</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Contact</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Status</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Notify</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="updateCaseListTableBody">
                                                </tbody>
                                                <tfoot style="width:100%">
                                                    <tr style='height: 20px; background-color: lightseagreen; vertical-align: middle'>
                                                        <th style='vertical-align: middle; width: 1%;'>Case No.</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Defendant</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Opponent</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Next Hearing</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Contact</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Status</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Notify</th>
                                                    </tr>
                                                </tfoot>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="application/javascript">
        var data, globaldate;
        //Initial Load Data
        window.onload = function() {
            $.ajax({
                url: "authuser/AJAX/getcaselistfordateclicked.php",
                type: "POST",
                success: function(result) {
                    data = JSON.parse(result);
                    //console.log(data);
                }
            });
        }

        // Alternative to load event
        document.onreadystatechange = function() {
            if (document.readyState === 'complete') {
                initApplication();
            }
        }

        //Load today's case list
        function initApplication() {
            var date = moment(date).format('DD-MM-YYYY');
            globaldate = date;
            $("#datePanel").append(date);
            if (eventlist[date]) {
                $("#todayEvents").append("<li><strong>" + eventlist[date] + "</li>");
            } else {
                $("#todayEvents").append("<li><strong>No events in Calendar for this date.</li>");
            }
            filterGlobal(date);
        }

        //Search
        function filterGlobal(date) {
            var table = calldatatable();
            //table.draw();
            table.search(date).draw();
        }

        //Add new case
        $('#saveNewCase').click(function() {
            var case_rgyear = document.getElementById('case_rgyear').value;
            console.log(case_rgyear);
            var case_type = document.getElementById('case_type').value;
            var court_complex_code = document.getElementById('court_complex_code').value;
            //Validation
            if (document.getElementById('search_case_no').value == 0) {
                alert("Please Enter Case Number");
                document.getElementById('search_case_no').value = "";
                document.getElementById('search_case_no').focus();
                return false;
            }

            if (document.getElementById('search_case_no').value == ' ') {
                alert("Please Enter Case Number");
                document.getElementById('search_case_no').value = "";
                document.getElementById('search_case_no').focus();
                return false;
            }

            if (isNaN(document.getElementById('search_case_no').value) == true) {
                alert("Please Enter Numeric Value");
                document.getElementById('search_case_no').value = "";
                document.getElementById('search_case_no').focus();
                return false;
            }

            if (checkInteger(document.getElementById('search_case_no').value) == false) {
                alert("Please Enter Numeric Value");
                document.getElementById('search_case_no').value = "";
                document.getElementById('search_case_no').focus();
                return false;
            }
            if (case_rgyear == 0) {
                alert("Please Select an Year");
                document.getElementById('case_rgyear').value = "";
                document.getElementById('case_rgyear').focus();
                return false;
            }
            if (case_type == "" || case_type == 0) {
                alert("Please Select a Case Type");
                document.getElementById('case_type').value = "";
                document.getElementById('case_type').focus();
                return false;
            }
            if (court_complex_code == "" || court_complex_code == 0) {
                alert("Please select court complex");
                document.getElementById('court_complex_code').value = "";
                document.getElementById('court_complex_code').focus();
                return false;
            }

            var captchaObj = document.getElementsByName('case_appelant');
            if (captchaObj[0].value == "") {
                alert("Please enter appelant name");
                captchaObj[0].focus();
                return false;
            }
            var captchaVal = captchaObj[0].value;
            var reg = new RegExp("^([a-zA-Z]{2,}(\\s[a-zA-z]{1,})?'?-?([a-zA-Z]{2,}\\s)?([a-zA-Z]{1,})?)$");
            if (!captchaVal.match(reg)) {
                alert("Enter only alphabetic characters in appelant name");
                captchaObj[0].value = '';
                captchaObj[0].focus();
                return false;
            }

            var oppObj = document.getElementsByName('case_opponent');
            if (oppObj[0].value == "") {
                alert("Please enter opponent name");
                oppObj[0].focus();
                return false;
            }
            var oppVal = oppObj[0].value;
            var reg = new RegExp("^([a-zA-Z]{2,}(\\s[a-zA-z]{1,})?'?-?([a-zA-Z]{2,}\\s)?([a-zA-Z]{1,})?)$");
            var oppVal = oppObj[0].value;
            if (!oppVal.match(reg)) {
                alert("Enter only alphabetic characters in oponent name");
                oppObj[0].value = '';
                oppObj[0].focus();
                return false;
            }

            var emailObj = document.getElementsByName('clientEmail');
            if (emailObj[0].value == "") {
                alert("Please enter Email");
                emailObj[0].focus();
                return false;
            }

            var emailVal = emailObj[0].value;
            var reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var emailVal = emailObj[0].value;
            if (!reg.test(emailVal)) {
                alert("Enter only valid email");
                emailObj[0].value = '';
                emailObj[0].focus();
                return false;
            }

            $.ajax({
                url: "authuser/AJAX/registernewcase.php",
                type: "POST",
                data: {
                    'caseComplex': $('#court_complex_code').val(),
                    'caseType': $('#case_type').val(),
                    'caseNumber': $('#search_case_no').val(),
                    'caseYear': $('#case_rgyear').val(),
                    'caseAppelant': $('#case_appelant').val(),
                    'caseOpponent': $('#case_opponent').val(),
                    'clientEmail': $('#clientEmail').val()
                },
                success: function(results) {
                    if (results == 1) {
                        bootbox.alert({
                            size: "small",
                            title: "Sorry",
                            message: "This case already exits. Please try again with different case number",
                        });
                    } else {
                        bootbox.alert({
                            size: "small",
                            title: "Success",
                            message: "A new case " + results + " has been successfully added.",
                        });
                        $("#addNewCaseModal").modal('hide');
                    }
                }
            });
            document.getElementById('registerNewCaseForm').reset();
        });

        //check integer
        function checkInteger(str) {
            var len = str.length;
            for (var i = 0; i < len; i++) {
                if (str.charAt(i) == ".") {
                    return false;
                }
            }
        }
        //Send Email
        $('#sendEmailButton').click(function() {
            //console.log($('#newclientName').val());
            var emailID = $('#emailID').val();
            var caseStatus = $('#emailCaseStatus').val();
            var regEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(emailID == "no data" | emailID == "" | !regEmail.test(emailID)) {
                document.getElementById("emailID").disabled = false;
                document.getElementById('emailID').value = "";
                document.getElementById('emailID').focus();
                bootbox.alert("Please, mention correct email");
                return false;
            }
            var reg = new RegExp("^([a-zA-Z]{2,}(\\s[a-zA-z]{1,})?'?-?([a-zA-Z]{2,}\\s)?([a-zA-Z]{1,})?)$");
            if(caseStatus == "" | !caseStatus.match(reg) | caseStatus == "Newly Added"){
                bootbox.alert("Please, mention email body");
                document.getElementById('emailCaseStatus').focus();
                return false;
            }
            $.ajax({
                url: "authuser/AJAX/sendmailtoclient.php",
                type: "POST",
                data: {
                    'caseID': $('#emailCaseIDSelector').val(),
                    'clientEmail': $('#emailID').val(),
                    'caseStatus': $('#emailCaseStatus').val(),
                    'nextDate': $('#emailNextDate').val(),
                    'clientName': $('#emailClientName').val(),
                    'clientId': $('#emailClientIDSelector').val()
                },
                success: function(results) {
                    //console.log(results);
                    if (results == 1) {
                        //console.log(changeReminder);
                        data['data'][changeReminder]['REMINDER_STATUS'] = 0;
                        bootbox.alert({
                            size: "small",
                            title: "Success",
                            message: "Email sent..!",
                        });
                        $("#emailClientModal").modal('hide');
                    } else {
                        data['data'][changeReminder]['REMINDER_STATUS'] = 1;
                        bootbox.alert({
                            size: "small",
                            title: "Sorry",
                            message: "Something Went wrong, Please try again later !",
                        });
                        $("#emailClientModal").modal('hide');
                    }
                }
            });
        });

        //Logout
        function logoutAccount() {
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function() {});
            //auth2.disconnect();
            window.open("logout.php", '_self');
        }

        //Populate Email Modal
        var changeReminder;

        function populateEmailDetails(Email, CaseNo, caseID) {
            //Because of looping in query statement
            document.getElementById("emailID").disabled = true;
            //console.log(caseID);
            if (data['data'][caseID]['REMINDER_STATUS']) {
                //console.log(data['data'][caseID]['REMINDER_STATUS']);
                changeReminder = caseID;
                $('#emailCaseIDSelector').val(data['data'][caseID]['CASE_ID']);
                $('#emailClientIDSelector').val(data['data'][caseID]['CLIENT_ID']);
                $('#emailCaseNo').val(data['data'][caseID]['FULL_ID']);
                $('#emailClientName').val(data['data'][caseID]['CASE_DEFENDANT']);
                $('#emailID').val(data['data'][caseID]['CLIENT_EMAIL']);
                $('#emailCaseStatus').val(data['data'][caseID]['CASE_SUMMARY']);
                $('#emailNextDate').val(data['data'][caseID]['NEXT_DATE']);
                $("#emailClientModal").modal('show');
            } else {
                $("#emailClientModal").modal('hide');
                bootbox.alert({
                    size: "small",
                    title: "Ohhh Snap !",
                    message: "Reminder already sent to client. Thank you.",
                });
            }
        }

        //function populateUserDataModal
        function populateUserDataModal() {
            $.ajax({
                url: "authuser/AJAX/getuserdata.php",
                type: "POST",
                success: function(results) {
                    result = JSON.parse(results);
                    var barCode = result[0]['BAR_CODE'].split("-");
                    if (result[0]['GIVE_UPDATE_STATUS']) {
                        $('#user_bar_code_1').val(barCode[0]);
                        $('#user_bar_code_2').val(barCode[1]);
                        $('#user_bar_code_3').val(barCode[2]);
                        $('#regUserName').val(result[0]['REG_NAME']);
                        $('#userLastUpdate').val(result[0]['LAST_UPDATED_ON']);
                        $('#user_court_complex_code').val(result[0]['COURT_COMPLEX']);
                    } else {
                        bootbox.alert({
                            size: "small",
                            title: "Sorry",
                            message: "Please Contact Admin !",
                        });
                        $("#updateUserDataModal").modal('hide');
                    }
                }
            });
        }
        var box = bootbox.alert({
            size: "small",
            title: "Loading....",
            message: "<img src='demo_wait.gif' style='' width='250' height='250'/><br> Please wait...while we complete the work for you !",
        });
        box.modal('hide');
        //Update User data
        $('#updateUserData').click(function() {
            //validate Fields
            var captchaObj = document.getElementsByName('regUserName');
            if (captchaObj[0].value == "") {
                bootbox.alert("Please enter Registered name");
                captchaObj[0].focus();
                return false;
            }
            var captchaVal = captchaObj[0].value;
            var reg = new RegExp("^([a-zA-Z\s]{2,}(\\s[a-zA-z]{1,})?'?-?([a-zA-Z]{2,}\\s)?([a-zA-Z]{1,})?)$");
            /*if (!captchaVal.match(reg)) {
                bootbox.alert("Enter only alphabetic characters in registered name");
                captchaObj[0].value = '';
                captchaObj[0].focus();
                return false;
            }*/
            $("#updateUserDataModal").modal('hide');
            box.modal('show');
            $.ajax({
                url: "authuser/AJAX/updateuserdata.php",
                type: "POST",
                data: {
                    'barCode': $('#user_bar_code_1').val()+"-"+$('#user_bar_code_2').val()+"-"+$('#user_bar_code_3').val(),
                    'regName': $('#regUserName').val(),
                    'courtComplex': $('#user_court_complex_code').val()
                },
                success: function(results) {
                    box.modal('hide');
                    console.log(results);
                    if (results == 1) {
                        bootbox.alert({
                            size: "small",
                            title: "Success",
                            message: "User data Updated"
                        });
                    } else {
                        bootbox.alert({
                            size: "small",
                            title: "Sorry",
                            message: "An error occured, Please try again later !"
                        });
                    }

                }
            });
        });
       
        document.getElementById("updateCaseProceeding").disabled = <?php echo $_SESSION['caseUpdate']?>;
        //Update case proceedings
        $('#updateCaseProceeding').click(function() {
            bootbox.confirm("Do you want to update the case proceedings ?", function(result) {
                if(result){
                    $.ajax({
                    url: "authuser/AJAX/updatecaseproceedings.php",
                    type: "POST",
                    success: function(results) {
                            document.getElementById("updateCaseProceeding").disabled = true;
                            console.log(results);
                            bootbox.alert({
                            size: "small",
                            title: "Just a moment !",
                            message: "Case proceedings will be updated !"
                            });
                        }
                    });
                }
                else{
                    //console.log("check");
                }
            });
        });
    </script>
</body>

</html>