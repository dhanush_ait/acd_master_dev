<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
    <title>ACDWeb | Update</title>

    <meta name="google-signin-client_id" content="709502795016-qbft7m0mc6m6ekkdu5esdefsvlsbj0s5.apps.googleusercontent.com">
    
    <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css">
    <link rel="stylesheet" href="css/defaults.css" type="text/css">
    <link rel="stylesheet" href="css/bootstrap-datetimepicker.css" type="text/css">
    <link rel="stylesheet" href="css/autocomplete.css" type="text/css">

    <script type="application/javascript" src="js/jquery.min.js"></script>
    <script type="application/javascript" src="js/jquery-ui.min.js"></script>
    <script type="application/javascript" src="js/bootstrap.min.js"></script>
    <script type="application/javascript" src="js/validator.min.js"></script>
    <script type="application/javascript" src="js/jquery.lazy.min.js"></script>
    <script type="application/javascript" src="js/bootbox.min.js"></script>
    <script type="application/javascript" src="js/moment.js"></script>
    <script type="application/javascript" src="js/docked-link.min.js"></script>
    <script type="application/javascript" src="js/datatables/datatables.js"></script>
    <script type="text/javascript" src="js/touchspin.min.js"></script>
    <script type="application/javascript" src="js/caseTypeLoader.js"></script>

    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script type="application/javascript">
        $(document).ready(function() {
            $('#updateCaseListTable').DataTable({
                "ajax": {
                    "url": "authuser/AJAX/fetchallcaselist.php",
                    "type": "POST"
                },
                columns: [{
                        "data": "FULL_ID"
                    },
                    {
                        "data": "CASE_DEFENDANT"
                    },
                    {
                        "data": "CASE_OPPONENT"
                    },
                    {
                        "data": "CASE_SUMMARY"
                    },
                    {
                        "data": "NEXT_DATE"
                    },
                    {
                        "data": "BUTTON"
                    }
                ],
                responsive: true,
                select: true,
                columnDefs: [{
                        responsivePriority: 1,
                        targets: 0
                    },
                    {
                        responsivePriority: 2,
                        targets: -1
                    },
                    {
                        responsivePriority: 2,
                        targets: -2
                    }
                ]
            });
            loadData();
        });

        function loadData(){
            $.ajax({
                url: "authuser/AJAX/fetchallcaselist.php",
                type: "POST",
                success: function(results) {
                    window.data = JSON.parse(results);
                    //console.log(data);
                }
            });
        }

        function populateCaseDetails(caseID) {
            $('#caseIDSelector').val(data['data'][caseID]['CASE_ID']);
            $('#case_no').val(data['data'][caseID]['FULL_ID']);
            $('#case_appelant').val(data['data'][caseID]['CASE_DEFENDANT']);
            $('#case_opponent').val(data['data'][caseID]['CASE_OPPONENT']);
            $('#case_status').val(data['data'][caseID]['CASE_SUMMARY']);
            $('#next_date').val(data['data'][caseID]['NEXT_DATE']);
        }

        //Populate delete modal
        function populateDeleteCaseDetails(caseID) {
            //console.log(data['data'][caseID]['CASE_ID']);
            $('#deleteCaseIDSelector').val(data['data'][caseID]['CASE_ID']);
            $('#deletecase_no').val(data['data'][caseID]['FULL_ID']);
            $('#deletecase_appelant').val(data['data'][caseID]['CASE_DEFENDANT']);
            $('#deletecase_opponent').val(data['data'][caseID]['CASE_OPPONENT']);
            $('#deletecase_status').val(data['data'][caseID]['CASE_SUMMARY']);
        }
    </script>

</head>

<body>

    <?php
    session_start();
    if (!isset($_SESSION['emailID']) || $_SESSION['userActive'] != 1) {
        echo "<script type=\"text/javascript\">
                bootbox.alert({
                size: \"large\",
                title: \"Sorry\",
                message: \"There was an error while processing your request. You may try logging-in again.\",
                callback: function() {
                    window.location = \"index.php\";
                }
                })
                </script>";
        echo "1";
        exit();
    }
    ?>

    <!-- Navbar -->

    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="../index.php" class="navbar-brand">
                    <img src="images/advlogo.jpg" width="28" height="24" />
                </a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav">
                    <li><a href="welcome.php">HOME</a></li>
                    <li><a href="updatecaselist.php">SEARCH</a></li>
                    <li><a href="updateclient.php">CLIENTS</a></li>
                    <li><a href="cupdate.php">NOTIFICATIONS</a></li>
                </ul>
                <form class="navbar-form navbar-right" id="recruiterLogout" name="recruiterLogout" method="" action="">
                    <div class="form-group">
                        <input type="button" class="btn btn-sm btn-danger pull-righ;t" onclick="logoutAccount()" name="logoutButtonLargeScreen" id="logoutButtonLargeScreen" value="Logout" />
                        <div class="g-signin2 btn" data-onsuccess="onSignIn" style="display:none"></div>
                        <input type="button" class="btn btn-sm btn-danger center-block" onclick="logoutAccount()" name="logoutButtonSmallScreen" id="logoutButtonSmallScreen" value="Logout" />
                    </div>
                </form>
            </div>
        </div>
    </nav>


    <div class="container" style="">
        <div class="row" id="updateCaseListRow" style="">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="text-align: center">
                            <strong>
                                <p class="panel-title" id="datePanel"></p>Case List
                            </strong>
                        </div>
                        <div id="" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row" id="updateCaseListPanel">
                                    <div class='col-md-12 col-xs-12 col-lg-12'>
                                        <div class='form-group'>
                                            <table class='table table-bordered table-hover table-responsive table-striped' id='updateCaseListTable'>
                                                <thead>
                                                    <tr style='height: 20px; background-color: lightseagreen; vertical-align: middle'>
                                                        <th style='vertical-align: middle; width: 1%;'>Case No.</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Defendant</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Opponent</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Status</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Next Hearing</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="updateCaseListTableBody">
                                                </tbody>
                                                <tfoot>
                                                    <tr style='height: 20px; background-color: lightseagreen; vertical-align: middle'>
                                                        <th style='vertical-align: middle; width: 1%;'>Case No.</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Defendant</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Opponent</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Status</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Next Hearing</th>
                                                        <th style='vertical-align: middle; width: 1%;'>Action</th>
                                                    </tr>
                                                </tfoot>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="editCaseDetailsModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Case</h4>
                </div>
                <div class="modal-body" style="max-height: 60vh; overflow: auto;">
                    <form name="updateCaseDetailsForm" id="updateCaseDetailsForm">
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="caseNumber">Case Number *</label>
                            <input type="text" maxlength="7" size="28" name="case_no" class="form-control" id="case_no" title="Enter Maximum 7 digit of Case Number Compulsory Field" autocomplete="off" disabled="disabled">
                            <input type="hidden" name="caseIDSelector" id="caseIDSelector" value="">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="Appelant">Appelant *</label>
                            <input type="text" class="form-control" name="case_appelant" id="case_appelant" value="" title="Please enter valid Name" autocomplete="off">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="caseOpponent">Opponent *</label>
                            <input type="text" class="form-control" name="case_opponent" id="case_opponent" value="" title="Please enter valid Name" autocomplete="off">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="nectDate">Next Date *</label>
                            <input type="date" class="form-control" name="next_date" id="next_date" value="" title="Please enter valid Name" autocomplete="off">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="caseSummary">Summary *</label>
                            <input type="text" class="form-control" name="case_status" id="case_status" value="" title="Please enter valid Name" autocomplete="off">
                            <div class="help-block with-errors"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="updateCaseDetails" class="btn btn-success pull-left">Update</button>
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="deleteCaseDetailsModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Are you sure to delete ?</h4>
                </div>
                <div class="modal-body" style="max-height: 60vh; overflow: auto;">
                    <form name="deleteCaseDetailsForm" id="deleteCaseDetailsForm">
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="caseNumber">Case Number</label>
                            <input type="text" maxlength="7" size="28" name="deletecase_no" class="form-control" id="deletecase_no" title="Enter Maximum 7 digit of Case Number Compulsory Field" autocomplete="off" disabled="disabled">
                            <input type="hidden" name="deleteCaseIDSelector" id="deleteCaseIDSelector" value="">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="Appelant">Appelant</label>
                            <input type="text" class="form-control" name="deletecase_appelant" id="deletecase_appelant" value="" title="Please enter valid Name" autocomplete="off" disabled="disabled">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="caseOpponent">Opponent</label>
                            <input type="text" class="form-control" name="deletecase_opponent" id="deletecase_opponent" value="" title="Please enter valid Name" autocomplete="off" disabled="disabled">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-lg-6">
                            <label class="control-label" for="caseSummary">Status</label>
                            <input type="text" class="form-control" name="deletecase_status" id="deletecase_status" value="" title="Please enter valid Name" autocomplete="off" disabled="disabled">
                            <div class="help-block with-errors"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="deleteCaseDetails" class="btn btn-danger pull-left">Confirm</button>
                    <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script type="application/javascript">
        $('#updateCaseDetails').click(function() {
            var captchaObj = document.getElementsByName('case_appelant');
            if (captchaObj[0].value == "") {
                alert("Please enter appelant name");
                captchaObj[0].focus();
                return false;
            }
            var captchaVal = captchaObj[0].value;
            var reg = new RegExp("^([a-zA-Z]{2,}(\\s[a-zA-z]{1,})?'?-?([a-zA-Z]{2,}\\s)?([a-zA-Z]{1,})?)$");
            if (!captchaVal.match(reg)) {
                alert("Enter only alphabetic characters in appelant name");
                captchaObj[0].value = '';
                captchaObj[0].focus();
                return false;
            }

            var oppObj = document.getElementsByName('case_opponent');
            if (oppObj[0].value == "") {
                alert("Please enter opponent name");
                oppObj[0].focus();
                return false;
            }
            var oppVal = oppObj[0].value;
            var reg = new RegExp("^([a-zA-Z]{2,}(\\s[a-zA-z]{1,})?'?-?([a-zA-Z]{2,}\\s)?([a-zA-Z]{1,})?)$");
            var oppVal = oppObj[0].value;
            if (!oppVal.match(reg)) {
                alert("Enter only alphabetic characters in oponent name");
                oppObj[0].value = '';
                oppObj[0].focus();
                return false;
            }

            var next_date = $('#next_date').val();
            var today = new Date();
            var dateString = moment(today).format('YYYY-MM-DD');
            if (dateString < next_date) {
                $.ajax({
                    url: "authuser/AJAX/updatecasedetails.php",
                    type: "POST",
                    data: {
                        'caseID': $('#caseIDSelector').val(),
                        'caseAppelant': $('#case_appelant').val(),
                        'caseOpponent': $('#case_opponent').val(),
                        'caseNextDate': $('#next_date').val(),
                        'caseSummary': $('#case_status').val()
                    },
                    success: function(results) {
                        bootbox.alert({
                            size: "small",
                            title: "Success",
                            message: "A case " + results + " has been successfully updated.",
                        });
                        var table = $('#updateCaseListTable').DataTable();
                        table.ajax.reload();
                        loadData();
                        $("#editCaseDetailsModal").modal('hide');
                    }
                });
            } else {
                bootbox.alert({
                    size: "small",
                    title: "Sorry",
                    message: "Please mention the proper next date !",
                });
                var oppnextDateObj = document.getElementsByName('next_date');
                oppnextDateObj[0].focus();
                return false;
            }
        });

        //Delete case
        $('#deleteCaseDetails').click(function() {
            $.ajax({
                url: "authuser/AJAX/deletecasedetails.php",
                type: "POST",
                data: {
                    'caseID': $('#deleteCaseIDSelector').val()
                },
                success: function(results) {
                    bootbox.alert({
                        size: "small",
                        title: "Success",
                        message: "A case " + results + " has been successfully deleted.",
                    });
                    var table = $('#updateCaseListTable').DataTable();
                    table.ajax.reload();
                    loadData();
                    $("#deleteCaseDetailsModal").modal('hide');
                }
            });
        });

        function logoutAccount() {
            window.open("logout.php", '_self');
        }
    </script>

</body>

</html>