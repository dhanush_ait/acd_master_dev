<?php
// Initialize the session
session_start();
 
// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  header("location: login.php");
  exit;
}
?>
<?php

// Include config file
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_NAME','case dairy');
 
/* Attempt to connect to MySQL database */
$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
//$db_found = mysql_select_db($database, $db_handle);
// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
} 
// Define variables and initialize with empty values
$C_id = $c_address=$c_state =$c_gender=$c_city =$c_contact=$c_email= "";
//$username_err = $password_err = "";
 
// Processing form data when form is submitted

if($_SERVER["REQUEST_METHOD"] == "POST"){
                $C_id =  $_POST['C_id'] ;
                //$cname = $_POST['cname']; 
                //$caid = $_POST['caid'];
                //echo "$caid " ;
                $qry="SELECT * FROM case_entry WHERE C_id = '$$C_id'";
                $result = mysqli_query($link,$qry);
                $num_rows = mysqli_num_rows($result);
                $c_address = $_POST['c_address'] ;
                $c_city = $_POST['c_city'] ;
                $c_state = $_POST['c_state'];
                $c_gender = $_POST['c_gender'] ;
                $c_contact = $_POST['c_contact'] ;
                $c_email = $_POST['c_email'];
                if($num_rows > 0){
                //echo "<h3>Found and Updated Client id $C_id </h3>" ;
                    $qry1 = "UPDATE client SET C_address = '$c_address' C_city='$c_city' C_state='$c_state' C_gender='$c_gender' C_contact='$c_contact' C_email='$c_email' where C_id = '$C_id'";
                    if(mysqli_query($link,$qry1))
                        echo "Success" ;
                }
                else{
                    echo "<h3>Client id $C_id not Registered with a case. Try Again !";
                }
                
                
                //echo "$c_email" ;
                //$sql = "INSERT INTO  client(C_id,C_name,C_Address,City,C_state,C_gender,C_contact,C_email) VALUES ('$cid','$cname','$c_address','$c_city','$c_state','$c_gender',$c_contact,'$c_email')";
                //if(mysqli_query($link,$sql))
                  //  echo "<h4> Client Added for client ID :$cid and name : $cname is Successful. </h4>" ;
                mysqli_close($link);
    }
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Client</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<div class="wrapper" style = "color:green;">
        <h3><a href = "welcome.php">Back to Home </a> </h3>
        <br />
        <h3><a href = "clist.php">Search for client details</a></h3>
        <br /><p>Please fill for Client details.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <!--div class="form-group">
                <label>Case ID *</label>
                <input type="text" name="caid" placeholder="caid" class="form-control" value=""" required>
            </div-->
            <div class="form-group">
             <label>Client ID</label>
                <input type="text" name="C_id" placeholder="C_id" class="form-control" value="" required>
            </div>    
            <!--div class="form-group">
                <label>Client Name</label>
                <input type="text" name="cname" placeholder="cname" class="form-control" value="">
             </div-->  
            <div class="form-group">
                <label>Adress</label>
                <input type="text" name="c_address" placeholder="address" class="form-control" value="" required>
            </div>     
             <div class="form-group">
                <label>City</label>
                <input type="text" name="c_city" placeholder="City" class="form-control" value="" required>
             </div>    
             <div class="form-group">
                <label>State</label>
                <input type="text" name="c_state" placeholder="State" class="form-control" value="">
             </div>
             <div class="form-group">
                <label>Gender</label>
                <input type="radio" name="c_gender" value="male" checked> Male<br>
                <input type="radio" name="c_gender" value="female"> Female
             </div>
             <div class="form-group">
                <label>Contact</label>
                <input type="text"  name="c_contact" placeholder="phone number" class="form-control" value="">
             </div>
            <div class="form-group">
                <label>Email </label>
                <input type="email" name="c_email" placeholder="email" class="form-control" value="">
             </div>
             <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Add Client">
                <input type="reset" class="btn btn-primary" value="Reset">          
            </div>
           </form>
    </div>    
</body>
</html>