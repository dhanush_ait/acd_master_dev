<?php
require_once 'config.php';
// Initialize the session
session_start();
 
// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  header("location: login.php");
  exit;
}
?>
<?php 
require_once 'config.php';
// Define variables and initialize with empty values
$cid = $cname = $caid=$catype=$court_name=$dor=$opp_cause= "" ;
//$username_err = $password_err = "";
/*function crep($link,$cid)
{
    $qry = "SELECT * from client where C_id = '$cid'";
    if(mysqli_query($link,$qry))
    {
            echo "$cid is already a client." ;
            return true ;
    }
    return false ;
} */


function cupdate($link,$cid,$cname)
 {
    //if(!crep($link,$cid))
    //{
       $sql = "INSERT INTO client(C_id,C_name) values ('$cid','$cname')";
       mysqli_query($link,$sql) ;
    //} 
}
 function cpupdate($link,$caid,$dor,$cname)
 {
    $sql1 =  "INSERT INTO case_proceeding(Ca_id,ca_year,C_name) VALUES ($caid,'$dor','$cname')";   
    mysqli_query($link,$sql1) ;
 }
 function fupdate($link,$cid,$cname)
{
    $sql1 = "INSERT INTO fee(C_id,C_name) VALUES ('$cid','$cname')";
    mysqli_query($link,$sql1) ;
}
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    $caid = $_POST['caid'];
    $dor = $_POST['dor'] ;
    $cid =  $_POST['cid'] ;
    $cname = $_POST['cname']; 
    $catype = $_POST['catype'] ;
    $court_name = $_POST['court_name'] ;
    $opp_cause = $_POST['opp_cause'];
    $sql = "INSERT INTO case_entry(C_id,C_name,Ca_id,Ca_type,Court_name,Opp_cause,ca_year) VALUES ('$cid','$cname',$caid,'$catype','$court_name','$opp_cause','$dor')";
    if(!mysqli_query($link,$sql))
    {
        echo("<h3> ERROR: Please Enter valid details.</h3>" . mysqli_connect_error());              
    }
    else
        {    
            cupdate($link,$cid,$cname);
            cpupdate($link,$caid,$dor,$cname);
            fupdate($link,$cid,$cname);
            echo " <h3>CASE ENTRY successfull for Case ID : $caid </h3>" ;        
        }   
            header("Refresh:5");
    mysqli_close($link);
}
?>
 
<DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add case</title> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
    <script src="js\caseentryvalid.js"></script>
</head>
<body onload="document.newcase.caid.focus();">
<div class="wrapper" style = "color:green;">
        <h3><a href = "welcome.php">Back to Home </a></h3>
        <br /><p>Please fill in your credentials to case entry.</p>
        <form name='newcase' action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" onSubmit=formvalidation();>
            <div class="form-group">
                <label>Case Number *</label>
                <input type="number" min="1" ma="5000" name="caid" placeholder="caid" class="form-control">
            </div>
            <div class="form-group">
                <label>Year *</label>
                <input type="number" name="dor" min="1990" max="2018" required>
              </div>
            <div class="form-group">
                <label>Client ID *</label>
                <input type="text" name="cid" placeholder="cid" class="form-control" required>
            
            </div>    
            
            <div class="form-group">
                <label>Case Type *</label>
                <select name="catype" size="1" required>
                <option value="OS">OS</option>
                <option value="CR">CR</option>
                 <option value="RRT">RRT</option>
                <option value="CC">CC</option>
            </select>
            </div>   
            <div class="form-group">
                <label>Client Name</label>
                <input type="text" name="cname" placeholder="cname" class="form-control">
             </div>    
             <div class="form-group">
                <label>Court Name *</label>
                <select name="court_name" size="1">
  <option value="CIVIL">CIVIL</option>
  <option value="JMFC">JMFC</option>
  <option value="SRDIV">SRDIV</option>
  <option value="JRDIV">JRDIV</option>
</select>
             </div>    
             <div class="form-group">
                <label>Opposite Cause</label>
                <input type="text" name="opp_cause" placeholder="opposite cause" class="form-control">
             </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Addcase">
                <input type="reset" class="btn btn-primary" value="Reset">          
            </div>
        </form>
    </div>    
</body>
</html>

<select name="court_complex_code" id="court_complex_code" style="width: 250px;height:21px;" onchange="funCourtEstChange();">
<option value="0">Select Court Complex</option><option value="1@12,19">Court Complex- C.N.Halli</option><option value="2@5,6">Court Complex- Gubbi</option><option value="3@4">Court Complex- Koratagere</option><option value="4@10,11">Court Complex- Kunigal</option><option value="5@23,24,25,26">Court Complex- Madhugiri</option><option value="6@9,18">Court Complex- Pavagada</option><option value="7@7,8">Court Complex- Sira</option><option value="9@15,14">Court Complex- Turuvekere</option><option value="101@1,2,3,13">District Court Complex-Tumakuru</option><option value="102@20,21,22">Court Complex Tiptur</option></select>

<select name="case_type" id="case_type" onchange="">
<option value="object(PDO)"></option>
<option value="625 (0) {}0">Select Case Type</option><option value="3">AA - Arbitration Application</option><option value="1">A.C. - Arbitration Cases</option><option value="5">Appl.10(1)(c) - Application u/s 10(1)(c)</option><option value="6">Appl.10(4)(A) - Application u/s 10(4)(A)</option><option value="7">Appl.33(2)b - Application u/s 33(2)b</option><option value="8">APPL.33(C)(2)</option><option value="10">APPLN - Application for Wakf Board</option><option value="9">Appl.u/s 11 - Application u/s 11</option><option value="2">A.S. - Arbitration Suits</option><option value="11">C.C. - CRIMINAL CASES</option><option value="12">C.O.A. - Company Applications</option><option value="13">C.O.P. - Company Petitions</option><option value="15">Cr - Crime Case</option><option value="16">CRL.A - CRIMINAL APPEAL</option><option value="17">CRL.M.A. - CRIMINAL MISC.APPEAL</option><option value="18">Crl.Misc. - CRIMINAL MISC.CASES</option><option value="19">Crl.Misc.(DVA) - CRL MISC(Domestic Voilence)</option><option value="20">CRL.R.P. - CRIMINAL REVISION PETITIONS</option><option value="21">E.A.T. - Education Appellate Tribunal C</option><option value="67">E.C.A. - Employees Compensation Act</option><option value="22">ELEC.C - ELECTION PETITIONS</option><option value="65">Ele.Misc. - 0/12</option><option value="66">Ele.Petn. - Election Petition</option><option value="23">EX - Execution Petition Under Order</option><option value="24">Ex.A. - Execution Appeals</option><option value="25">Ex.C - Execution Cases</option><option value="26">FDP - Petitioner For Final Decree pr</option><option value="28">G and W.C. - Guardian and Wards Cases</option><option value="27">G  and WC CASE - Appointment Of Guardian, Other</option><option value="29">H.R.C. - House Rent Control Cases</option><option value="30">H.R.C.A. - House Rent Control Appeals</option><option value="31">I.C. - Insolvency Cases</option><option value="34">IDact-S10 - Under Sec 10 of ID Act (Ref.Ca</option><option value="64">IDact-S10(1)(C) - Industrial Disputes u/s 10(1)(</option><option value="35">IDact-S10(1)(d) - Industrial Disputes u/s 10(1)(</option><option value="32">IDact-S.33 - Serial Applications Under Sec.</option><option value="36">IDact-S33(2)(b) - Approval Application u/s 33(2)</option><option value="37">IDact-S33(A) - Under Sec 33(A) of ID Act (Com</option><option value="33">IDact-S.A - Complaints under Sec.A of the</option><option value="38">IID.1947,Sec.32 (A).</option><option value="39">Ind Emp-SO Act - Appeal under Industrial Employ</option><option value="40">J.C. - JUVENILE CASES</option><option value="41">KID 10-4(A)</option><option value="42">L.A.C. - Land Acquisition Cases</option><option value="44">LAC(APPL) - L.A.C.APPEAL</option><option value="45">M.A. - Miscellanuous Appeals</option><option value="48">MA(EAT) - Appeal Under Education Act</option><option value="46">M.C. - MATRIMONIAL CASES</option><option value="49">Misc - Miscellaneous Cases</option><option value="50">Misc.Appln. - Miscelleneous Application</option><option value="47">M.V.C. - Accident Claim Cases u/r M.V.</option><option value="51">O.L. - OTHER LAW CASES</option><option value="52">O.S. - Original Suit</option><option value="54">P and Sc - Probate and Succession Cases</option><option value="55">P.C.R. - PRIVATE COMPLAINTS</option><option value="56">P.MIS. - Petition Filed Indegent Person</option><option value="53">P  SC - Petition For Succession Certif</option><option value="57">R.A. - Regular Appeals</option><option value="58">R.C.(E)</option><option value="59">R.E.V. - Revision Petitions</option><option value="68">REVIEW  PETITION</option><option value="60">R.E.V. (RENT) - Revision Petition Under Rent C</option><option value="62">SC - SESSION CASES</option><option value="61">S.C. - Small Cause Suit</option><option value="63">SPL.C - SPECIAL CASES</option></select>