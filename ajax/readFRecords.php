<?php
// Initialize the session
session_start();
 
// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  header("location: /project/login.php");
  exit;
}
?>
<?php
	// include Database connection file 
    include("config.php");
    
	// Design initial table header 
	$data = '<table class="table table-bordered table-striped">
						<tr>
                        <th>Index No.</th>
                            <th>Client ID</th>
							<th>Client Name</th>
							<th>Reciept ID</th>
							<th>Total Amount</th>
                            <th>Amount Paid</th>
                            <th>Balance</th>
                            <th>Pay Mode</th>
                            <th>Update</th>
							
						</tr>';

	$query = "SELECT * FROM fee";

	if (!$result = mysqli_query($link, $query)) {
        exit(mysqli_error($con));
    }
    // if query results contains rows then featch those rows 
    if(mysqli_num_rows($result) > 0)
    {
    	$number = 1;
    	while($row = mysqli_fetch_assoc($result))
    	{
    		$data .= '<tr>
				<td>'.$number.'</td>
				<td>'.$row['C_id'].'</td>
                <td>'.$row['C_name'].'</td>
                <td>'.$row['Reciept_id'].'</td>
				<td>'.$row['T_amt'].'</td>
                <td>'.$row['amt_paid'].'</td>
                <td>'.$row['balance'].'</td>
                <td>'.$row['pay_mode'].'</td>
                <td>
					<button onclick="GetFeeDetails(\''.$row['C_id'].'\')" class="btn btn-warning">Update</button>
				</td>
				
    		</tr>';
    		$number++;
    	}
    }
    else
    {
    	// records now found 
    	$data .= '<tr><td colspan="6">Records not found!</td></tr>';
    }

    $data .= '</table>';

    echo $data;
?>