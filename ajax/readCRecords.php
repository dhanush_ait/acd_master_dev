<?php
// Initialize the session
session_start();
 
// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  header("location: /project/login.php");
  exit;
}
?>
<?php
	// include Database connection file 
    include("config.php");
    
	// Design initial table header 
	$data = '<table class="table table-bordered table-striped">
						<tr>
                        <th>Index No.</th>
                            <th>Client ID</th>
							<th>Client Name</th>
							<th>Address</th>
							<th>City</th>
                            <th>State</th>
                            <th>Gender</th>
                            <th>Contact</th>
                            <th>Email</th>
                            <th>Update</th>
						
						</tr>';

	$query = "SELECT * FROM client";

	if (!$result = mysqli_query($link, $query)) {
        exit(mysqli_error($con));
    }
    // if query results contains rows then featch those rows 
    if(mysqli_num_rows($result) > 0)
    {
    	$number = 1;
    	while($row = mysqli_fetch_assoc($result))
    	{
    		$data .= '<tr>
				<td>'.$number.'</td>
				<td>'.$row['C_id'].'</td>
                <td>'.$row['C_name'].'</td>
                <td>'.$row['C_Address'].'</td>
				<td>'.$row['City'].'</td>
                <td>'.$row['C_state'].'</td>
                <td>'.$row['C_gender'].'</td>
                <td>'.$row['C_contact'].'</td>
                <td>'.$row['C_email'].'</td>
                <td>
					<button onclick="GetClientDetails(\''.$row['C_id'].'\')" class="btn btn-warning">Update</button>
				</td>
				
    		</tr>';
    		$number++;
    	}
    }
    else
    {
    	// records now found 
    	$data .= '<tr><td colspan="6">Records not found!</td></tr>';
    }

    $data .= '</table>';

    echo $data;
?>