<?php
// Initialize the session
session_start();
 
// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  header("location: /project/login.php");
  exit;
}
?>
<?php
	// include Database connection file 
    include("config.php");
    
	// Design initial table header 
	$data = '<table class="table table-bordered table-striped">
						<tr>
                        <th>Index No.</th>
                            <th>Case Number</th>
							<th>Procc Summary</th>
							<th>Procc Date</th>
							<th>Next Hearing Date</th>
                            <th>Client Name </th>
                            <th>Expected Disposal</th>
                            <th>Result</th>
                            <th>Update</th>
							<th>Delete</th>
						</tr>';

	$query = "SELECT * FROM case_proceeding";

	if (!$result = mysqli_query($link, $query)) {
        exit(mysqli_error($con));
    }
    // if query results contains rows then featch those rows 
    if(mysqli_num_rows($result) > 0)
    {
    	$number = 1;
    	while($row = mysqli_fetch_assoc($result))
    	{
    		$data .= '<tr>
				<td>'.$number.'</td>
				<td>'.$row['Ca_id'].'</td>
                <td>'.$row['Proc_Summ'].'</td>
                <td>'.$row['Proc_date'].'</td>
				<td>'.$row['N_hearing_date'].'</td>
                <td>'.$row['C_name'].'</td>
                <td>'.$row['Disposal'].'</td>
                <td>'.$row['Result'].'</td>
                <td>
					<button onclick="GetUserDetails('.$row['Ca_id'].')" class="btn btn-warning">Update</button>
				</td>
				<td>
					<button onclick="DeleteUser('.$row['Ca_id'].')" class="btn btn-danger">Delete</button>
				</td>
    		</tr>';
    		$number++;
    	}
    }
    else
    {
    	// records now found 
    	$data .= '<tr><td colspan="6">Records not found!</td></tr>';
    }

    $data .= '</table>';

    echo $data;
?>