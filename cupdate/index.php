<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title> Update Case List</title>

<!-- Bootstrap CSS File -->
<link rel="stylesheet" type="text/css" href="bootstrap-3.3.5-dist/css/bootstrap.css" />
</head>
<body>

<!-- Content Section -->
<!-- Content Section -->
<div class="container">
<div class="row">
<div class="col-md-12">
<h2>Update Case List</h2>
<div class="pull-right">
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<h4>Records:</h4>
<div class="records_content"></div>
</div>
</div>
</div>

<!-- Modal - Update User details -->
<div class="modal fade" id="update_user_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Update</h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <label for="update_Proc_Summ">Procc Summary</label>
                    <input type="text" id="Proc_Summ" placeholder="Procc Summary" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="update_Proc_date">Procc Date</label>
                    <input type="text" id="Proc_date" placeholder="Procc Date" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="update_N_hearing_date">Next Hearing Date</label>
                    <input type="text" id="N_hearing_date" placeholder="Next Hearing date" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="update_Oppcause">Opp Cause</label>
                    <input type="text" id="Oppcause" placeholder="Oppcause" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="update_Disposal">Disposal</label>
                    <input type="text" id="Disposal" placeholder="Disposal" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="update_Result">Result</label>
                    <input type="text" id="Result" placeholder="Result" class="form-control"/>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="UpdateUserDetails()" >Save Changes</button>
                <input type="hidden" id="hidden_user_id">
            </div>
        </div>
    </div>
</div>
<!-- // Modal -->


<!-- /Content Section -->
<!-- /Content Section -->

<!-- Jquery JS file -->
<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>

<!-- Bootstrap JS file -->
<script type="text/javascript" src="bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>

<!-- Custom JS file -->
<script type="text/javascript" src="js/script.js"></script>
</body>
</html>