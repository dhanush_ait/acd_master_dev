<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>ACDWeb | Login</title>
    <meta name="google-signin-client_id" content="709502795016-qbft7m0mc6m6ekkdu5esdefsvlsbj0s5.apps.googleusercontent.com">
    <meta <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css">
    <link rel="stylesheet" href="css/defaults.css" type="text/css">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="application/javascript" src="js/bootbox.min.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

</head>

<body>

    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="index.php" class="navbar-brand">
                    <img src="images/advlogo.jpg" width="28" height="24" />
                </a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav">
                    <li><a href="index.php">Home<span class="sr-only">(current)</span></a></li>
                    <li><a href="about.php">About Us</a></li>
                    <li class="active"><a href="applogin.php">App Login</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container" style="padding-top: 3%">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel-group" id="panel-774858">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <p class="panel-title">ACDWeb | <strong>Login</strong></p>
                        </div>
                        <div id="panel-element-344374" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <form id="jookeboxLoginForm" name="jookeboxLoginForm" data-toggle="validator" method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
                                    <div class="form-group">
                                        <label class="control-label">Have an account, Login with Google</label>
                                        <div class="g-signin2" onclick="ClickLogin()" data-onsuccess="onSignIn" id="gLogin" type="button"></div>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Or <br> <br> Login with registered Email</label>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">E-mail *</label>
                                        <input type="email" name="userEmail" id="userEmail" class="form-control" maxlength="254" placeholder="Registered e-mail address" pattern="^[\w\-\.]+@[a-zA-Z_0-9]+?((\.[a-zA-Z]{2,3})|(\.[a-zA-Z]{2,3}\.[a-zA-Z]{2,3}))$" data-pattern-error="Invalid email address" data-required-error="This is a mandatory field" required="true" />
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Password *</label>
                                        <input type="password" name="userPassword" id="userPassword" class="form-control" data-minlength="8" maxlength="24" placeholder="Your password" data-required-error="This is a mandatory field" required />
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-success" id="loginbtn">Login</button>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Don't have an account, Sign up now</label>
                                        <button type="button" class="btn btn-warning" id="signUp" data-toggle="modal" data-target="#signUpModal">Sign Up</button>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>


    <!-- The Modal (contains the Sign Up form) -->
    <div class="modal fade" data-backdrop="static" data-keyboard="false" id="signUpModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Sign Up</h4>
                </div>
                <div class="modal-body" style="max-height: 60vh; overflow: auto;">
                    <div class="form-group">
                        <label class="control-label">Have an account, Login with Google</label>
                        <div class="g-signin2" onclick="ClickLoginReg()" data-onsuccess="onSignInReg" id="gLogin" type="button">Sign Up</div>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

</body>

<script type="text/javascript">
    var clicked = false; //Global Variable
    function ClickLogin() {
        clicked = true;
    }
    var clickedReg = false; //Global Variable
    function ClickLoginReg() {
        clickedReg = true;
    }

    function onSignIn(googleUser) {
        if (clicked) {
            var profile = googleUser.getBasicProfile();
            var mail = profile.getEmail();
            var username = profile.getName();
            var tokenId = googleUser.getAuthResponse().id_token;
            callphp(mail, username, tokenId);
        }
    }

    function onSignInReg(googleUser) {
        if (clickedReg) {
            var profile = googleUser.getBasicProfile();
            var mail = profile.getEmail();
            var username = profile.getName();
            var tokenId = googleUser.getAuthResponse().id_token;
            callphpreg(mail, username, tokenId);
        }
    }

    function callphp(mail, username, tokenId) {
        $.ajax({
            url: 'processor/checkuser.php',
            type: 'POST',
            data: {
                'email': mail,
                'username': username,
                'tokenId': tokenId
            },
            success: function(results) {
                if (results == 1) {
                    window.open("welcome.php", '_self');
                } else if (results == 2) {
                    bootbox.alert({
                        size: "large",
                        title: "Sorry",
                        message: "Sorry, your email is not verified",
                        callback: function() {
                            location.reload();
                        }
                    });
                } else if (results == 3) {
                    bootbox.alert({
                        size: "large",
                        title: "Sorry",
                        message: "Sorry, your account is disabled",
                        callback: function() {
                            location.reload();
                        }
                    });
                }
            }
        });
    }

    function callphpreg(mail, username, tokenId) {
        $.ajax({
            url: 'processor/registeruser.php',
            type: 'POST',
            data: {
                'email': mail,
                'username': username,
                'tokenId': tokenId
            },
            success: function(results) {
                //console.log(results);
                if (results == 0) {
                    bootbox.alert({
                        size: "small",
                        title: "Sorry",
                        message: "Something went wrong, Please try again later.",
                        callback: function() {
                            //location.reload();
                        }
                    });
                } else {
                    bootbox.alert({
                        size: "small",
                        title: "Success",
                        message: "We have recieved your request, we will come back to you soon ! Thank you.",
                        callback: function() {
                            location.reload();
                        }
                    });
                }
            }
        });
    }

    $('#loginbtn').click(function() {
        $.ajax({
            url: "processor/checkuseroffline.php",
            type: "POST",
            data: {
                'email': $('#userEmail').val(),
                'password': $('#userPassword').val()
            },
            success: function(results) {
                if (results == 1) {
                    window.open("welcome.php", '_self');
                } else if (results == 2) {
                    bootbox.alert({
                        size: "large",
                        title: "Sorry",
                        message: "Sorry, your email is not verified",
                        callback: function() {
                            location.reload();
                        }
                    });
                } else if (results == 3) {
                    bootbox.alert({
                        size: "large",
                        title: "Sorry",
                        message: "Sorry, your account is disabled",
                        callback: function() {
                            location.reload();
                        }
                    });
                }
            }
        });
    });
</script>

</html>