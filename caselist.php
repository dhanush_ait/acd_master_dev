<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Case List</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('table1 tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // DataTable
    var table = $('table1').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
} );
</script>
<script src = "https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
</head>
<div class="wrapper" style = "color:green;">
        <h3><a href = "welcome.php">Back to Home </a></h3>
        <br /><p>Enter Case Number to find the details.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            
            
            <div class="form-group">
             <label>Case ID</label>
                <input type="text" name="caid" placeholder="caid" class="form-control" required>
                <input type="submit" class="btn btn-primary" value="Search"> 
            </div>    
           </form>
    </div>
    <div class="wrapper" style = "color:green;">
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
    <div class="form-group">
    <input type="hidden" name="caid" placeholder="caid" class="form-control">  
    <input type="submit" class="btn btn-primary" value="Search all">
    </form>
    </div>
    </div>

    <div class="container">
	<!--div class="row">
		<h2>CASE DETAIL</h2>
		<div class="panel panel-primary">
		    <div class="panel-heading">
		        <h3> DATA 
    		        <div class="pull-right">
    		        </div>
		        </h3>
		    </div-->
		    <!--div id="toolbar-admin" class="panel-body">
		        <div class="btn-toolbar" role="toolbar" aria-label="admin">
                    <div class="btn-group pull-right" role="group">
                        <button id="btn-online" class="btn btn-success">Online</button>
                        <button id="btn-offline" class="btn btn-warning">Offline</button>
                        <button id="btn-out-of-order" class="btn btn-danger">Out Of Order</button>
                    </div-->
                </div>
		    </div>
		    <table id = "table1" border='5' class="table table-striped table-hover">
		        <thead>
                    <tr>
<?php
require_once 'config.php';
// Initialize the session
session_start();
 
// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  header("location: login.php");
  exit;
}
//Include config file 
require_once 'config.php';
// Processing form data when form is submitted
function check($result)
{
    $row=mysqli_fetch_row($result) ;
    if(!$row) 
        echo "No record" ;
}
if($_SERVER["REQUEST_METHOD"] == "POST"){
                if(!($_POST['caid']))
                {
                    //$table = 'Case Detail' ;
                    $sql = "SELECT * from case_entry";
                    $result = mysqli_query($link,$sql);
                    if (!$result) {
                                die("Query to show fields from table failed");
                    }
                    $fields_num =  mysqli_num_fields($result);
                
                }    
                else
                {
                $caid =  $_POST['caid'] ;
                $table = 'Case Detail' ;
                $sql = "SELECT * from case_entry where Ca_id='$caid' ";
                $result = mysqli_query($link,$sql);
                if (!$result) {
                            die("Query to show fields from table failed");
                }
                $fields_num = mysqli_num_fields($result);
                }
                

               // echo "<th class="col-check"> </th>";
                //echo "<table border='1'><tr>";
                // printing table headers
                for($i=0; $i<$fields_num; $i++)
                {
                    $field = mysqli_fetch_field($result);
                    echo "<th><h3><b>{$field->name}</h3></th>";
                }
                echo "</tr> </thead>";
                echo "<tbody>";
               // check($result) ;
               
                while($row = mysqli_fetch_row($result))
                {
                    
                    // $row is array... foreach( .. ) puts every element
                    // of $row to $cell variable
                    foreach($row as $cell)
                            echo "<td><h4>$cell</h4></td>";

                    echo "</tr>\n";
                }
                echo " </tbody>
                </table>
            </div>
        </div>
    </div>";
                mysqli_free_result($result);
            }
             
?>
</body>
</html>
