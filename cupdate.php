<?php
// Initialize the session
session_start();
 
// If session variable is not set it will redirect to login page
if(!isSet($_SESSION['username']) || empty($_SESSION['username'])){
  header("location:applogin.php");
  exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
      <title>Client Details Update</title> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>

<div class="wrapper" style = "color:green;">
        <h3><a href = "welcome.php">Back to Home </a></h3>
        <!--h3><a href = "cdetails.php">Add new Client </a></h3-->
    <!-- Bootstrap CSS File  -->
    <link rel="stylesheet" type="text/css" href="bootstrap-3.3.5-dist/css/bootstrap.css"/>
</head>
<body>

<!-- Content Section -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>UPDATE Client details</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <!--button class="btn btn-success" data-toggle="modal" data-target="#add_new_record_modal">Add New Record</button-->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
                        <div class="records_content">
                            
                        </div>
        </div>
    </div>
</div>
<!-- /Content Section -->


<!-- Bootstrap Modals -->
<!-- Modal - Add New Record/User -->
<!--div class="modal fade" id="add_new_record_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Record</h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <label for="first_name">First Name</label>
                    <input type="text" id="first_name" placeholder="First Name" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="last_name">Last Name</label>
                    <input type="text" id="last_name" placeholder="Last Name" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="email">Email Address</label>
                    <input type="text" id="email" placeholder="Email Address" class="form-control"/>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="addRecord()">Add Record</button>
            </div>
        </div>
    </div>
</div-->
<!-- // Modal -->

<!-- Modal - Update User details -->
<div class="modal fade" id="update_Client_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Update</h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <label for="update_C_Address">C_Address</label>
                    <input type="text" id="C_Address" placeholder="C_Address" class="form-control" required/>
                </div>

                <div class="form-group">
                    <label for="update_City">City</label>
                    <input type="text" id="City" placeholder="City" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="update_C_state">C_state</label>
                    <input type="text" id="C_state" placeholder="C_state" class="form-control"/>
                </div>
                
                <div class="form-group">
                    <label for="update_C_gender">C_gender</label>
                    <input type="text" id="C_gender"  placeholder="C_gender" class="form-control"/>
                </div>
                
                <div class="form-group">
                    <label for="update_C_contact">C_contact</label>
                    <input type="number" min="10"  id="C_contact" placeholder="C_contact" class="form-control"/>
                </div>
                    <div class="form-group">
                    <label for="update_C_email">C_email</label>
                    <input type="email" id="C_email" placeholder="C_email" class="form-control"/>
                    </div>
                  
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="UpdateClientDetails()">Save Changes</button>
                <input type="hidden" id="hidden_user_id">
            </div>
        </div>
    </div>
</div>
<!-- // Modal -->

<!-- Jquery JS file -->
<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>

<!-- Bootstrap JS file -->
<script type="text/javascript" src="bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>

<!-- Custom JS file -->
<script type="text/javascript" src="js/cscript.js"></script>
</body>
</html>