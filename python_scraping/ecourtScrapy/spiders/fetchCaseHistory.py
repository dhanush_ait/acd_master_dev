import os
import scrapy
import logging
import re

from scrapy import crawler
from scrapy.utils.log import configure_logging

logger =logging.getLogger()

formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S')
configure_logging(install_root_handler = False)
logging.basicConfig (
    filename = 'logging_caseHistory.txt',
    level=logging.DEBUG
    )
class QuotesSpider(scrapy.Spider):
    logger.info("\n\n\n1st Test Case")
    name = "caseHistory"
    def start_requests(self):
        urls = [
            'https://services.ecourts.gov.in/ecourtindia_v4_bilingual/cases/case_no.php?state=D&state_cd=3&dist_cd=18'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        self.log(self.court +" "+self.cnr+" "+self.cid)
        yield scrapy.FormRequest.from_response(
                response,
                method='POST',
                url="https://services.ecourts.gov.in/ecourtindia_v4_bilingual/cases/o_civil_case_history.php",
                formdata={"court_code":self.court,"state_code": "3","dist_code":"18","case_no":"205600050192018","cino":self.cnr,"appFlag":"","lang":"","str1":""},
                callback=self.after_login)

    def after_login(self, response):
        #page = response.url.split("/")[-2]
        #filename = 'quotes-%s.html' % page
        if(response.body=="<font color='red' size='3'><center><b>Connection Failed</b></center></font>"):
            self.log("server down")
            return 0

        dir = "/userdata/"+self.cid+"/pages"
        if (not (os.path.isdir(dir))):
            os.mkdir(dir)
        fname = dir+"/"+self.cnr+".html"
        with open(fname, 'wb') as f:
            f.write(response.body)
            self.log('Saved file %s' % fname)
        page = response.body
        #print page
        year="2019"
        pattern = '<td>[0-9]{2}-[0-9]{2}-'+year+'</td>'
        match1 = re.findall(pattern, page)
        max = match1.__len__()
        nextdate = str(match1[max-1])
        print nextdate.replace("<td>","").replace("</td>","")
        data = self.listToString(match1)
        #print data
        pat = '<td style=\'\'> [A-Z\s]+</td>'
        pat1 = '[A-Z]+[\s]?[A-Z\s]*'
        match2 = re.findall(pat, page)
        text = self.listToString(match2)
        match3 = re.findall(pat1, text)
        #print match3
        max = match3.__len__()
        # max = match2.__len__()
        # print match2[max-1]
        print match3[max - 1]
        self.log(match3[max - 1])

    def listToString(self,s):
        # initialize an empty string
        str1 = ""
        # traverse in the string
        for ele in s:
            str1 += ele + " "
            # return string
        # print(str1)
        return str1