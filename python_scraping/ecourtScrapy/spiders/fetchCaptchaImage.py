import scrapy
from scrapy.http.cookies import CookieJar
from time import sleep

class QuotesSpider(scrapy.Spider):
    name = "image"
    def start_requests(self):
        urls = [
            'https://services.ecourts.gov.in/ecourtindia_v4_bilingual/securimage/securimage_show.php?0.26273817007413636'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.after_captcha)

    def parse(self, response):
        #cookie = response.headers.getlist('Set-Cookie')[0].split(';')[0]
        #print cookie
        cookie = "PHPSESSID = 97j0261rjbnlhg90ptcinun8c0"
        yield scrapy.FormRequest(
                headers={'Cookie': cookie},
                method='GET',
                url="https://services.ecourts.gov.in/ecourtindia_v4_bilingual/securimage/securimage_show.php?0.26273817007413636",
                callback=self.after_captcha)

    def after_captcha(self, response):
        cookie = response.headers
        print cookie
        #page = response.url.split("/")[-2]
        #filename = 'quotes-%s.html' % page
        fname="captcha.png"
        with open(fname, 'wb') as f:
            f.write(response.body)
            self.log('Saved file %s' % fname)
