import os
import re
import time
import scrapy
import logging
from scrapy.utils.log import configure_logging

logger =logging.getLogger()

formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S')
configure_logging(install_root_handler = False)
logging.basicConfig (
    filename = 'logging.txt',
    level=logging.DEBUG
    )
global start
global end
global count
count = 0
class QuotesSpider(scrapy.Spider):
    logger.info("\n\n\n1st Test Case")
    name = "caseListByName"
    custom_settings = {
        'DUPEFILTER_CLASS': 'scrapy.dupefilters.BaseDupeFilter',
    }
    def start_requests(self):
        self.log("1st test case")
        urls = [
            'https://services.ecourts.gov.in/ecourtindia_v4_bilingual/securimage/securimage_show.php?0.26273817007413636'
        ]
        for url in urls:
            global start
            start = time.time()
            yield scrapy.Request(url=url, callback=self.write_captcha,meta={'dont_merge_cookies': True})

    def parse(self, response):
        global res
        res = response
        global cookie
        cookie = response.headers.getlist('Set-Cookie')[0].split(';')[0]
        #print cookie
        yield scrapy.Request(
            headers={'Cookie': cookie},
            method='GET',
            url="https://services.ecourts.gov.in/ecourtindia_v4_bilingual/securimage/securimage_show.php?0.26273817007413636",
            callback=self.write_captcha,
            meta={'dont_merge_cookies': True})

    def get_page(self,code):
        #print cookie
        return scrapy.FormRequest(
            method='POST',
            headers={'Cookie': cookie},
            meta={'dont_merge_cookies': True},
            url="https://services.ecourts.gov.in/ecourtindia_v4_bilingual/cases/qs_civil_advocate_qry.php",
            formdata={"state_code": "3", "dist_code": "18", "advocate_name": self.advName,
                      "court_codeArr": self.courtComplex, "search_type": "1", "f": "Pending",
                      "action_code": "showRecords", "lang": "", "captcha": code},
            callback=self.write_page)

    def write_captcha(self,response):
        global cookie
        cookie = response.headers.getlist('Set-Cookie')[0].split(';')[0]
        #print cookie
        fname = "captcha.png"
        with open(fname, 'wb') as f:
            f.write(response.body)
            self.log('Saved file %s' % fname)
        code = self.getCode()
        #print code
        if(code):
            #print "success"
            #self.get_page(code,cookie)
            yield scrapy.FormRequest(
            method='POST',
            headers={'Cookie': cookie},
            meta={'dont_merge_cookies': True},
            url="https://services.ecourts.gov.in/ecourtindia_v4_bilingual/cases/qs_civil_advocate_qry.php",
            formdata={"state_code": "3", "dist_code": "18", "advocate_name": self.advName,
                          "court_codeArr": self.courtComplex, "search_type": "1", "f": "Pending",
                          "action_code": "showRecords", "lang": "", "captcha": code},
            callback=self.write_page)
        else:
            global count
            count+=1
            yield scrapy.FormRequest(
                method='GET',
                url="https://services.ecourts.gov.in/ecourtindia_v4_bilingual/securimage/securimage_show.php?0.26273817007413636",
                callback=self.write_captcha,
                meta={'dont_merge_cookies': True})


    def write_page(self,response):
        #print response.headers.getlist('Set-Cookie')[0].split(';')[0]
        global cookie
        #print cookie
        dir ="/userdata/"+self.ID
        if(not (os.path.isdir(dir))):
            os.mkdir(dir)
        fname=dir+"/caseListByName.json"
        if(response.body == '{"con":"Invalid Captcha"}'):
            global count
            count +=1
            self.log("Invalid Captcha")
            #print "exception"
            yield scrapy.FormRequest(
                method='GET',
                url="https://services.ecourts.gov.in/ecourtindia_v4_bilingual/securimage/securimage_show.php?0.26273817007413636",
                callback=self.write_captcha,
                meta={'dont_merge_cookies': True})

        else:
            global count
            self.log(response.body)
            with open(fname, 'wb') as f:
                f.write(response.body)
                self.log('Saved file %s' % fname)
            global end
            end = time.time()
            self.log("Number of attempts made to get result : "+str(count))
            self.log("Start time = " + str(start))
            self.log("End time = "+str(end))
            self.log("Total time in seconds = "+str(end-start))
            print 1

    def listToString(self,s):
        # initialize an empty string
        str1 = ""
        # traverse in the string
        for ele in s:
            str1 += ele + ""
            # return string
        # print(str1)
        return str1

    def getCode(self):
        pattern = '[a-zA-Z0-9]{6}'
        result1 = os.popen('tesseract ./captcha.png stdout -l eng --psm 9').read()
        result2 = os.popen('tesseract ./captcha.png stdout -l eng --psm 8').read()
        match1 = re.findall(pattern, result1)
        match2 = re.findall(pattern, result2)

        if (match1):
            data1 = self.listToString(match1)
            #print data1 + " Match @ 1"
            return data1

        if (match2):
            data2 = self.listToString(match2)
            #print data2 + " Match @ 2"
            return data2